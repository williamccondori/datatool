package Presentation;

import Business.WorkspaceLogic;
import Entity.WorkspaceEntity;
import Helpers.MessageHelper;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;

public class ApplicationFrame extends javax.swing.JFrame {

    private static String WorkspacePath;

    public ApplicationFrame() {
        initComponents();
        loadWindow();
    }

    public static void main(String args[]) {
        new ApplicationFrame().setVisible(Boolean.TRUE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnShowImageManager3 = new javax.swing.JButton();
        tooPrincipal = new javax.swing.JToolBar();
        btnShowImageManager = new javax.swing.JButton();
        btnShowClassManager = new javax.swing.JButton();
        btnShowPlaceManager = new javax.swing.JButton();
        btnShowStateManager = new javax.swing.JButton();
        btnShowImageDetection = new javax.swing.JButton();
        btnShowWorkspaceSettings = new javax.swing.JButton();
        btnShowExportDetectionsDataset = new javax.swing.JButton();
        desPrincipal = new javax.swing.JDesktopPane();
        menPrincipal = new javax.swing.JMenuBar();
        menFile = new javax.swing.JMenu();
        menExit = new javax.swing.JMenuItem();
        menManager = new javax.swing.JMenu();
        menShowImageManager = new javax.swing.JMenuItem();
        menShowClassManager = new javax.swing.JMenuItem();
        menShowPlaceManager = new javax.swing.JMenuItem();
        menShowStateManager = new javax.swing.JMenuItem();
        menImage = new javax.swing.JMenu();
        menShowImageDetection = new javax.swing.JMenuItem();
        menWorkspace = new javax.swing.JMenu();
        menShowWorkspaceSettings = new javax.swing.JMenuItem();
        menExportDetection = new javax.swing.JMenu();
        menShowExportDetectionsDataset = new javax.swing.JMenuItem();
        menTool = new javax.swing.JMenu();
        menShowImageTrimmer = new javax.swing.JMenuItem();
        menShowExportImage = new javax.swing.JMenuItem();
        menHelp = new javax.swing.JMenu();
        menShowAbout = new javax.swing.JMenuItem();

        btnShowImageManager3.setBackground(new java.awt.Color(250, 250, 250));
        btnShowImageManager3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-iconos-medianos-16.png"))); // NOI18N
        btnShowImageManager3.setBorderPainted(false);
        btnShowImageManager3.setFocusPainted(false);
        btnShowImageManager3.setFocusable(false);
        btnShowImageManager3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnShowImageManager3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tooPrincipal.setRollover(true);

        btnShowImageManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-iconos-medianos-16.png"))); // NOI18N
        btnShowImageManager.setToolTipText("Image manager");
        btnShowImageManager.setBorderPainted(false);
        btnShowImageManager.setFocusPainted(false);
        btnShowImageManager.setFocusable(false);
        btnShowImageManager.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnShowImageManager.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnShowImageManager);

        btnShowClassManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-etiqueta-16.png"))); // NOI18N
        btnShowClassManager.setToolTipText("Class manager");
        btnShowClassManager.setBorderPainted(false);
        btnShowClassManager.setFocusPainted(false);
        btnShowClassManager.setFocusable(false);
        btnShowClassManager.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnShowClassManager.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnShowClassManager);

        btnShowPlaceManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-geo-cerca-16.png"))); // NOI18N
        btnShowPlaceManager.setToolTipText("Place manager");
        btnShowPlaceManager.setBorderPainted(false);
        btnShowPlaceManager.setFocusPainted(false);
        btnShowPlaceManager.setFocusable(false);
        btnShowPlaceManager.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnShowPlaceManager.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnShowPlaceManager);

        btnShowStateManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-hoy-16.png"))); // NOI18N
        btnShowStateManager.setToolTipText("State manager");
        btnShowStateManager.setBorderPainted(false);
        btnShowStateManager.setFocusPainted(false);
        btnShowStateManager.setFocusable(false);
        btnShowStateManager.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnShowStateManager.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnShowStateManager);

        btnShowImageDetection.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-agrupar-objetos-16.png"))); // NOI18N
        btnShowImageDetection.setToolTipText("Image detection");
        btnShowImageDetection.setBorderPainted(false);
        btnShowImageDetection.setFocusPainted(false);
        btnShowImageDetection.setFocusable(false);
        btnShowImageDetection.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnShowImageDetection.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnShowImageDetection);

        btnShowWorkspaceSettings.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-configuración-de-datos-16.png"))); // NOI18N
        btnShowWorkspaceSettings.setToolTipText("Workspace settings");
        btnShowWorkspaceSettings.setBorderPainted(false);
        btnShowWorkspaceSettings.setFocusPainted(false);
        btnShowWorkspaceSettings.setFocusable(false);
        btnShowWorkspaceSettings.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnShowWorkspaceSettings.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnShowWorkspaceSettings);

        btnShowExportDetectionsDataset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-exportar-16.png"))); // NOI18N
        btnShowExportDetectionsDataset.setToolTipText("Export detections to dataset");
        btnShowExportDetectionsDataset.setBorderPainted(false);
        btnShowExportDetectionsDataset.setFocusPainted(false);
        btnShowExportDetectionsDataset.setFocusable(false);
        btnShowExportDetectionsDataset.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnShowExportDetectionsDataset.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnShowExportDetectionsDataset);

        javax.swing.GroupLayout desPrincipalLayout = new javax.swing.GroupLayout(desPrincipal);
        desPrincipal.setLayout(desPrincipalLayout);
        desPrincipalLayout.setHorizontalGroup(
            desPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        desPrincipalLayout.setVerticalGroup(
            desPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 254, Short.MAX_VALUE)
        );

        menFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-archivo-16.png"))); // NOI18N
        menFile.setText("File");

        menExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cerrar-ventana-16.png"))); // NOI18N
        menExit.setText("Exit");
        menFile.add(menExit);

        menPrincipal.add(menFile);

        menManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-herramientas-del-administrador-16.png"))); // NOI18N
        menManager.setText("Manager");

        menShowImageManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-iconos-medianos-16.png"))); // NOI18N
        menShowImageManager.setText("Image manager");
        menManager.add(menShowImageManager);

        menShowClassManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-etiqueta-16.png"))); // NOI18N
        menShowClassManager.setText("Class manager");
        menManager.add(menShowClassManager);

        menShowPlaceManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-geo-cerca-16.png"))); // NOI18N
        menShowPlaceManager.setText("Place manager");
        menManager.add(menShowPlaceManager);

        menShowStateManager.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-hoy-16.png"))); // NOI18N
        menShowStateManager.setText("State manager");
        menManager.add(menShowStateManager);

        menPrincipal.add(menManager);

        menImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-pila-de-fotos-16.png"))); // NOI18N
        menImage.setText("Image");

        menShowImageDetection.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        menShowImageDetection.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-agrupar-objetos-16.png"))); // NOI18N
        menShowImageDetection.setText("Image detection");
        menImage.add(menShowImageDetection);

        menPrincipal.add(menImage);

        menWorkspace.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-base-de-datos-16.png"))); // NOI18N
        menWorkspace.setText("Workspace");

        menShowWorkspaceSettings.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-configuración-de-datos-16.png"))); // NOI18N
        menShowWorkspaceSettings.setText("Settings");
        menWorkspace.add(menShowWorkspaceSettings);

        menExportDetection.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-exportación-de-base-de-datos-16.png"))); // NOI18N
        menExportDetection.setText("Export detections");

        menShowExportDetectionsDataset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-exportar-16.png"))); // NOI18N
        menShowExportDetectionsDataset.setText("To data set");
        menExportDetection.add(menShowExportDetectionsDataset);

        menWorkspace.add(menExportDetection);

        menPrincipal.add(menWorkspace);

        menTool.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-caja-de-herramientas-16.png"))); // NOI18N
        menTool.setText("Tools");

        menShowImageTrimmer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cortar-papel-16.png"))); // NOI18N
        menShowImageTrimmer.setText("Image clipper");
        menTool.add(menShowImageTrimmer);

        menShowExportImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-enviar-archivo-16.png"))); // NOI18N
        menShowExportImage.setText("Export images");
        menTool.add(menShowExportImage);

        menPrincipal.add(menTool);

        menHelp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-ayuda-16.png"))); // NOI18N
        menHelp.setText("Help");

        menShowAbout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-acerca-de-16.png"))); // NOI18N
        menShowAbout.setText("About");
        menHelp.add(menShowAbout);

        menPrincipal.add(menHelp);

        setJMenuBar(menPrincipal);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tooPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
            .addComponent(desPrincipal)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tooPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(desPrincipal))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnShowClassManager;
    private javax.swing.JButton btnShowExportDetectionsDataset;
    private javax.swing.JButton btnShowImageDetection;
    private javax.swing.JButton btnShowImageManager;
    private javax.swing.JButton btnShowImageManager3;
    private javax.swing.JButton btnShowPlaceManager;
    private javax.swing.JButton btnShowStateManager;
    private javax.swing.JButton btnShowWorkspaceSettings;
    public static javax.swing.JDesktopPane desPrincipal;
    private javax.swing.JMenuItem menExit;
    private javax.swing.JMenu menExportDetection;
    private javax.swing.JMenu menFile;
    private javax.swing.JMenu menHelp;
    private javax.swing.JMenu menImage;
    private javax.swing.JMenu menManager;
    private javax.swing.JMenuBar menPrincipal;
    private javax.swing.JMenuItem menShowAbout;
    private javax.swing.JMenuItem menShowClassManager;
    private javax.swing.JMenuItem menShowExportDetectionsDataset;
    private javax.swing.JMenuItem menShowExportImage;
    private javax.swing.JMenuItem menShowImageDetection;
    private javax.swing.JMenuItem menShowImageManager;
    private javax.swing.JMenuItem menShowImageTrimmer;
    private javax.swing.JMenuItem menShowPlaceManager;
    private javax.swing.JMenuItem menShowStateManager;
    private javax.swing.JMenuItem menShowWorkspaceSettings;
    private javax.swing.JMenu menTool;
    private javax.swing.JMenu menWorkspace;
    private javax.swing.JToolBar tooPrincipal;
    // End of variables declaration//GEN-END:variables

    /**
     * ALMACENA LA RUTA DE LA CARPETA DEL PROYECTO.
     *
     * @param workspacePath RUTA DE LA CARPETA DEL PROYECTO.
     */
    public void setWorkspacePath(String workspacePath) {
        WorkspacePath = workspacePath;
    }

    /**
     * DEVUELVE LA RUTA DE LA CARPETA DEL PROYECTO.
     *
     * @return RUTA DE LA CARPETA DEL PROYECTO.
     */
    public static String getWorkspacePath() {
        return WorkspacePath;
    }

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA EL FUNCIONAMIENTO DE LOS CONTROLES DE LA VENTANA.
     */
    private void loadControl() {
        this.setSize(new Dimension(800, 600));
        this.setLocationRelativeTo(null);
        this.menExit.addActionListener(this::exit);
        this.menShowImageManager.addActionListener(this::showImageManager);
        this.menShowClassManager.addActionListener(this::showClassManager);
        this.menShowPlaceManager.addActionListener(this::showPlaceManager);
        this.menShowStateManager.addActionListener(this::showStateManager);
        this.menShowImageDetection.addActionListener(this::showImageDetection);
        //this.menShowImageClassification.addActionListener(this::showObjectClassificationTool);
        this.menShowWorkspaceSettings.addActionListener(this::showWorkspaceSettings);
        this.menShowExportDetectionsDataset.addActionListener(this::showExportDetectionsDataset);
        this.menShowImageTrimmer.addActionListener(this::showImageTrimmer);
        this.menShowExportImage.addActionListener(this::showExportImage);
        this.menShowAbout.addActionListener(this::showAbout);

        this.btnShowImageManager.addActionListener(this::showImageManager);
        this.btnShowClassManager.addActionListener(this::showClassManager);
        this.btnShowPlaceManager.addActionListener(this::showPlaceManager);
        this.btnShowStateManager.addActionListener(this::showStateManager);
        this.btnShowImageDetection.addActionListener(this::showImageDetection);
        this.btnShowWorkspaceSettings.addActionListener(this::showWorkspaceSettings);
        this.btnShowExportDetectionsDataset.addActionListener(this::showExportDetectionsDataset);
        
        this.tooPrincipal.setFloatable(Boolean.FALSE);
        ApplicationFrame.desPrincipal.setBackground(new Color(38, 50, 56));
        try {
            ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Icons/logo_datatool.png"));
            this.setIconImage(imageIcon.getImage());
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA LOS DATOS UTILIZADOS EN LA VENTANA.
     *
     * @param workspacePath RUTA DE LA CARPETA DEL PROYECTO.
     */
    public void loadData(String workspacePath) {
        try {
            WorkspaceLogic workspaceLogic = new WorkspaceLogic(workspacePath);
            WorkspaceEntity workspaceEntity = workspaceLogic.get();
            this.setTitle(String.format("%s - Data Set Annotation Tool", workspaceEntity.getName()));
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CIERRA LA APLICACION EN CONJUNTO.
     *
     * @param e EVENTO.
     */
    private void exit(ActionEvent e) {
        try {
            this.dispose();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void showImageManager(ActionEvent e) {
        try {
            ImageManagerFrame classManagerFrame = new ImageManagerFrame();
            showInternalWindow(classManagerFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA VENTANA DEL ADMINISTRADOR DE CLASES.
     *
     * @param e EVENTO.
     */
    private void showClassManager(ActionEvent e) {
        try {
            ClassManagerFrame classManagerFrame = new ClassManagerFrame();
            showInternalWindow(classManagerFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA VENTANA DEL ADMINISTRADOR DE LUGARES.
     *
     * @param e EVENTO.
     */
    private void showPlaceManager(ActionEvent e) {
        try {
            PlaceManagerFrame classManagerFrame = new PlaceManagerFrame();
            showInternalWindow(classManagerFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA VENTANA DEL ADMINISTRADOR DE ESTADOS.
     *
     * @param e EVENTO.
     */
    private void showStateManager(ActionEvent e) {
        try {
            StateManagerFrame statusManagerFrame = new StateManagerFrame();
            showInternalWindow(statusManagerFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA VENTANA DE CLASIFICACION DE OBJETOS.
     *
     * @param e EVENTO.
     */
    private void showObjectClassificationTool(ActionEvent e) {
        try {
            ImageClassificationFrame imageClassificationFrame = new ImageClassificationFrame();
            showInternalWindow(imageClassificationFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA VENTANA DE DETECCION DE OBJETOS.
     *
     * @param e EVENTO.
     */
    private void showImageDetection(ActionEvent e) {
        try {
            ImageDetectionFrame imageDetectionFrame = new ImageDetectionFrame();
            showInternalWindow(imageDetectionFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA VENTANA DE CONFIGURACION DEL ESPACIO DE TRABAJO.
     *
     * @param e EVENTO.
     */
    private void showWorkspaceSettings(ActionEvent e) {
        try {
            WorkspaceSettingsFrame workspaceSettingsFrame = new WorkspaceSettingsFrame();
            showInternalWindow(workspaceSettingsFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA VENTANA DE EXPORTACION A DATASET PARA DETECCIONES.
     *
     * @param e EVENTO.
     */
    private void showExportDetectionsDataset(ActionEvent e) {
        try {
            ExportDetectionsDatasetFrame exportDetectionsDatasetFrame = new ExportDetectionsDatasetFrame();
            showInternalWindow(exportDetectionsDatasetFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA VENTANA DE LA RECORTADORA DE IMAGENES.
     *
     * @param e EVENTO.
     */
    private void showImageTrimmer(ActionEvent e) {
        try {
            ImageClipperFrame imageTrimmerFrame = new ImageClipperFrame();
            showInternalWindow(imageTrimmerFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void showExportImage(ActionEvent e) {
        try {
            ExportTrimDatasetFrame exportTrimDatasetFrame = new ExportTrimDatasetFrame();
            showInternalWindow(exportTrimDatasetFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }
    
    private void showAbout(ActionEvent e) {
        try {
            AboutFrame aboutFrame = new AboutFrame();
            showInternalWindow(aboutFrame);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UNA VENTANA DENTRO DEL COMPONENTE DE ESCRITORIO.
     *
     * @param window VENTANA A MOSTRAR.
     */
    private void showInternalWindow(JInternalFrame window) {
        desPrincipal.add(window);
        Dimension desktopSize = desPrincipal.getSize();
        Dimension windowSize = window.getSize();
        window.setLocation((int) ((desktopSize.getWidth() - windowSize.getWidth()) / 2), (int) ((desktopSize.getHeight() - windowSize.getHeight()) / 2));
        window.setVisible(true);
    }
}
