package Entity;

public class ComboEntity {

    private int Value;
    private String Description;

    public int getValue() {
        return Value;
    }

    public String getDescription() {
        return Description;
    }

    public ComboEntity() {
    }

    public ComboEntity(int Value, String Description) {
        this.Value = Value;
        this.Description = Description;
    }

    public static ComboEntity getDefault() {
        ComboEntity comboModel = new ComboEntity();
        comboModel.Value = 0;
        comboModel.Description = "[SELECT]";
        return comboModel;
    }

    @Override
    public String toString(){
        return this.Description;
    }
}
