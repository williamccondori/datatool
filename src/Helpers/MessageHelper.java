package Helpers;

import javax.swing.JOptionPane;

public class MessageHelper {

    public static void showErrorMessage(Exception exception) {
        JOptionPane.showMessageDialog(null, exception.getLocalizedMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
    }

    public static int showWarningMessage(String message) {
        return JOptionPane.showConfirmDialog(null, message, "Warning!", JOptionPane.YES_NO_OPTION);
    }

    public static boolean isWarningYes(int result) {
        return result == JOptionPane.YES_OPTION;
    }

    public static void showSuccessMessage(String message) {
        JOptionPane.showMessageDialog(null, message, "Correct!", JOptionPane.INFORMATION_MESSAGE);
    }
}
