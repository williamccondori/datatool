package Helpers;

import javax.swing.JTable;

public class TableHelper {

    /**
     * OBTIENE EL INDICE SELECCIONADO DE UNA TABLA.
     *
     * @param table TABLA.
     * @return INDICE SELECCIONADO.
     */
    public static final int getSelectedId(JTable table) {
        int id = 0;
        int[] selectedRow = table.getSelectedRows();
        for (int i = 0; i < selectedRow.length; i++) {
            id = (int) table.getValueAt(selectedRow[i], 0);
        }
        return id;
    }
}
