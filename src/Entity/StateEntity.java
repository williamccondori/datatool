package Entity;

import Helpers.ApplicationSettings;
import Helpers.StringHelper;
import java.awt.Color;

public class StateEntity {

    private final int Id;
    private String Name;
    private String Description;
    private Color DisplayColor;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public Color getDisplayColor() {
        return DisplayColor;
    }

    public StateEntity() {
        this.Id = 0;
    }

    public StateEntity(int Id, String Name, String Description, Color DisplayColor) {
        this.Id = Id;
        this.Name = Name.toUpperCase();
        this.Description = Description;
        this.DisplayColor = DisplayColor;
    }

    public static StateEntity getDefaultState() {
        return new StateEntity(ApplicationSettings.PREFIX_ID, "NOT ASSIGNED", "NOT ASSIGNED", new Color(255, 205, 210));
    }

    public void updateState(String name, String description, Color displayColor) {
        this.Name = name.toUpperCase();
        this.Description = description;
        this.DisplayColor = displayColor;
    }

    public void validate() throws Exception {
        if (StringHelper.isNullOrEmpty(this.Name)) {
            throw new Exception("Name is required!");
        }
        if (this.DisplayColor == null) {
            throw new Exception("Display color is required!");
        }
    }
}
