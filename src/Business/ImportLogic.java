package Business;

import Entity.AnnotationEntity;
import Entity.ClassEntity;
import Entity.DatasetType;
import Entity.DetectionEntity;
import Entity.ImageEntity;
import Entity.ImportImageEntity;
import Entity.VocImportEntity;
import Entity.WorkspaceEntity;
import Helpers.FileHelper;
import Helpers.StringHelper;
import Presentation.ApplicationFrame;
import Presentation.ImportWorkspaceFrame;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImportLogic extends Thread {

    private final String DatasetPath;
    private final WorkspaceEntity Workspace;
    private DatasetType DatasetTipe;

    public ImportLogic(String datasetPath, WorkspaceEntity workspaceEntity) {
        this.DatasetPath = datasetPath;
        this.Workspace = workspaceEntity;
    }

    @Override
    public void run() {
        try {
            startWindow();

            detectDatasetType();

            if (this.DatasetTipe == null) {
                throw new Exception("The Dataset format is not valid!");
            }

            String imageInputPath;
            String annotationInputPath;
            switch (this.DatasetTipe) {
                case VOC_PASCAL:
                    imageInputPath = String.format("%s/%s", this.DatasetPath, "JPEGImages");
                    annotationInputPath = String.format("%s/%s", this.DatasetPath, "Annotations");
                    break;
                case YOLO:
                    imageInputPath = String.format("%s/%s", this.DatasetPath, "Images");
                    annotationInputPath = String.format("%s/%s", this.DatasetPath, "Images");
                    break;
                default:
                    throw new Exception("The Dataset format is not valid!");
            }

            WorkspaceLogic workspaceLogic = new WorkspaceLogic(this.Workspace.getPath());
            workspaceLogic.create(this.Workspace);
            ArrayList<ImportImageEntity> importImages = ImportImageEntity.getImportImages(Boolean.TRUE, imageInputPath);
            ImportWorkspaceFrame.proLog.setMaximum(importImages.size());
            int lastImageId = workspaceLogic.getLastImageId();
            AnnotationLogic annotationLogic = new AnnotationLogic(this.Workspace.getAnnotationPath());

            printLog(String.format("Number of images: %s", importImages.size()));

            for (int i = 0; i < importImages.size(); i++) {
                int idImage = lastImageId + i;

                // PROCESO DE COPIADO DE LAS IMAGENES.
                ImportImageEntity importImageEntity = importImages.get(i);
                File imageInput = new File(String.format("%s/%s", imageInputPath, importImageEntity.getName()));
                File imageOutput = new File(String.format("%s/%s.%s", this.Workspace.getImagePath(), idImage, importImageEntity.getExtension()));
                FileHelper.copyFile(imageInput, imageOutput);

                printLog(String.format("Image input: %s", imageInput.getName()));
                printLog(String.format("Image output: %s", imageOutput.getName()));

                // PROCESO DE GENERACION DE LAS ANOTACIONES.
                ArrayList<DetectionEntity> detections = new ArrayList<>();
                ArrayList<ClassEntity> classes = new ArrayList<>();

                annotationLogic.generate(idImage, imageOutput, imageInput.getName());

                switch (this.DatasetTipe) {
                    case VOC_PASCAL:
                        ImportVocLogic importVocLogic = new ImportVocLogic(annotationInputPath);
                        VocImportEntity vocImportEntity = importVocLogic.getDetections(getAnnotationName(imageInput.getName(), "xml"));
                        detections = vocImportEntity.getDetections();
                        classes = vocImportEntity.getClasses();
                        break;
                    case YOLO:
                        AnnotationEntity annotationEntity = annotationLogic.get(idImage);
                        ImportYoloLogic importYoloLogic = new ImportYoloLogic(annotationInputPath);
                        detections = importYoloLogic.getDetections(getAnnotationName(imageInput.getName(), "txt"),
                                annotationEntity.getWidth(), annotationEntity.getHeight());
                        classes = new ArrayList<>();
                        break;
                    default:
                        throw new Exception("The Dataset format is not valid!");
                }

                for (ClassEntity classEntity : classes) {
                    classEntity.validate();
                    workspaceLogic.saveClass(classEntity);
                }

                annotationLogic.addDetections(idImage, detections);
                printLog(String.format("Importing annotation: %s", idImage));

                // REGISTRA LA IMAGEN IMPORTADA EN EL ARCHIVO PRINCIPAL.
                ImageEntity imageEntity = ImageEntity.register(idImage, importImageEntity.getExtension());
                workspaceLogic.registerImage(imageEntity);

                // LEE LOS ATRIBUTOS DEL XML SEGUN EL TIPO DE DATASET.
                // ACTUALIZA EL VALOR DE LA BARRA DE PROGRESO.
                ImportWorkspaceFrame.proLog.setValue(i + 1);
            }
            finishWindow();
        } catch (Exception exception) {
            FileHelper.deleteFolder(this.Workspace.getPath());
            errorWindow(exception);
        }
    }

    private void startWindow() {
        ImportWorkspaceFrame.lblLog.setText("Importing workspace, this may take a few minutes...");
        ImportWorkspaceFrame.txtLog.setText(StringHelper.EMPTY);
        ImportWorkspaceFrame.txtLog.setEditable(Boolean.FALSE);
        ImportWorkspaceFrame.proLog.setMaximum(0);
        ImportWorkspaceFrame.proLog.setValue(0);
        ImportWorkspaceFrame.btnAccept.setEnabled(Boolean.FALSE);
    }

    private void finishWindow() {
        ImportWorkspaceFrame.lblLog.setText("Completed!");
        ImportWorkspaceFrame.proLog.setValue(ImportWorkspaceFrame.proLog.getMaximum());
        printLog("Completed!");
        ImportWorkspaceFrame.btnAccept.setEnabled(Boolean.TRUE);
    }

    private void errorWindow(Exception exception) {
        ImportWorkspaceFrame.lblLog.setText("Error!");
        ImportWorkspaceFrame.proLog.setMaximum(0);
        ImportWorkspaceFrame.proLog.setValue(0);
        printLog(String.format("An error has occurred: %s", exception.getLocalizedMessage()));
        ImportWorkspaceFrame.btnAccept.setEnabled(Boolean.TRUE);
    }

    private void printLog(String message) {
        ImportWorkspaceFrame.txtLog.append(message + "\n");
        ImportWorkspaceFrame.txtLog.setCaretPosition(ImportWorkspaceFrame.txtLog.getText().length());
    }

    private void detectDatasetType() throws Exception {
        File datasetFolder = new File(this.DatasetPath);

        if (datasetFolder.exists()) {
            File[] content = datasetFolder.listFiles();

            if (Arrays.stream(content).anyMatch(p -> p.getName().equals("Images"))) {
                this.DatasetTipe = DatasetType.YOLO;
            }

            if (Arrays.stream(content).anyMatch(p -> p.getName().equals("JPEGImages"))) {
                this.DatasetTipe = DatasetType.VOC_PASCAL;
            }
        }
    }

    private String getAnnotationName(String fileName, String extension) {
        String id = FileHelper.getFileNameWithoutExtension(fileName);
        return String.format("%s.%s", id, extension);
    }
}
