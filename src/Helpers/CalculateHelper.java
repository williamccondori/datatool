package Helpers;

import Entity.DetectionEntity;
import java.util.ArrayList;
import java.util.List;

public class CalculateHelper {

    public static List<DetectionEntity> calculateNewBoudingBox(List<DetectionEntity> listDetectionEntity, int Xclipmin, int Xclipmax,
            int Yclipmin, int Yclipmax, int Xmax, int Ymax) {
        List<DetectionEntity> listDetectionEntityNew = new ArrayList<>();
        for (int i = 0; i < listDetectionEntity.size(); i++) {
            DetectionEntity detectionEntity = listDetectionEntity.get(i);
            if (isInsideBoudingBox(detectionEntity, Xclipmin, Xclipmax, Yclipmin, Yclipmax)) {
                int Xnewmin = detectionEntity.getXmin() - Xclipmin;
                int Xnewmax = detectionEntity.getXmax() - Xclipmin;
                int Ynewmin = detectionEntity.getYmin() - Yclipmin;
                int Ynewmax = detectionEntity.getYmax() - Yclipmin;
                DetectionEntity detectionEntityTemp = DetectionEntity.setObjectData(detectionEntity.getId(),
                        detectionEntity.getIdClass(),
                        detectionEntity.getIdSubClass(),
                        detectionEntity.getIdSize(),
                        detectionEntity.getPose(),
                        detectionEntity.getTruncated(),
                        detectionEntity.getDifficult(),
                        detectionEntity.getOccluded());
                detectionEntityTemp.setObjectDetection(Xnewmin, Ynewmin, Xnewmax, Ynewmax);
                listDetectionEntityNew.add(detectionEntityTemp);

            }
        }
        return listDetectionEntityNew;
    }

    public static boolean isInsideBoudingBox(DetectionEntity detectionEntity, int Xclipmin, int Xclipmax,
            int Yclipmin, int Yclipmax) {
        return detectionEntity.getXmin() >= Xclipmin
                && detectionEntity.getXmax() <= Xclipmax
                && detectionEntity.getYmin() >= Yclipmin
                && detectionEntity.getYmax() <= Yclipmax;
    }

}
