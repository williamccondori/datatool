package Helpers;

public class SystemHelper {

    public static String getUserName() {
        return System.getProperty("user.name");
    }

    public static String getUserHome() {
        return System.getProperty("user.home");
    }

    public static String getJavaRuntime() {
        return System.getProperty("java.runtime.name");
    }

    public static String getJavaHome() {
        return System.getProperty("java.home");
    }

    public static String getJavaVirtualMachine() {
        return System.getProperty("java.vm.name");
    }

    public static String getOperatingSystem() {
        return System.getProperty("os.name");
    }

    public static String getOperatingSystemVersion() {
        return System.getProperty("os.version");
    }
}
