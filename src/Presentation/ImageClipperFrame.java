package Presentation;

import Business.AnnotationLogic;
import Business.WorkspaceLogic;
import Components.TrimmerComponent;
import Entity.AnnotationEntity;
import Entity.ClassEntity;
import Entity.DetectionEntity;
import Entity.WorkspaceEntity;
import Helpers.ApplicationSettings;
import Helpers.MessageHelper;
import Helpers.StringHelper;
import Helpers.TableHelper;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.util.ArrayList;
import java.util.Optional;
import javax.imageio.ImageIO;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

public class ImageClipperFrame extends javax.swing.JInternalFrame {

    private int Id;
    private int LastId;
    private int IdSubDetection;

    private BufferedImage OriginalImage;
    private BufferedImage ResultImage;

    public ImageClipperFrame() {
        this.Id = ApplicationSettings.PREFIX_ID;
        this.IdSubDetection = 0;
        initComponents();
        loadWindow();
    }

    public ImageClipperFrame(int Id) {
        this.Id = Id;
        this.IdSubDetection = 0;
        initComponents();
        loadWindow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tooPrincipal = new javax.swing.JToolBar();
        btnCursor = new javax.swing.JButton();
        btnHand = new javax.swing.JButton();
        btnResize = new javax.swing.JButton();
        pnlId = new javax.swing.JPanel();
        lblId = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        pnlImage = new javax.swing.JPanel();
        scrImage = new javax.swing.JScrollPane();
        cmpTrimmerWindow = new Components.TrimmerComponent();
        jPanel2 = new javax.swing.JPanel();
        lblSearch = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        lblBrightness = new javax.swing.JLabel();
        sliBrightness = new javax.swing.JSlider();
        lblContrast = new javax.swing.JLabel();
        sliContrast = new javax.swing.JSlider();
        txtBrightness = new javax.swing.JLabel();
        txtContrast = new javax.swing.JLabel();
        btnSearch = new javax.swing.JButton();
        btnTrim = new javax.swing.JButton();
        srcObjects = new javax.swing.JScrollPane();
        tblTrimObjects = new javax.swing.JTable();
        btnDeleteTrim = new javax.swing.JButton();
        tooZoom = new javax.swing.JToolBar();
        btnZoomIn = new javax.swing.JButton();
        btnZoomOut = new javax.swing.JButton();
        btnZoomDefault = new javax.swing.JButton();
        sliZoom = new javax.swing.JSlider();
        txtZoom = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cortar-papel-16.png"))); // NOI18N

        tooPrincipal.setRollover(true);

        btnCursor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-crear-nuevo-16.png"))); // NOI18N
        btnCursor.setToolTipText("Draw");
        btnCursor.setBorderPainted(false);
        btnCursor.setFocusPainted(false);
        btnCursor.setFocusable(false);
        btnCursor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCursor.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnCursor);

        btnHand.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cambiar-tamaño-cuatro-direcciones-16.png"))); // NOI18N
        btnHand.setToolTipText("Move");
        btnHand.setBorderPainted(false);
        btnHand.setFocusPainted(false);
        btnHand.setFocusable(false);
        btnHand.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnHand.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnHand);

        btnResize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cambiar-el-tamaño-16.png"))); // NOI18N
        btnResize.setToolTipText("Resize");
        btnResize.setBorderPainted(false);
        btnResize.setFocusPainted(false);
        btnResize.setFocusable(false);
        btnResize.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnResize.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnResize);

        lblId.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblId.setText("[ID]");
        lblId.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblId.setPreferredSize(new java.awt.Dimension(100, 26));

        javax.swing.GroupLayout pnlIdLayout = new javax.swing.GroupLayout(pnlId);
        pnlId.setLayout(pnlIdLayout);
        pnlIdLayout.setHorizontalGroup(
            pnlIdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblId, javax.swing.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE)
        );
        pnlIdLayout.setVerticalGroup(
            pnlIdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblId, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
        );

        tooPrincipal.add(pnlId);

        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-anterior-16.png"))); // NOI18N
        btnBack.setToolTipText("Back");
        btnBack.setBorderPainted(false);
        btnBack.setFocusPainted(false);
        btnBack.setFocusable(false);
        btnBack.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBack.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnBack);

        btnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-siguiente-16.png"))); // NOI18N
        btnNext.setToolTipText("Next");
        btnNext.setBorderPainted(false);
        btnNext.setFocusPainted(false);
        btnNext.setFocusable(false);
        btnNext.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNext.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnNext);

        pnlImage.setLayout(new javax.swing.OverlayLayout(pnlImage));

        javax.swing.GroupLayout cmpTrimmerWindowLayout = new javax.swing.GroupLayout(cmpTrimmerWindow);
        cmpTrimmerWindow.setLayout(cmpTrimmerWindowLayout);
        cmpTrimmerWindowLayout.setHorizontalGroup(
            cmpTrimmerWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 413, Short.MAX_VALUE)
        );
        cmpTrimmerWindowLayout.setVerticalGroup(
            cmpTrimmerWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 419, Short.MAX_VALUE)
        );

        scrImage.setViewportView(cmpTrimmerWindow);

        pnlImage.add(scrImage);

        lblSearch.setText("Search:");

        txtSearch.setText("jTextField1");

        lblBrightness.setText("Brightness:");

        lblContrast.setText("Contrast:");

        txtBrightness.setText("[]");

        txtContrast.setText("[]");

        btnSearch.setBackground(new java.awt.Color(255, 255, 255));
        btnSearch.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(0, 102, 153));
        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-búsqueda-16.png"))); // NOI18N
        btnSearch.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSearch.setFocusPainted(false);

        btnTrim.setBackground(new java.awt.Color(255, 255, 255));
        btnTrim.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnTrim.setForeground(new java.awt.Color(0, 102, 153));
        btnTrim.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cortar-16.png"))); // NOI18N
        btnTrim.setText("Clip");
        btnTrim.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnTrim.setFocusPainted(false);

        tblTrimObjects.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        srcObjects.setViewportView(tblTrimObjects);

        btnDeleteTrim.setBackground(new java.awt.Color(255, 255, 255));
        btnDeleteTrim.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnDeleteTrim.setForeground(new java.awt.Color(0, 102, 153));
        btnDeleteTrim.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-eliminar-16.png"))); // NOI18N
        btnDeleteTrim.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnDeleteTrim.setFocusPainted(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sliBrightness, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sliContrast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblBrightness)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBrightness))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblContrast)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtContrast)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnTrim, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnDeleteTrim, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(srcObjects, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(txtSearch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBrightness)
                    .addComponent(txtBrightness))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliBrightness, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblContrast)
                    .addComponent(txtContrast))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliContrast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTrim, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(srcObjects, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDeleteTrim, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(93, Short.MAX_VALUE))
        );

        tooZoom.setRollover(true);

        btnZoomIn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-acercar-16.png"))); // NOI18N
        btnZoomIn.setToolTipText("Zoom in");
        btnZoomIn.setBorder(null);
        btnZoomIn.setBorderPainted(false);
        btnZoomIn.setFocusPainted(false);
        btnZoomIn.setFocusable(false);
        btnZoomIn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnZoomIn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooZoom.add(btnZoomIn);

        btnZoomOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-alejar-16.png"))); // NOI18N
        btnZoomOut.setToolTipText("Zoom out");
        btnZoomOut.setBorderPainted(false);
        btnZoomOut.setFocusPainted(false);
        btnZoomOut.setFocusable(false);
        btnZoomOut.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnZoomOut.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooZoom.add(btnZoomOut);

        btnZoomDefault.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-visible-16.png"))); // NOI18N
        btnZoomDefault.setToolTipText("Zoom default");
        btnZoomDefault.setBorderPainted(false);
        btnZoomDefault.setFocusPainted(false);
        btnZoomDefault.setFocusable(false);
        btnZoomDefault.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnZoomDefault.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooZoom.add(btnZoomDefault);
        tooZoom.add(sliZoom);

        txtZoom.setText("100");
        tooZoom.add(txtZoom);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tooPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(tooZoom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tooPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addComponent(tooZoom, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCursor;
    private javax.swing.JButton btnDeleteTrim;
    private javax.swing.JButton btnHand;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnResize;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnTrim;
    private javax.swing.JButton btnZoomDefault;
    private javax.swing.JButton btnZoomIn;
    private javax.swing.JButton btnZoomOut;
    private Components.TrimmerComponent cmpTrimmerWindow;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblBrightness;
    private javax.swing.JLabel lblContrast;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblSearch;
    private javax.swing.JPanel pnlId;
    private javax.swing.JPanel pnlImage;
    private javax.swing.JScrollPane scrImage;
    private javax.swing.JSlider sliBrightness;
    private javax.swing.JSlider sliContrast;
    private javax.swing.JSlider sliZoom;
    private javax.swing.JScrollPane srcObjects;
    private javax.swing.JTable tblTrimObjects;
    private javax.swing.JToolBar tooPrincipal;
    private javax.swing.JToolBar tooZoom;
    private javax.swing.JLabel txtBrightness;
    private javax.swing.JLabel txtContrast;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JLabel txtZoom;
    // End of variables declaration//GEN-END:variables

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
            loadData();
            loadForm();
            loadResource();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void loadControl() {
        this.setSize(new Dimension(600, 500));
        this.setTitle("Image clipper");

        this.tooPrincipal.setFloatable(Boolean.FALSE);
        this.tooZoom.setFloatable(Boolean.FALSE);

        this.btnCursor.addActionListener(this::cursor);
        this.btnHand.addActionListener(this::hand);
        this.btnResize.addActionListener(this::resize);
        this.btnBack.addActionListener(this::back);
        this.btnNext.addActionListener(this::next);
        this.btnZoomIn.addActionListener(this::zoomIn);
        this.btnZoomOut.addActionListener(this::zoomOut);
        this.btnZoomDefault.addActionListener(this::zoomDefault);
        this.btnSearch.addActionListener(this::search);
        this.btnTrim.addActionListener(this::trim);
        this.btnDeleteTrim.addActionListener(this::deleteTrim);

        this.scrImage.getVerticalScrollBar().setUnitIncrement(20);
        this.scrImage.getHorizontalScrollBar().setUnitIncrement(20);
        this.scrImage.addMouseWheelListener(this::addZoomEvent);

        this.cmpTrimmerWindow.loadScrollPanel(this.scrImage);

        // CONFIGRUACION DEL ZOOM, BRILLO Y CONTRASTE.
        this.sliZoom.addChangeListener(this::changeZoom);
        this.sliBrightness.addChangeListener(this::changeIntensity);
        this.sliContrast.addChangeListener(this::changeIntensity);
        this.sliZoom.setMaximum(ApplicationSettings.MAX_ZOOM);
        this.sliZoom.setMinimum(ApplicationSettings.MIN_ZOOM);
        this.sliBrightness.setMinimum((int) ApplicationSettings.MIN_BRIGHTNESS);
        this.sliBrightness.setMaximum((int) ApplicationSettings.MAX_BRIGHTNESS);
        this.sliContrast.setMinimum((int) ApplicationSettings.MIN_CONTRAST);
        this.sliContrast.setMaximum((int) ApplicationSettings.MAX_CONTRAST);

        ListSelectionModel classSelectionModel = this.tblTrimObjects.getSelectionModel();
        classSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        classSelectionModel.addListSelectionListener(this::selectTrimObject);

        String columns[] = {"Id", "X", "Y", "W", "H"};
        DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
        this.tblTrimObjects.setModel(defaultTableModel);
    }

    public void loadData() throws Exception {
    }

    private void loadForm() {
        setZoom(ApplicationSettings.DEF_ZOOM);
        setBrightness((int) ApplicationSettings.DEF_BRIGHTNESS);
        setContrast((int) ApplicationSettings.DEF_CONTRAST);
        this.txtSearch.setText(StringHelper.EMPTY);
    }

    private void loadResource() {
        try {
            this.cmpTrimmerWindow.clear();
            WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
            WorkspaceEntity workspaceEntity = workspaceLogic.get();
            AnnotationLogic annotationLogic = new AnnotationLogic(workspaceEntity.getAnnotationPath());
            AnnotationEntity annotationEntity = getAnnotationEntity(annotationLogic);

            if (annotationEntity != null) {
                this.LastId = workspaceLogic.getLastImageId() - 1;

                //CARGA LA IMAGEN SELECCIONADA.
                String imagePath = String.format("%s/%s", workspaceEntity.getImagePath(), annotationEntity.getImageName());
                BufferedImage bufferedImage = ImageIO.read(new File(imagePath));
                this.cmpTrimmerWindow.loadImage(bufferedImage);
                this.OriginalImage = bufferedImage;
                this.ResultImage = ImageIO.read(new File(imagePath));
                bufferedImage.flush();

                ArrayList<DetectionEntity> objects = annotationEntity.getDetections();

                String columns[] = {"Id", "X", "Y", "W", "H"};
                DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
                ArrayList<DetectionEntity> subObjects = annotationEntity.getSubDetections();

                objects.forEach((object) -> {
                    String className = getClassName(workspaceEntity.getClasses(), object.getIdClass());
                    Color classColor = getClassColor(workspaceEntity.getClasses(), object.getIdClass());
                    this.cmpTrimmerWindow.setObjectStatic(object.getXmin(),
                            object.getYmin(), object.getXmax(), object.getYmax(), classColor, className);
                });

                subObjects.forEach((object) -> {
                    this.cmpTrimmerWindow.setObjectTrim(object.getXmin(), object.getYmin(), object.getXmax(), object.getYmax());
                    int pw = Math.abs(object.getXmin() - object.getXmax());
                    int ph = Math.abs(object.getYmin() - object.getYmax());
                    defaultTableModel.addRow(new Object[]{object.getId(), object.getXmin(), object.getYmin(), pw, ph});
                });

                this.tblTrimObjects.setModel(defaultTableModel);

                this.txtSearch.setText(String.valueOf(annotationEntity.getId()));
                this.lblId.setText(String.valueOf(annotationEntity.getId()));
            } else {
                this.txtSearch.setText(StringHelper.EMPTY);
                this.lblId.setText(StringHelper.EMPTY);
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void cursor(ActionEvent e) {
        try {
            this.cmpTrimmerWindow.setObjectEditable(Boolean.FALSE);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void hand(ActionEvent e) {
        try {
            this.cmpTrimmerWindow.setHandMode();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void resize(ActionEvent e) {
        try {
            this.cmpTrimmerWindow.setObjectEditable(Boolean.TRUE);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA SIGUIENTE IMAGEN.
     *
     * @param e EVENTO.
     */
    private void next(ActionEvent e) {
        try {
            if (!(this.Id >= this.LastId)) {
                this.Id++;
                loadForm();
                loadResource();
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA ANTERIOR IMAGEN.
     *
     * @param e EVENTO.
     */
    private void back(ActionEvent e) {
        try {
            if (!(this.Id == ApplicationSettings.PREFIX_ID)) {
                this.Id--;
                loadForm();
                loadResource();
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * BUSCA UNA IMAGEN ESPECIFICA.
     *
     * @param e EVENTO.
     */
    private void search(ActionEvent e) {
        try {
            if (!(StringHelper.isNullOrEmpty(txtSearch.getText()))) {
                int idImage = StringHelper.toInt32(txtSearch.getText());
                if (idImage < ApplicationSettings.PREFIX_ID) {
                    idImage = idImage + (ApplicationSettings.PREFIX_ID - 1);
                }
                if (idImage > this.LastId) {
                    idImage = this.LastId;
                }
                this.Id = idImage;
                loadForm();
                loadResource();
            }
        } catch (NumberFormatException exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * REALIZA UN ACERCAMIENTO A LA IMAGEN.
     *
     * @param e EVENTO.
     */
    private void zoomIn(ActionEvent e) {
        try {
            setZoom(this.sliZoom.getValue() + 1);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * REALIZA UN ALEJAMIENTO A LA IMAGEN.
     *
     * @param e EVENTO.
     */
    private void zoomOut(ActionEvent e) {
        try {
            setZoom(this.sliZoom.getValue() - 1);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA IMAGEN SIN ACERCAMIENTO NI ALEJAMIENTO.
     *
     * @param e EVENTO.
     */
    private void zoomDefault(ActionEvent e) {
        try {
            setZoom(ApplicationSettings.DEF_ZOOM);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * ASIGNA UN VALOR PARA EL ZOOM.
     *
     * @param value VALOR.
     */
    private void setZoom(int value) {
        this.sliZoom.setValue(value);
        changeZoom();
    }

    /**
     * ASIGNA UN VALOR PARA EL BRILLO.
     *
     * @param value VALOR.
     */
    private void setBrightness(int value) {
        this.sliBrightness.setValue(value);
        changeIntensity();
    }

    /**
     * ASIGNA UN VALOR PARA EL CONTRASTE.
     *
     * @param value VALOR.
     */
    private void setContrast(int value) {
        this.sliContrast.setValue(value);
        changeIntensity();
    }

    /**
     * CONTROLA EL NIVEL DEL ZOOM.
     *
     * @param e EVENTO.
     */
    private void changeZoom(ChangeEvent e) {
        try {
            changeZoom();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CONTROLA EL NIVEL DE INTENSIDAD.
     *
     * @param e EVENTO.
     */
    private void changeIntensity(ChangeEvent e) {
        try {
            changeIntensity();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MODIFICA EL VALOR DEL ZOOM.
     */
    private void changeZoom() {

        // VALIDAR LOS LIMITES DEL ZOOM.
        if (this.sliZoom.getValue() == ApplicationSettings.MAX_ZOOM) {
            this.sliZoom.setValue(ApplicationSettings.MAX_ZOOM);
        }

        if (this.sliZoom.getValue() == ApplicationSettings.MIN_ZOOM) {
            this.sliZoom.setValue(ApplicationSettings.MIN_ZOOM);
        }

        // ACTUALIZA LOS VALORES DE LOS CONTROLES.
        this.txtZoom.setText(String.valueOf(this.sliZoom.getValue()));

        // ACTUALIZA EL VALOR DEL ZOOM DE LA IMAGEN.
        if (!(this.OriginalImage == null || this.ResultImage == null)) {
            double factor = (double) this.sliZoom.getValue() / ApplicationSettings.DEF_ZOOM;
            this.cmpTrimmerWindow.setZoom(factor);
        }
    }

    /**
     * MODIFICA EL VALOR DE LA INTENSIDAD.
     */
    private void changeIntensity() {
        // ACTUALIZA LOS VALORES DE LOS CONTROLES.
        this.txtBrightness.setText(String.valueOf(this.sliBrightness.getValue()));
        this.txtContrast.setText(String.valueOf(this.sliContrast.getValue()));

        // ACTUALIZA LA INTENSIDAD DE LA IMAGEN.
        if (!(this.OriginalImage == null || this.ResultImage == null)) {
            float factorBrightness = (float) (this.sliBrightness.getValue() / ApplicationSettings.DEF_BRIGHTNESS);
            float factorContrast = (float) ((this.sliContrast.getValue() - ApplicationSettings.DEF_CONTRAST) / 2);
            RescaleOp rescale = new RescaleOp(factorBrightness, factorContrast, null);
            rescale.filter(this.OriginalImage, this.ResultImage);
            this.cmpTrimmerWindow.loadImage(this.ResultImage);
        }
    }

    /**
     * AGREGA EL EVENTO DEL ZOOM CUANDO SE PRESIONA CONTROL.
     *
     * @param e EVENTO.
     */
    private void addZoomEvent(MouseWheelEvent e) {
        try {
            if (e.isControlDown()) {
                if (e.getWheelRotation() < 0) {
                    setZoom(this.sliZoom.getValue() + 1);
                } else {
                    setZoom(this.sliZoom.getValue() - 1);
                }
                this.scrImage.setWheelScrollingEnabled(false);
            } else {
                this.scrImage.setWheelScrollingEnabled(true);
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * DEVUELVE LOS DATOS DE UNA ANOTACION SIN LANZAR UNA EXCEPCION.
     *
     * @param annotationLogic LOGICA DE LA ANOTACION.
     * @return ENTIDAD DE LA ANOTACION.
     */
    private AnnotationEntity getAnnotationEntity(AnnotationLogic annotationLogic) {
        try {
            return annotationLogic.get(this.Id);
        } catch (Exception e) {
            return null;
        }
    }

    private void trim(ActionEvent e) {
        try {
            WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
            WorkspaceEntity workspaceEntity = workspaceLogic.get();

            AnnotationLogic annotationLogic = new AnnotationLogic(workspaceEntity.getAnnotationPath());

            TrimmerComponent.BoundingBox object = this.cmpTrimmerWindow.getObjectTrim();

            if (object == null) {
                throw new Exception("First, draw a valid object!");
            }
            DetectionEntity subDetectionEntity = DetectionEntity.createSubDetection(
                    annotationLogic.getNextSubDetection(this.Id),
                    Math.min(object.getXmin(), object.getXmax()),
                    Math.min(object.getYmin(), object.getYmax()),
                    Math.max(object.getXmin(), object.getXmax()),
                    Math.max(object.getYmin(), object.getYmax()));

            annotationLogic.addSubDetection(this.Id, subDetectionEntity);

            loadResource();
            changeIntensity();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void deleteTrim(ActionEvent e) {
        int result = MessageHelper.showWarningMessage("You are sure to delete the object?");
        if (MessageHelper.isWarningYes(result)) {
            try {
                if (this.IdSubDetection > 0) {
                    WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                    WorkspaceEntity workspaceEntity = workspaceLogic.get();
                    AnnotationLogic annotationLogic = new AnnotationLogic(workspaceEntity.getAnnotationPath());
                    annotationLogic.deleteSubDetection(this.Id, this.IdSubDetection);
                    loadResource();
                    changeIntensity();
                }
            } catch (Exception exception) {
                MessageHelper.showErrorMessage(exception);
            }
        }
    }

    private String getClassName(ArrayList<ClassEntity> classEntitys, int idClass) {
        Optional<ClassEntity> optional = classEntitys.stream().filter(p -> p.getId() == idClass)
                .findFirst();
        return optional.isPresent() ? optional.get().getName() : StringHelper.EMPTY;
    }

    private Color getClassColor(ArrayList<ClassEntity> classEntitys, int idClass) {
        Optional<ClassEntity> optional = classEntitys.stream().filter(p -> p.getId() == idClass)
                .findFirst();
        return optional.isPresent() ? optional.get().getEdgeColor() : Color.GREEN;
    }

    private void selectTrimObject(ListSelectionEvent e) {
        try {
            if (!e.getValueIsAdjusting()) {
                int selectedRow = this.tblTrimObjects.getSelectedRow();
                this.cmpTrimmerWindow.selectObject(selectedRow);
                this.IdSubDetection = TableHelper.getSelectedId(this.tblTrimObjects);
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }
}
