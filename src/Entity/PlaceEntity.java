package Entity;

import Helpers.StringHelper;

public class PlaceEntity {

    private final int Id;
    private String Country;
    private String Region;

    public int getId() {
        return Id;
    }

    public String getCountry() {
        return Country;
    }

    public String getRegion() {
        return Region;
    }

    public PlaceEntity() {
        this.Id = 0;
    }

    public PlaceEntity(int Id, String Country, String Region) {
        this.Id = Id;
        this.Country = Country.toUpperCase();
        this.Region = Region.toUpperCase();
    }

    public void updatePlace(String country, String region) {
        this.Country = country.toUpperCase();
        this.Region = region.toUpperCase();
    }

    public void validate() throws Exception {
        if (StringHelper.isNullOrEmpty(this.Country)) {
            throw new Exception("Country is required!");
        }
        if (StringHelper.isNullOrEmpty(this.Region)) {
            throw new Exception("Region is required!");
        }
    }
}
