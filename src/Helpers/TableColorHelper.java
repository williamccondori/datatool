package Helpers;

import Business.WorkspaceLogic;
import Presentation.ApplicationFrame;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class TableColorHelper extends DefaultTableCellRenderer {

    private final WorkspaceLogic WorkspaceLogic;

    public TableColorHelper() {
        this.WorkspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean selected, boolean focused, int row, int column) {
        super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        String valueCell = table.getValueAt(row, 0).toString();
        int idImage = Integer.parseInt(valueCell);
        Color stateColor = this.WorkspaceLogic.getColorStateByIdImage(idImage);
        this.setBackground(stateColor);
        this.setForeground(Color.BLACK);
        return this;
    }
}
