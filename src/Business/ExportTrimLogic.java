package Business;

import Entity.AnnotationEntity;
import Entity.ClassEntity;
import Entity.DatasetType;
import Entity.DetectionEntity;
import Entity.ExportType;
import Entity.ImageEntity;
import Entity.SettingsEntity;
import Entity.VocEntity;
import Entity.VocObjectEntity;
import Entity.VocSizeEntity;
import Entity.VocSourceEntity;
import Entity.WorkspaceEntity;
import Entity.YoloEntity;
import Entity.YoloObjectEntity;
import Helpers.CalculateHelper;
import Helpers.FileHelper;
import Helpers.StringHelper;
import Presentation.ApplicationFrame;
import Presentation.ExportTrimDatasetFrame;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.imageio.ImageIO;

public class ExportTrimLogic extends Thread {

    private DatasetType DatasetTipe;
    private WorkspaceEntity Workspace;
    private String ImageFolder;
    private String AnnotationFolder;

    public ExportTrimLogic() {
    }

    public ExportTrimLogic(String destinationPath, DatasetType datasetTipe) {
        try {
            WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
            this.Workspace = workspaceLogic.get();
            this.DatasetTipe = datasetTipe;
            createDatasetStructure(destinationPath);
        } catch (Exception exception) {
            errorWindow(exception);
        }
    }

    private void createDatasetStructure(String destinationPath) {
        switch (this.DatasetTipe) {
            case VOC_PASCAL:
                createDatasetStructureVocPascal(destinationPath);
                break;
            case YOLO:
                createDataSsetStructureYolo(destinationPath);
                break;
            default:
                errorWindow(new Exception("Data set type requiered!"));
                break;
        }
    }

    private void createDatasetStructureVocPascal(String destinationPath) {
        String projectFolderPath = String.format("%s/VOC_TRIM_%s", destinationPath, this.Workspace.getName());
        FileHelper.createFolder(projectFolderPath);

        String annotationsFolderPath = String.format("%s/Annotations", projectFolderPath);
        FileHelper.createFolder(annotationsFolderPath);

        String jpegImagesFolderPath = String.format("%s/JPEGImages", projectFolderPath);
        FileHelper.createFolder(jpegImagesFolderPath);

        this.AnnotationFolder = annotationsFolderPath;
        this.ImageFolder = jpegImagesFolderPath;
    }

    private void createDataSsetStructureYolo(String destinationPath) {
        String projectFolderPath = String.format("%s/YOLO_TRIM_%s", destinationPath, this.Workspace.getName());
        FileHelper.createFolder(projectFolderPath);

        String imagesFolderPath = String.format("%s/Images", projectFolderPath);
        FileHelper.createFolder(imagesFolderPath);

        this.AnnotationFolder = imagesFolderPath;
        this.ImageFolder = imagesFolderPath;
    }

    @Override
    public void run() {
        try {
            startWindow();

            ArrayList<ImageEntity> images = this.Workspace.getImages();
            ExportTrimDatasetFrame.proLog.setMaximum(images.size());

            AnnotationLogic annotationLogic = new AnnotationLogic(this.Workspace.getAnnotationPath());

            for (int i = 0; i < images.size(); i++) {
                try {
                    AnnotationEntity annotationEntity = annotationLogic.get(images.get(i).getId());
                    ArrayList<DetectionEntity> subDetections = annotationEntity.getSubDetections();

                    for (int j = 0; j < subDetections.size(); j++) {
                        String imageName = String.format("%s_%s", annotationEntity.getId(), j + 1);

                        int px = Math.min(subDetections.get(j).getXmin(), subDetections.get(j).getXmax());
                        int py = Math.min(subDetections.get(j).getYmin(), subDetections.get(j).getYmax());
                        int pw = Math.abs(subDetections.get(j).getXmin() - subDetections.get(j).getXmax());
                        int ph = Math.abs(subDetections.get(j).getYmin() - subDetections.get(j).getYmax());

                        File inputFile = new File(this.Workspace.getImagePath() + "/" + annotationEntity.getImageName());
                        BufferedImage image = ImageIO.read(inputFile);
                        BufferedImage subImage = image.getSubimage(px, py, pw, ph);
                        File outputfile = new File(String.format("%s/%s.jpg", this.ImageFolder, imageName));
                        ImageIO.write(subImage, "jpg", outputfile);

                        List<DetectionEntity> annotations = CalculateHelper.calculateNewBoudingBox(annotationEntity.getDetections(),
                                subDetections.get(j).getXmin(), subDetections.get(j).getXmax(),
                                subDetections.get(j).getYmin(), subDetections.get(j).getYmax(),
                                annotationEntity.getWidth(), annotationEntity.getHeight());
                        
                        generateAnnotations(annotations, pw, ph, 3, imageName);
                    }

                    ExportTrimDatasetFrame.proLog.setValue(i + 1);
                } catch (Exception exception) {
                    errorWindow(exception);
                }
            }
            finishWindow();
        } catch (Exception exception) {
            errorWindow(exception);
        }
    }

    private String getClassName(int idClass) {
        Optional<ClassEntity> optional = this.Workspace.getClasses()
                .stream().filter(p -> p.getId() == idClass)
                .findFirst();
        return optional.isPresent() ? optional.get().getName() : StringHelper.EMPTY;
    }

    private void generateAnnotations(List<DetectionEntity> detections, int width,
            int height, int depth, String imageName) throws Exception {
        switch (this.DatasetTipe) {
            case VOC_PASCAL:
                ArrayList<VocObjectEntity> vocObjects = new ArrayList<>();

                detections.stream().map(p -> new VocObjectEntity(
                        getClassName(p.getIdClass()),
                        p.getPose(),
                        p.getTruncated() ? 1 : 0,
                        p.getDifficult() ? 1 : 0,
                        p.getOccluded() ? 1 : 0,
                        p.getXmin(),
                        p.getXmax(),
                        p.getYmin(),
                        p.getYmax())).forEachOrdered((vocObject) -> {
                    vocObjects.add(vocObject);
                });

                VocEntity vocEntity = new VocEntity("folder",
                        imageName,
                        String.format("%s/%s", this.ImageFolder, imageName),
                        new VocSourceEntity(this.Workspace.getName()),
                        new VocSizeEntity(String.valueOf(width),
                                String.valueOf(height),
                                String.valueOf(depth)),
                        vocObjects.size() > 0 ? StringHelper.TRUE : StringHelper.FALSE,
                        vocObjects);

                ExportVocLogic exportVocLogic = new ExportVocLogic(this.AnnotationFolder);
                exportVocLogic.GenerateFile(vocEntity, imageName);
                break;
            case YOLO:
                ArrayList<YoloObjectEntity> yoloObjects = new ArrayList<>();

                detections.stream().map(p -> YoloObjectEntity.toYolo(
                        p.getIdClass(),
                        p.getXmin(),
                        p.getYmin(),
                        p.getXmax(),
                        p.getYmax(),
                        width,
                        height
                )).forEachOrdered((yoloObject) -> {
                    yoloObjects.add(yoloObject);
                });

                YoloEntity yoloEntity = new YoloEntity(yoloObjects);

                ExportYoloLogic exportYoloLogic = new ExportYoloLogic(this.AnnotationFolder);
                exportYoloLogic.GenerateFile(yoloEntity, imageName);
                break;
            default:
                errorWindow(new Exception("Data set type requiered!"));
                break;
        }
    }

    private void startWindow() {
        ExportTrimDatasetFrame.lblLog.setText("Exporting images, this may take a few minutes...");
        ExportTrimDatasetFrame.txtLog.setText(StringHelper.EMPTY);
        ExportTrimDatasetFrame.txtLog.setEditable(Boolean.FALSE);
        ExportTrimDatasetFrame.proLog.setMaximum(0);
        ExportTrimDatasetFrame.proLog.setValue(0);
        ExportTrimDatasetFrame.btnAccept.setEnabled(Boolean.FALSE);
    }

    private void finishWindow() {
        ExportTrimDatasetFrame.lblLog.setText("Completed!");
        ExportTrimDatasetFrame.proLog.setValue(ExportTrimDatasetFrame.proLog.getMaximum());
        printLog("Completed!");
        ExportTrimDatasetFrame.btnAccept.setEnabled(Boolean.TRUE);
    }

    private void errorWindow(Exception exception) {
        ExportTrimDatasetFrame.lblLog.setText("Error!");
        ExportTrimDatasetFrame.proLog.setMaximum(0);
        ExportTrimDatasetFrame.proLog.setValue(0);
        printLog(String.format("An error has occurred: %s", exception.getLocalizedMessage()));
        ExportTrimDatasetFrame.btnAccept.setEnabled(Boolean.TRUE);
    }

    private void printLog(String message) {
        ExportTrimDatasetFrame.txtLog.append(message + "\n");
        ExportTrimDatasetFrame.txtLog.setCaretPosition(ExportTrimDatasetFrame.txtLog.getText().length());
    }
}
