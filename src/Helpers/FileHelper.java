package Helpers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Locale;

public class FileHelper {

    /**
     * CREA UNA CARPETA EN UNA UBICACION PROPORCIONADA.
     *
     * @param path UBICACION DE LA NUEVA CARPETA.
     */
    public static void createFolder(String path) {
        File folder = new File(path);
        folder.mkdir();
    }

    /**
     * COPIA UN ARCHIVO DE UN LUGAR A OTRO.
     *
     * @param input UBICACION DE ENTRADA.
     * @param output UBICACION DE SALIDA.
     * @throws IOException EXCEPCION NO CONTROLADA.
     */
    public static void copyFile(File input, File output) throws IOException {
        Files.copy(input.toPath(), output.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * OBTIENE LA EXTENSION DE UN ARCHIVO.
     *
     * @param file ARCHIVO.
     * @return EXTENSION DEL ARCHIVO.
     */
    public static String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
            return extension.toLowerCase(Locale.ROOT);
        } else {
            return "";
        }
    }

    public static String getFileNameWithoutExtension(String fileName) {
        if (fileName == null) {
            return null;
        }
        int pos = fileName.lastIndexOf(".");
        if (pos == -1) {
            return fileName;
        }
        return fileName.substring(0, pos);
    }

    public static void deleteFolder(String workspacePath) {
        try {
            Files.delete(Paths.get(workspacePath));
        } catch (IOException exception) {
        }
    }
}
