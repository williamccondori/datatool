package Data;

import Entity.AnnotationEntity;
import Helpers.ApplicationSettings;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

public class AnnotationFileLogic {

    private final String AnnotationFolderPath;

    public AnnotationFileLogic(String AnnotationFolderPath) {
        this.AnnotationFolderPath = AnnotationFolderPath;
    }

    /**
     * LEE EL ARCHIVO DE ANOTACION.
     *
     * @param id ID DEL ARCHIVO DE ANOTACION.
     * @return ENTIDAD CON LOS DATOS DE LA ANOTACION.
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    public AnnotationEntity read(int id) throws Exception {
        String json;
        Gson gson = new Gson();
        try (FileReader fileReader = new FileReader(String.format("%s/%s.%s", this.AnnotationFolderPath, id, ApplicationSettings.APP_FILE_TYPE));
                BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            json = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        }
        return gson.fromJson(json, AnnotationEntity.class);
    }

    /**
     * ALMACENA EN EL ARCHIVO DE ANOTACION.
     *
     * @param id ID DEL ARCHIVO DE ANOTACION.
     * @param annotationEntity ENTIDAD CON LOS DATOS DE LA ANOTACION.
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    public void save(int id, AnnotationEntity annotationEntity) throws Exception {
        Gson gson = new Gson();
        try (FileWriter fileWriter = new FileWriter(String.format("%s/%s.%s", this.AnnotationFolderPath, id, ApplicationSettings.APP_FILE_TYPE));
                PrintWriter printWriter = new PrintWriter(fileWriter)) {
            String json = gson.toJson(annotationEntity);
            printWriter.println(json);
            printWriter.close();
            fileWriter.close();
        }
    }
}
