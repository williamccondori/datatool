package Helpers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

public class ChartHelper {

    public static void showPieChart(DefaultPieDataset defaultPieDataset, JPanel panel) {
        JFreeChart chart = ChartFactory.createPieChart(StringHelper.EMPTY, defaultPieDataset, true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(200, 150));
        chartPanel.setBackground(Color.BLACK);
        panel.removeAll();
        panel.setLayout(new BorderLayout());
        panel.add(chartPanel, BorderLayout.CENTER);
        panel.validate();
    }
}
