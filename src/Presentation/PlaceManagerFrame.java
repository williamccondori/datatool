package Presentation;

import Business.WorkspaceLogic;
import Entity.PlaceEntity;
import Helpers.ApplicationSettings;
import Helpers.MessageHelper;
import Helpers.StringHelper;
import Helpers.TableHelper;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Locale;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

public class PlaceManagerFrame extends javax.swing.JInternalFrame {

    private int LastRecord;

    public PlaceManagerFrame() {
        initComponents();
        loadWindow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrPlace = new javax.swing.JScrollPane();
        tblPlace = new javax.swing.JTable();
        lblId = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        lblRegion = new javax.swing.JLabel();
        txtRegion = new javax.swing.JTextField();
        lblDescription = new javax.swing.JLabel();
        cmbCountry = new javax.swing.JComboBox<>();
        btnClear = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-geo-cerca-16.png"))); // NOI18N

        tblPlace.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrPlace.setViewportView(tblPlace);

        lblId.setText("Id:");

        txtId.setText("jTextField1");

        lblRegion.setText("Region:");

        txtRegion.setText("jTextField2");

        lblDescription.setText("Country:");

        cmbCountry.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnClear.setBackground(new java.awt.Color(255, 255, 255));
        btnClear.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnClear.setForeground(new java.awt.Color(0, 102, 153));
        btnClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-borrar-16.png"))); // NOI18N
        btnClear.setText("Clear");
        btnClear.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnClear.setFocusPainted(false);

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 102, 153));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-guardar-16.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSave.setFocusPainted(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrPlace, javax.swing.GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDescription, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(lblRegion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 262, Short.MAX_VALUE))
                            .addComponent(cmbCountry, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtRegion)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblId)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDescription)
                    .addComponent(cmbCountry, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRegion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRegion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 251, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox<String> cmbCountry;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblRegion;
    private javax.swing.JScrollPane scrPlace;
    private javax.swing.JTable tblPlace;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtRegion;
    // End of variables declaration//GEN-END:variables

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
            loadData();
            loadForm();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA EL FUNCIONAMIENTO DE LOS CONTROLES DE LA VENTANA.
     */
    private void loadControl() {
        this.setSize(new Dimension(500, 500));
        this.setResizable(Boolean.FALSE);
        this.setTitle("Place manager");
        this.btnSave.addActionListener(this::save);
        this.btnClear.addActionListener(this::clear);
        this.txtId.setEditable(Boolean.FALSE);
        ListSelectionModel placeSelectionModel = this.tblPlace.getSelectionModel();
        placeSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        placeSelectionModel.addListSelectionListener(this::selectPlace);
    }

    /**
     * CARGA LOS DATOS UTILIZADOS EN LA VENTANA.
     *
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    public void loadData() throws Exception {
        loadPlaces();
        loadCountries();
    }

    /**
     * CARGA LOS DATOS PREDETERMINADOS PARA EL FORMULARIO.
     */
    private void loadForm() {
        this.txtId.setText(String.valueOf(this.LastRecord));
        this.cmbCountry.setSelectedIndex(0);
        this.txtRegion.setText(StringHelper.EMPTY);
    }

    /**
     * ASIGNA LOS VALORES A LA TABLA DE LOS LUGARES.
     *
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    private void loadPlaces() throws Exception {
        String columns[] = {"Id", "Region"};
        DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
        WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
        ArrayList<PlaceEntity> places = workspaceLogic.get().getPlaces();
        this.LastRecord = places.isEmpty() ? ApplicationSettings.PREFIX_ID : places.size() + ApplicationSettings.PREFIX_ID;
        places.forEach((p) -> {
            defaultTableModel.addRow(new Object[]{
                p.getId(),
                p.getRegion()
            });
        });
        this.tblPlace.setModel(defaultTableModel);
    }

    /**
     * ASIGNA LOS VALORES AL COMBO DE LOS PAISES.
     */
    private void loadCountries() {
        String[] countriesIso = Locale.getISOCountries();

        this.cmbCountry.removeAllItems();

        for (String countryIso : countriesIso) {
            Locale obj = new Locale(StringHelper.EMPTY, countryIso);
            String[] countries = {obj.getDisplayCountry()};
            for (String country : countries) {
                this.cmbCountry.addItem(StringHelper.cleanString(country).toUpperCase());
            }
        }
    }

    /**
     * SELECCIONA UNA FILA DE LA TABLA DE LOS LUGARES.
     *
     * @param e EVENTO.
     */
    private void selectPlace(ListSelectionEvent e) {
        try {
            if (!e.getValueIsAdjusting()) {
                int idPlace = TableHelper.getSelectedId(this.tblPlace);
                if (idPlace != 0) {
                    WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                    PlaceEntity placeEntity = workspaceLogic.getPlaceById(idPlace);
                    this.txtId.setText(String.valueOf(placeEntity.getId()));
                    this.cmbCountry.setSelectedItem(placeEntity.getCountry());
                    this.txtRegion.setText(placeEntity.getRegion());
                }
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * GAURDA LOS CAMBIOS REALIZADOS EN EL FORMULARIO.
     *
     * @param e EVENTO.
     */
    private void save(ActionEvent e) {
        int result = MessageHelper.showWarningMessage("Do you want to save the changes?");
        if (MessageHelper.isWarningYes(result)) {
            try {
                PlaceEntity placeEntity = new PlaceEntity(
                        Integer.parseInt(txtId.getText()), (String) cmbCountry.getSelectedItem(),
                        txtRegion.getText());
                placeEntity.validate();
                WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                workspaceLogic.savePlace(placeEntity);
                loadPlaces();
                loadForm();
                MessageHelper.showSuccessMessage("The changes were saved correctly!");
            } catch (Exception exception) {
                MessageHelper.showErrorMessage(exception);
            }
        }
    }

    /**
     * DESHACE LOS CAMBIOS REALIZADOS EN EL FORMULARIO.
     *
     * @param e EVENTO.
     */
    private void clear(ActionEvent e) {
        try {
            loadPlaces();
            loadForm();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }
}
