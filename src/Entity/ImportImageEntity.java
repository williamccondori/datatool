package Entity;

import Helpers.FileHelper;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class ImportImageEntity {

    private final String Name;
    private final String Path;
    private final String Extension;

    public String getName() {
        return Name;
    }

    public String getPath() {
        return Path;
    }

    public String getExtension() {
        return Extension;
    }

    public ImportImageEntity(String Name, String Path, String Extension) {
        this.Name = Name;
        this.Path = Path;
        this.Extension = Extension;
    }

    public static ArrayList<ImportImageEntity> getImportImages(boolean isDirectory, String imageInputPath) {
        ArrayList<ImportImageEntity> importImages = new ArrayList<>();
        String[] supportExtensions = {"bmp", "png", "jpeg", "jpg"};
        if (isDirectory) {
            File directory = new File(imageInputPath);
            File[] files = directory.listFiles();
            Arrays.sort(files);
            for (File file : files) {
                String fileName = file.getName();
                String fileExtension = FileHelper.getFileExtension(file);
                if (Arrays.stream(supportExtensions).anyMatch(p -> p.equals(fileExtension))) {
                    importImages.add(new ImportImageEntity(fileName, file.getAbsolutePath(), fileExtension));
                }
            }
        } else {
            File file = new File(imageInputPath);
            String fileName = file.getName();
            String fileExtension = FileHelper.getFileExtension(file);
            if (Arrays.stream(supportExtensions).anyMatch(p -> p.equals(fileExtension))) {
                importImages.add(new ImportImageEntity(fileName, file.getAbsolutePath(), fileExtension));
            }
        }
        return importImages;
    }
}
