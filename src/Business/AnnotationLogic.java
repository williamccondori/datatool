package Business;

import Data.AnnotationFileLogic;
import Entity.AnnotationEntity;
import Entity.DetectionEntity;
import Entity.MetadataEntity;
import Helpers.ApplicationSettings;
import Helpers.StringHelper;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import java.io.File;
import java.util.ArrayList;
import java.util.Optional;

public class AnnotationLogic {

    private final AnnotationFileLogic AnnotationFile;

    public AnnotationLogic(String AnnotationFolderPath) {
        this.AnnotationFile = new AnnotationFileLogic(AnnotationFolderPath);
    }

    public AnnotationEntity get(int id) throws Exception {
        return this.AnnotationFile.read(id);
    }

    public void generate(int id, File imageOutput, String imageInputName) throws Exception {
        AnnotationEntity annotationEntity = AnnotationEntity.generate(id,
                imageOutput.getName(), imageInputName, imageOutput.getName());
        MetadataEntity metadataEntity = getMetadata(imageOutput);
        annotationEntity.setProperties(
                metadataEntity.getFileTypeName(),
                metadataEntity.getFileTypeLongName(),
                metadataEntity.getFileTypeMime(),
                metadataEntity.getFileExtension(),
                metadataEntity.getFileSize(),
                metadataEntity.getWidth(),
                metadataEntity.getHeight(),
                metadataEntity.getNumberComponents());
        this.AnnotationFile.save(id, annotationEntity);
    }

    public void upadte(int id, AnnotationEntity annotation) throws Exception {
        AnnotationEntity annotationEntity = this.AnnotationFile.read(id);
        annotationEntity.update(annotation.getIdPlace(),
                annotation.getDescription());
        this.AnnotationFile.save(id, annotationEntity);
    }

    private MetadataEntity getMetadata(File imageFile) throws Exception {
        Metadata metadata = ImageMetadataReader.readMetadata(imageFile);
        MetadataEntity metadataEntity = new MetadataEntity();
        metadataEntity.setProperties(
                getMetadataByName(metadata, "Compression Type"),
                getMetadataByName(metadata, "Data Precision"),
                getMetadataByName(metadata, "Image Height"),
                getMetadataByName(metadata, "Image Width"),
                getMetadataByName(metadata, "Number of Components"),
                getMetadataByName(metadata, "Version"),
                getMetadataByName(metadata, "Resolution Units"),
                getMetadataByName(metadata, "X Resolution"),
                getMetadataByName(metadata, "Y Resolution"),
                getMetadataByName(metadata, "Thumbnail Width Pixels"),
                getMetadataByName(metadata, "Thumbnail Height Pixels"),
                getMetadataByName(metadata, "Number of Tables"),
                getMetadataByName(metadata, "Detected File Type Name"),
                getMetadataByName(metadata, "Detected File Type Long Name"),
                getMetadataByName(metadata, "Detected MIME Type"),
                getMetadataByName(metadata, "Expected File Name Extension"),
                getMetadataByName(metadata, "File Name"),
                getMetadataByName(metadata, "File Size"),
                getMetadataByName(metadata, "File Modified Date"),
                getMetadataByName(metadata, "Comment"));
        return metadataEntity;
    }

    private String getMetadataByName(Metadata metadata, String metadataName) {
        String metadataDescription = StringHelper.EMPTY;
        for (Directory directory : metadata.getDirectories()) {
            for (Tag tag : directory.getTags()) {
                if (metadataName.equals(tag.getTagName())) {
                    metadataDescription = tag.getDescription();
                }
            }
        }
        return metadataDescription;
    }

    public void addDetection(int id, DetectionEntity detectionEntity) throws Exception {
        AnnotationEntity annotationEntity = this.AnnotationFile.read(id);
        ArrayList<DetectionEntity> detections = annotationEntity.getDetections();
        detections.add(detectionEntity);
        annotationEntity.updateDetections(detections);
        this.AnnotationFile.save(id, annotationEntity);
    }

    public int getNextDetection(int id) throws Exception {
        AnnotationEntity annotationEntity = this.AnnotationFile.read(id);
        ArrayList<DetectionEntity> detections = annotationEntity.getDetections();
        Optional<DetectionEntity> optional = detections.stream().max(DetectionEntity.getComparatorById());
        return optional.isPresent() ? optional.get().getId() + 1 : ApplicationSettings.PREFIX_ID;
    }

    public void deleteDetection(int id, int idDetection) throws Exception {
        AnnotationEntity annotationEntity = this.AnnotationFile.read(id);
        ArrayList<DetectionEntity> detections = annotationEntity.getDetections();
        Optional<DetectionEntity> optional = detections.stream().filter(p -> p.getId() == idDetection).findFirst();

        if (!optional.isPresent()) {
            throw new Exception("The object was not found!");
        }

        detections.remove(optional.get());
        detections.forEach((detection) -> {
            if (detection.getId() > idDetection) {
                detection.updateId(detection.getId() - 1);
            }
        });

        annotationEntity.updateDetections(detections);
        this.AnnotationFile.save(id, annotationEntity);
    }

    public void addDetections(int id, ArrayList<DetectionEntity> detections) throws Exception {
        AnnotationEntity annotationEntity = this.AnnotationFile.read(id);
        annotationEntity.updateDetections(detections);
        this.AnnotationFile.save(id, annotationEntity);
    }

    public void addSubDetection(int id, DetectionEntity subDetectionEntity) throws Exception {
        AnnotationEntity annotationEntity = this.AnnotationFile.read(id);
        ArrayList<DetectionEntity> subDetections = annotationEntity.getSubDetections();
        subDetections.add(subDetectionEntity);
        annotationEntity.updateSubDetections(subDetections);
        this.AnnotationFile.save(id, annotationEntity);
    }

    public int getNextSubDetection(int id) throws Exception {
        AnnotationEntity annotationEntity = this.AnnotationFile.read(id);
        ArrayList<DetectionEntity> subDetections = annotationEntity.getSubDetections();
        Optional<DetectionEntity> optional = subDetections.stream().max(DetectionEntity.getComparatorById());
        return optional.isPresent() ? optional.get().getId() + 1 : ApplicationSettings.PREFIX_ID;
    }

    public void deleteSubDetection(int id, int idSubDetection) throws Exception {
        AnnotationEntity annotationEntity = this.AnnotationFile.read(id);
        ArrayList<DetectionEntity> subDetections = annotationEntity.getSubDetections();
        Optional<DetectionEntity> optional = subDetections.stream().filter(p -> p.getId() == idSubDetection).findFirst();

        if (!optional.isPresent()) {
            throw new Exception("The trim area was not found!");
        }

        subDetections.remove(optional.get());
        subDetections.forEach((subDetection) -> {
            if (subDetection.getId() > idSubDetection) {
                subDetection.updateId(subDetection.getId() - 1);
            }
        });

        annotationEntity.updateSubDetections(subDetections);
        this.AnnotationFile.save(id, annotationEntity);
    }

    public void addSubDetections(int id, ArrayList<DetectionEntity> subDetections) throws Exception {
        AnnotationEntity annotationEntity = this.AnnotationFile.read(id);
        annotationEntity.updateSubDetections(subDetections);
        this.AnnotationFile.save(id, annotationEntity);
    }
}
