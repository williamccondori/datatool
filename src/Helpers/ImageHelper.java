package Helpers;

import java.awt.Dimension;
import java.awt.Image;
import javax.swing.ImageIcon;

public class ImageHelper {

    /**
     * OBTIENE LA MINIATURA DE UNA IMAGEN.
     *
     * @param image IMAGEN DE ENTRADA.
     * @param size TAMANIO DE LA MINIATURA.
     * @return IMAGEN EN MINIATURA.
     */
    public static ImageIcon getThumbnail(Image image, Dimension size) {
        int imageWidth = image.getWidth(null);
        int imageHeight = image.getHeight(null);
        double scaleFactor = Math.min(1d, getScale(new Dimension(imageWidth, imageHeight), size));
        int scaleWidth = (int) Math.round(imageWidth * scaleFactor);
        int scaleHeight = (int) Math.round(imageHeight * scaleFactor);
        return new ImageIcon(image.getScaledInstance(scaleWidth, scaleHeight, Image.SCALE_SMOOTH));
    }

    /**
     * OBTIENE UNA ESCALA PARA LA REDIMENSION DE UNA IMAGEN.
     *
     * @param original TAMANIO ORIGINAL.
     * @param toFit TAMANIO A REDIMENSIONAR.
     * @return ESCALA.
     */
    public static double getScale(Dimension original, Dimension toFit) {
        double dScale = 1d;
        if (original != null && toFit != null) {

            double dScaleWidth = getScaleFactor(original.width, toFit.width);
            double dScaleHeight = getScaleFactor(original.height, toFit.height);
            dScale = Math.min(dScaleHeight, dScaleWidth);
        }
        return dScale;
    }

    /**
     * OBTIENE UN FACTOR DE ESCALAMIENTO.
     *
     * @param iMasterSize FACTOR DE ENTRADA.
     * @param iTargetSize FACTOR DE SALIDA.
     * @return FACTOR DE ESCALAMIENTO.
     */
    private static double getScaleFactor(int iMasterSize, int iTargetSize) {
        if (iMasterSize > iTargetSize) {
            return (double) iTargetSize / (double) iMasterSize;
        } else {
            return (double) iTargetSize / (double) iMasterSize;
        }
    }
}
