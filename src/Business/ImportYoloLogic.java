package Business;

import Entity.DetectionEntity;
import Helpers.ApplicationSettings;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class ImportYoloLogic {

    private final String AnnotationFolder;

    public ImportYoloLogic(String AnnotationFolder) {
        this.AnnotationFolder = AnnotationFolder;
    }

    public ArrayList<DetectionEntity> getDetections(String fileName, int widthImage, int heightImage) throws Exception {
        ArrayList<DetectionEntity> detections = new ArrayList<>();

        int idObject = ApplicationSettings.PREFIX_ID;

        try (Scanner input = new Scanner(new File(String.format("%s/%s", this.AnnotationFolder, fileName)))) {
            while (input.hasNextLine()) {
                String line = input.nextLine();
                String[] content = line.split(" ");
                int idClass = Integer.parseInt(content[0]);
                float xObject = Float.parseFloat(content[1]);
                float yObject = Float.parseFloat(content[2]);
                float wObject = Float.parseFloat(content[3]);
                float hObject = Float.parseFloat(content[4]);
                DetectionEntity detectionEntity = readYoloFormat(idObject, idClass, xObject, yObject, wObject, hObject, widthImage, heightImage);
                detections.add(detectionEntity);
                idObject++;
            }
            input.close();
        }

        return detections;
    }

    public static DetectionEntity readYoloFormat(int idObject, int idClass, float xObject, float yObject, float widthObject,
            float heightObject, int widthImage, int heightImage) {
        int xMin = (int) ((xObject * (float) widthImage) - ((widthObject * (float) widthImage) / 2f));
        int xMax = (int) ((xObject * (float) widthImage) + ((widthObject * (float) widthImage) / 2f));
        int yMin = (int) ((yObject * (float) heightImage) - ((heightObject * (float) heightImage) / 2f));
        int yMax = (int) ((yObject * (float) heightImage) + ((heightObject * (float) heightImage) / 2f));

        DetectionEntity detectionEntity = DetectionEntity.setObjectData(idObject, ApplicationSettings.PREFIX_ID + idClass, 0, 0,
                "Frontal", Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
        detectionEntity.setObjectDetection(xMin, yMin, xMax, yMax);
        return detectionEntity;
    }
}
