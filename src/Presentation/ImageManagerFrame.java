package Presentation;

import Helpers.TableColorHelper;
import Entity.ComboEntity;
import Business.AnnotationLogic;
import Business.ImportImageLogic;
import Business.WorkspaceLogic;
import Entity.AnnotationEntity;
import Entity.ImageEntity;
import Entity.PlaceEntity;
import Entity.StateEntity;
import Entity.WorkspaceEntity;
import Helpers.ImageHelper;
import Helpers.MessageHelper;
import Helpers.StringHelper;
import Helpers.SystemHelper;
import Helpers.TableHelper;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Optional;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

public class ImageManagerFrame extends javax.swing.JInternalFrame {

    private int Id;

    public ImageManagerFrame() {
        this.Id = 0;
        initComponents();
        loadWindow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgImportImage = new javax.swing.JDialog();
        lblLog = new javax.swing.JLabel();
        scrLog = new javax.swing.JScrollPane();
        txtLog = new javax.swing.JTextArea();
        proLog = new javax.swing.JProgressBar();
        btnAccept = new javax.swing.JButton();
        scrImage = new javax.swing.JScrollPane();
        tblImage = new javax.swing.JTable();
        tabPrincipal = new javax.swing.JTabbedPane();
        panGeneral = new javax.swing.JPanel();
        lblImageName = new javax.swing.JLabel();
        lblImageIcon = new javax.swing.JLabel();
        txtImageName = new javax.swing.JLabel();
        lblAnnotationName = new javax.swing.JLabel();
        txtAnnotationName = new javax.swing.JLabel();
        lblState = new javax.swing.JLabel();
        txtState = new javax.swing.JLabel();
        btnShowImageTrimmer = new javax.swing.JButton();
        btnShowImageDetection = new javax.swing.JButton();
        panDetails = new javax.swing.JPanel();
        scrDetails = new javax.swing.JScrollPane();
        tblDetails = new javax.swing.JTable();
        panCustomize = new javax.swing.JPanel();
        lblDescription = new javax.swing.JLabel();
        txtDescription = new javax.swing.JTextField();
        lblPlace = new javax.swing.JLabel();
        cmbPlace = new javax.swing.JComboBox<>();
        lblId = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        btnRefreshPlace = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        tooFile = new javax.swing.JToolBar();
        btnImportFile = new javax.swing.JButton();
        btnImportFolder = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();

        lblLog.setText("jLabel1");

        txtLog.setBackground(new java.awt.Color(51, 51, 51));
        txtLog.setColumns(20);
        txtLog.setFont(new java.awt.Font("Consolas", 0, 13)); // NOI18N
        txtLog.setForeground(new java.awt.Color(255, 255, 255));
        txtLog.setRows(5);
        txtLog.setText("s");
        scrLog.setViewportView(txtLog);

        btnAccept.setBackground(new java.awt.Color(255, 255, 255));
        btnAccept.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnAccept.setForeground(new java.awt.Color(0, 102, 153));
        btnAccept.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-de-acuerdo-16.png"))); // NOI18N
        btnAccept.setText("Accept");
        btnAccept.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnAccept.setFocusPainted(false);

        javax.swing.GroupLayout dlgImportImageLayout = new javax.swing.GroupLayout(dlgImportImage.getContentPane());
        dlgImportImage.getContentPane().setLayout(dlgImportImageLayout);
        dlgImportImageLayout.setHorizontalGroup(
            dlgImportImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgImportImageLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgImportImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrLog, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(lblLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(proLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgImportImageLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dlgImportImageLayout.setVerticalGroup(
            dlgImportImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgImportImageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblLog)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(proLog, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrLog, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-iconos-medianos-16.png"))); // NOI18N

        tblImage.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrImage.setViewportView(tblImage);

        lblImageName.setText("Image name:");

        txtImageName.setText("jLabel1");

        lblAnnotationName.setText("Annotation name:");

        txtAnnotationName.setText("jLabel3");

        lblState.setText("State:");

        txtState.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtState.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtState.setText("jLabel5");

        btnShowImageTrimmer.setBackground(new java.awt.Color(255, 255, 255));
        btnShowImageTrimmer.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnShowImageTrimmer.setForeground(new java.awt.Color(0, 102, 153));
        btnShowImageTrimmer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cortar-papel-16.png"))); // NOI18N
        btnShowImageTrimmer.setText("Image clipper");
        btnShowImageTrimmer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnShowImageTrimmer.setFocusPainted(false);

        btnShowImageDetection.setBackground(new java.awt.Color(255, 255, 255));
        btnShowImageDetection.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnShowImageDetection.setForeground(new java.awt.Color(0, 102, 153));
        btnShowImageDetection.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-agrupar-objetos-16.png"))); // NOI18N
        btnShowImageDetection.setText("Image detection");
        btnShowImageDetection.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnShowImageDetection.setFocusPainted(false);

        javax.swing.GroupLayout panGeneralLayout = new javax.swing.GroupLayout(panGeneral);
        panGeneral.setLayout(panGeneralLayout);
        panGeneralLayout.setHorizontalGroup(
            panGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panGeneralLayout.createSequentialGroup()
                        .addComponent(lblImageIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblImageName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtImageName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblAnnotationName, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                            .addComponent(txtAnnotationName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblState, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtState, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panGeneralLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnShowImageDetection, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panGeneralLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnShowImageTrimmer, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panGeneralLayout.setVerticalGroup(
            panGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panGeneralLayout.createSequentialGroup()
                        .addComponent(lblImageName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtImageName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblAnnotationName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAnnotationName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblState)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtState, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblImageIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 174, Short.MAX_VALUE)
                .addComponent(btnShowImageTrimmer, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnShowImageDetection, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabPrincipal.addTab("General", panGeneral);

        tblDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrDetails.setViewportView(tblDetails);

        javax.swing.GroupLayout panDetailsLayout = new javax.swing.GroupLayout(panDetails);
        panDetails.setLayout(panDetailsLayout);
        panDetailsLayout.setHorizontalGroup(
            panDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrDetails, javax.swing.GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
                .addContainerGap())
        );
        panDetailsLayout.setVerticalGroup(
            panDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrDetails, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabPrincipal.addTab("Details", panDetails);

        lblDescription.setText("Description:");

        txtDescription.setText("jTextField1");

        lblPlace.setText("Place:");

        lblId.setText("Id:");

        txtId.setText("jTextField1");

        btnRefreshPlace.setBackground(new java.awt.Color(255, 255, 255));
        btnRefreshPlace.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnRefreshPlace.setForeground(new java.awt.Color(0, 102, 153));
        btnRefreshPlace.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-reiniciar-16.png"))); // NOI18N
        btnRefreshPlace.setToolTipText("Refresh");
        btnRefreshPlace.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnRefreshPlace.setFocusPainted(false);
        btnRefreshPlace.setFocusable(false);
        btnRefreshPlace.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 102, 153));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-guardar-16.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSave.setFocusPainted(false);

        javax.swing.GroupLayout panCustomizeLayout = new javax.swing.GroupLayout(panCustomize);
        panCustomize.setLayout(panCustomizeLayout);
        panCustomizeLayout.setHorizontalGroup(
            panCustomizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panCustomizeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panCustomizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDescription)
                    .addComponent(lblPlace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDescription, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panCustomizeLayout.createSequentialGroup()
                        .addComponent(lblId, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 191, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panCustomizeLayout.createSequentialGroup()
                        .addComponent(cmbPlace, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefreshPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panCustomizeLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panCustomizeLayout.setVerticalGroup(
            panCustomizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panCustomizeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panCustomizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblId)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPlace)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panCustomizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnRefreshPlace, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(cmbPlace))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDescription)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 238, Short.MAX_VALUE)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabPrincipal.addTab("Customize", panCustomize);

        tooFile.setRollover(true);

        btnImportFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-añadir-imagen-16.png"))); // NOI18N
        btnImportFile.setText("Import file");
        btnImportFile.setFocusable(false);
        btnImportFile.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooFile.add(btnImportFile);

        btnImportFolder.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-agregar-carpeta-16.png"))); // NOI18N
        btnImportFolder.setText("Import folder");
        btnImportFolder.setFocusable(false);
        btnImportFolder.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooFile.add(btnImportFolder);

        btnRefresh.setBackground(new java.awt.Color(255, 255, 255));
        btnRefresh.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnRefresh.setForeground(new java.awt.Color(0, 102, 153));
        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-reiniciar-16.png"))); // NOI18N
        btnRefresh.setToolTipText("Refresh");
        btnRefresh.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnRefresh.setFocusPainted(false);
        btnRefresh.setFocusable(false);
        btnRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrImage, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabPrincipal)
                .addContainerGap())
            .addComponent(tooFile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(tooFile, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tabPrincipal)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scrImage, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnAccept;
    private javax.swing.JButton btnImportFile;
    private javax.swing.JButton btnImportFolder;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnRefreshPlace;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnShowImageDetection;
    private javax.swing.JButton btnShowImageTrimmer;
    private javax.swing.JComboBox<ComboEntity> cmbPlace;
    private javax.swing.JDialog dlgImportImage;
    private javax.swing.JLabel lblAnnotationName;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblImageIcon;
    private javax.swing.JLabel lblImageName;
    public static javax.swing.JLabel lblLog;
    private javax.swing.JLabel lblPlace;
    private javax.swing.JLabel lblState;
    private javax.swing.JPanel panCustomize;
    private javax.swing.JPanel panDetails;
    private javax.swing.JPanel panGeneral;
    public static javax.swing.JProgressBar proLog;
    private javax.swing.JScrollPane scrDetails;
    private javax.swing.JScrollPane scrImage;
    private javax.swing.JScrollPane scrLog;
    private javax.swing.JTabbedPane tabPrincipal;
    private javax.swing.JTable tblDetails;
    private javax.swing.JTable tblImage;
    private javax.swing.JToolBar tooFile;
    private javax.swing.JLabel txtAnnotationName;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtId;
    private javax.swing.JLabel txtImageName;
    public static javax.swing.JTextArea txtLog;
    private javax.swing.JLabel txtState;
    // End of variables declaration//GEN-END:variables

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
            loadData();
            loadForm();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA EL FUNCIONAMIENTO DE LOS CONTROLES DE LA VENTANA.
     */
    private void loadControl() {
        this.setSize(new Dimension(600, 500));
        this.setTitle("Image manager");
        this.btnImportFile.addActionListener(this::importFile);
        this.btnImportFolder.addActionListener(this::importFolder);
        this.btnRefresh.addActionListener(this::refresh);
        this.btnShowImageTrimmer.addActionListener(this::showImageTrimmer);
        this.btnShowImageDetection.addActionListener(this::showImageDetection);
        this.btnRefreshPlace.addActionListener(this::refreshPlace);
        this.btnSave.addActionListener(this::save);
        ImageManagerFrame.btnAccept.addActionListener(this::accept);
        this.lblImageIcon.setOpaque(Boolean.TRUE);
        this.lblImageIcon.setBackground(new Color(38, 50, 56));
        this.txtState.setOpaque(Boolean.TRUE);
        this.tooFile.setFloatable(Boolean.FALSE);
        ListSelectionModel classSelectionModel = this.tblImage.getSelectionModel();
        classSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        classSelectionModel.addListSelectionListener(this::selectClass);
    }

    /**
     * CARGA LOS DATOS UTILIZADOS EN LA VENTANA.
     *
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    public void loadData() throws Exception {
        loadImages();
        loadPlaces();
    }

    /**
     * CARGA LOS DATOS PREDETERMINADOS PARA EL FORMULARIO.
     */
    private void loadForm() {
        this.txtImageName.setText(StringHelper.EMPTY);
        this.txtAnnotationName.setText(StringHelper.EMPTY);
        this.txtState.setText(StringHelper.EMPTY);
        this.txtState.setBackground(new Color(38, 50, 56));
        this.lblImageIcon.setIcon(null);

        String columns[] = {"Property", "Value"};
        DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
        defaultTableModel.addRow(new Object[]{"Id", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"Input file name", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"Output file name", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"Image name", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"Annotation name", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"Width", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"Height", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"Depth", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"File type:", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"File type large:", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"File type MIME:", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"Extension:", StringHelper.EMPTY});
        defaultTableModel.addRow(new Object[]{"File size:", StringHelper.EMPTY});
        this.tblDetails.setModel(defaultTableModel);

        this.txtId.setEditable(Boolean.FALSE);
        this.txtId.setText(StringHelper.EMPTY);
        this.cmbPlace.setSelectedIndex(0);
        this.txtDescription.setText(StringHelper.EMPTY);
    }

    /**
     * ASIGNA LOS VALORES A LA TABLA DE LAS IMAGENES.
     *
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    private void loadImages() throws Exception {
        String columns[] = {"Id"};
        DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
        WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
        ArrayList<ImageEntity> images = workspaceLogic.get().getImages();
        images.forEach((p) -> {
            defaultTableModel.addRow(new Object[]{
                p.getId()
            });
        });
        TableColorHelper tableColor = new TableColorHelper();
        this.tblImage.setDefaultRenderer(Object.class, tableColor);
        this.tblImage.setModel(defaultTableModel);
    }

    /**
     * ASIGNA LOS VALORES AL COMBO DE LOS LUGARES.
     *
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    private void loadPlaces() throws Exception {
        this.cmbPlace.removeAllItems();
        this.cmbPlace.addItem(ComboEntity.getDefault());
        WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
        WorkspaceEntity workspaceEntity = workspaceLogic.get();
        workspaceEntity.getPlaces().forEach(p -> {
            this.cmbPlace.addItem(new ComboEntity(p.getId(), String.format("[%s] %s", p.getCountry(), p.getRegion())));
        });
    }

    /**
     * SELECCIONA UNA FILA DE LA TABLA DE LAS IMAGENES.
     *
     * @param e EVENTO.
     */
    private void selectClass(ListSelectionEvent e) {
        try {
            if (!e.getValueIsAdjusting()) {
                int idImage = TableHelper.getSelectedId(this.tblImage);
                if (idImage != 0) {
                    WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                    WorkspaceEntity workspaceEntity = workspaceLogic.get();
                    AnnotationLogic annotationLogic = new AnnotationLogic(workspaceEntity.getAnnotationPath());
                    AnnotationEntity annotationEntity = annotationLogic.get(idImage);

                    this.Id = annotationEntity.getId();
                    this.txtId.setText(String.valueOf(annotationEntity.getId()));
                    this.txtImageName.setText(annotationEntity.getImageName());
                    this.txtAnnotationName.setText(annotationEntity.getAnnotationName());

                    // ASIGNACION DEL VALOR DEL ESTADO.
                    Optional<ImageEntity> resultImage = workspaceEntity.getImages()
                            .stream().filter(p -> p.getId() == idImage).findFirst();
                    if (resultImage.isPresent()) {
                        Optional<StateEntity> resultState = workspaceEntity.getStates()
                                .stream().filter(p -> p.getId() == resultImage.get().getIdState()).findFirst();
                        this.txtState.setText(resultState.isPresent() ? resultState.get().getName() : StringHelper.EMPTY);
                        this.txtState.setBackground(resultState.isPresent() ? resultState.get().getDisplayColor() : Color.WHITE);
                    }

                    //ASIGNACION DE LOS VALORES GENERALES.
                    String imagePath = String.format("%s/%s", workspaceEntity.getImagePath(), annotationEntity.getImageName());
                    Image image = ImageIO.read(new File(imagePath));
                    ImageIcon imageIcon = ImageHelper.getThumbnail(image, this.lblImageIcon.getSize());
                    image.flush();
                    this.lblImageIcon.setIcon(imageIcon);

                    //ASIGNACION DE LOS VALORES PARA LOS DETALLES.
                    String columns[] = {"Property", "Value"};
                    DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
                    defaultTableModel.addRow(new Object[]{"Id", annotationEntity.getId()});
                    defaultTableModel.addRow(new Object[]{"Input file name", annotationEntity.getFileNameInput()});
                    defaultTableModel.addRow(new Object[]{"Output file name", annotationEntity.getFileNameOutput()});
                    defaultTableModel.addRow(new Object[]{"Image name", annotationEntity.getImageName()});
                    defaultTableModel.addRow(new Object[]{"Annotaion name", annotationEntity.getAnnotationName()});
                    defaultTableModel.addRow(new Object[]{"Width", annotationEntity.getWidth()});
                    defaultTableModel.addRow(new Object[]{"Height", annotationEntity.getHeight()});
                    defaultTableModel.addRow(new Object[]{"Depth", annotationEntity.getDepth()});
                    defaultTableModel.addRow(new Object[]{"File type:", annotationEntity.getFileType()});
                    defaultTableModel.addRow(new Object[]{"File type large:", annotationEntity.getFileTypeLarge()});
                    defaultTableModel.addRow(new Object[]{"File type MIME:", annotationEntity.getFileTypeMime()});
                    defaultTableModel.addRow(new Object[]{"Extension:", annotationEntity.getExtension()});
                    defaultTableModel.addRow(new Object[]{"File size:", annotationEntity.getFileSize()});
                    this.tblDetails.setModel(defaultTableModel);

                    //ASIGNACION DE LOS VALORES PERSONALIZADOS
                    PlaceEntity placeEntity = workspaceLogic.getPlaceById(annotationEntity.getIdPlace());
                    setSelectedPlace(placeEntity);
                    this.txtDescription.setText(annotationEntity.getDescription());
                }
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * SELECCIONA UN ELEMENTO EN EL COMBO DE LUGARES DE ACUERDO AL MODELO
     * SELECCIONADO.
     *
     * @param placeEntity MODELO DEL LUGAR.
     */
    private void setSelectedPlace(PlaceEntity placeEntity) {
        if (placeEntity == null) {
            this.cmbPlace.setSelectedIndex(0);
        } else {
            for (int i = 0; i < this.cmbPlace.getItemCount(); i++) {
                ComboEntity comboModel = this.cmbPlace.getItemAt(i);
                if (comboModel.getValue() == placeEntity.getId()) {
                    this.cmbPlace.setSelectedIndex(i);
                }
            }
        }
    }

    /**
     * MUESTRA UN CUADRO DE DIALOGO QUE PERMITE IMPORTAR UNA IMAGEN.
     *
     * @param e EVENTO.
     */
    private void importFile(ActionEvent e) {
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File(SystemHelper.getUserHome()));
            fileChooser.setDialogTitle("Choose a file");
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                showImportImageWindow();
                String imageInputPath = fileChooser.getSelectedFile().getPath();
                ImportImageLogic importImageLogic = new ImportImageLogic(imageInputPath, Boolean.FALSE);
                importImageLogic.start();
            }
        } catch (HeadlessException exception) {
            this.dlgImportImage.dispose();
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UN CUADRO DE DIALOGO QUE PERMITE IMPORTAR UNA CARPETA DE
     * IMAGENES.
     *
     * @param e EVENTO.
     */
    private void importFolder(ActionEvent e) {
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File(SystemHelper.getUserHome()));
            fileChooser.setDialogTitle("Choose a direcory");
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                showImportImageWindow();
                String imageInputPath = fileChooser.getSelectedFile().getPath();
                ImportImageLogic importImageLogic = new ImportImageLogic(imageInputPath, Boolean.TRUE);
                importImageLogic.start();
            }
        } catch (HeadlessException exception) {
            this.dlgImportImage.dispose();
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * VUELVE A CONSULTAR EL LISTADO DE LAS IMAGENES.
     *
     * @param e EVENTO.
     */
    private void refresh(ActionEvent e) {
        try {
            loadImages();
            loadForm();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * VUELVE A CONSULTAR EL LISTADO DE LOS LUGARES.
     *
     * @param e EVENTO.
     */
    private void refreshPlace(ActionEvent e) {
        try {
            int selectedIndex = this.cmbPlace.getSelectedIndex();
            loadPlaces();
            this.cmbPlace.setSelectedIndex(selectedIndex);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void showImageTrimmer(ActionEvent e) {
        try {
            if (this.Id != 0) {
                ImageClipperFrame imageTrimmerFrame = new ImageClipperFrame(this.Id);
                showInternalWindow(imageTrimmerFrame);
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void showImageDetection(ActionEvent e) {
        try {
            if (this.Id != 0) {
                ImageDetectionFrame imageDetectionFrame = new ImageDetectionFrame(this.Id);
                showInternalWindow(imageDetectionFrame);
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * GAURDA LOS CAMBIOS REALIZADOS EN EL FORMULARIO.
     *
     * @param e EVENTO.
     */
    private void save(ActionEvent e) {
        int result = MessageHelper.showWarningMessage("Do you want to save the changes?");
        if (MessageHelper.isWarningYes(result)) {
            try {
                int id = Integer.parseInt(StringHelper.isNullOrEmpty(txtId.getText()) ? "0" : txtId.getText());
                if (id != 0) {
                    WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                    WorkspaceEntity workspaceEntity = workspaceLogic.get();
                    AnnotationLogic annotationLogic = new AnnotationLogic(workspaceEntity.getAnnotationPath());
                    ComboEntity comboModel = (ComboEntity) this.cmbPlace.getSelectedItem();
                    String description = this.txtDescription.getText();
                    AnnotationEntity annotationEntity = AnnotationEntity.setCustomProperties(
                            comboModel.getValue(),
                            description
                    );
                    annotationLogic.upadte(id, annotationEntity);
                    MessageHelper.showSuccessMessage("The changes were saved correctly!");
                }
            } catch (Exception exception) {
                MessageHelper.showErrorMessage(exception);
            }
        }
    }

    /**
     * CIERRA EL CUADRO DE DIALOGO DE LA IMPORTACION.
     *
     * @param e EVENTO.
     */
    private void accept(ActionEvent e) {
        try {
            this.dlgImportImage.dispose();
            loadImages();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA EL CUADRO DE DIALOGO DE LA IMPORTACION.
     */
    private void showImportImageWindow() {
        this.dlgImportImage.setVisible(Boolean.TRUE);
        this.dlgImportImage.setSize(new Dimension(400, 300));
        this.dlgImportImage.setAlwaysOnTop(Boolean.TRUE);
        this.dlgImportImage.setResizable(Boolean.FALSE);
        this.dlgImportImage.setLocationRelativeTo(null);
        this.dlgImportImage.setTitle("Import images to workspace");
        try {
            ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Icons/icons8-tarea-del-sistema-100.png"));
            this.dlgImportImage.setIconImage(imageIcon.getImage());
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UNA VENTANA DENTRO DEL COMPONENTE DE ESCRITORIO.
     *
     * @param window VENTANA A MOSTRAR.
     */
    private void showInternalWindow(JInternalFrame window) {
        ApplicationFrame.desPrincipal.add(window);
        Dimension desktopSize = ApplicationFrame.desPrincipal.getSize();
        Dimension windowSize = window.getSize();
        window.setLocation((int) ((desktopSize.getWidth() - windowSize.getWidth()) / 2), (int) ((desktopSize.getHeight() - windowSize.getHeight()) / 2));
        window.setVisible(true);
    }
}
