package Business;

import Data.WorkspaceFileLogic;
import Entity.ClassEntity;
import Entity.ImageEntity;
import Entity.PlaceEntity;
import Entity.SettingsEntity;
import Entity.StateEntity;
import Entity.WorkspaceEntity;
import Helpers.ApplicationSettings;
import Helpers.FileHelper;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Optional;

public class WorkspaceLogic {

    private final WorkspaceFileLogic WorkspaceFile;

    public WorkspaceLogic(String WorkspaceFolderPath) {
        this.WorkspaceFile = new WorkspaceFileLogic(WorkspaceFolderPath);
    }

    public void inport(WorkspaceEntity workspaceEntity) throws Exception {
        FileHelper.createFolder(workspaceEntity.getPath());
        FileHelper.createFolder(workspaceEntity.getImagePath());
        FileHelper.createFolder(workspaceEntity.getAnnotationPath());
        this.WorkspaceFile.save(workspaceEntity);
    }

    public void create(WorkspaceEntity workspaceEntity) throws Exception {
        FileHelper.createFolder(workspaceEntity.getPath());
        FileHelper.createFolder(workspaceEntity.getImagePath());
        FileHelper.createFolder(workspaceEntity.getAnnotationPath());
        this.WorkspaceFile.save(workspaceEntity);
    }

    public String open(WorkspaceEntity workspaceEntity) throws Exception {
        this.WorkspaceFile.save(workspaceEntity);
        return workspaceEntity.getPath();
    }

    public WorkspaceEntity get() throws Exception {
        return this.WorkspaceFile.read();
    }

    public void saveClass(ClassEntity classEntity) throws Exception {
        WorkspaceEntity workspaceEntity = this.WorkspaceFile.read();
        ArrayList<ClassEntity> classes = workspaceEntity.getClasses();
        boolean existClass = classes.stream()
                .anyMatch(p -> classEntity.getId() == p.getId());
        if (!existClass) {
            workspaceEntity.createClass(classEntity);
        } else {
            classes.forEach((ClassEntity p) -> {
                if (p.getId() == classEntity.getId()) {
                    p.updateClass(classEntity.getName(),
                            classEntity.getDescription(),
                            classEntity.getCustomName(),
                            classEntity.getDisplayColor(),
                            classEntity.getEdgeColor(),
                            classEntity.getSegmentationColor());
                }
            });
            workspaceEntity.updateClasses(classes);
        }
        this.WorkspaceFile.save(workspaceEntity);
    }

    public void savePlace(PlaceEntity placeEntity) throws Exception {
        WorkspaceEntity workspaceEntity = this.WorkspaceFile.read();
        ArrayList<PlaceEntity> places = workspaceEntity.getPlaces();
        boolean existPlace = places.stream()
                .anyMatch(p -> placeEntity.getId() == p.getId());
        if (!existPlace) {
            workspaceEntity.createPlace(placeEntity);
        } else {
            places.forEach((PlaceEntity p) -> {
                if (p.getId() == placeEntity.getId()) {
                    p.updatePlace(placeEntity.getCountry(),
                            placeEntity.getRegion());
                }
            });
            workspaceEntity.updatePlaces(places);
        }
        this.WorkspaceFile.save(workspaceEntity);
    }

    public void saveState(StateEntity stateEntity) throws Exception {
        WorkspaceEntity workspaceEntity = this.WorkspaceFile.read();
        ArrayList<StateEntity> states = workspaceEntity.getStates();
        boolean existPlace = states.stream()
                .anyMatch(p -> stateEntity.getId() == p.getId());
        if (!existPlace) {
            workspaceEntity.createState(stateEntity);
        } else {
            states.forEach((StateEntity p) -> {
                if (p.getId() == stateEntity.getId()) {
                    p.updateState(stateEntity.getName(),
                            stateEntity.getDescription(),
                            stateEntity.getDisplayColor());
                }
            });
            workspaceEntity.updateStates(states);
        }
        this.WorkspaceFile.save(workspaceEntity);
    }

    public void registerImage(ImageEntity imageEntity) throws Exception {
        WorkspaceEntity workspaceEntity = this.WorkspaceFile.read();
        workspaceEntity.createImage(imageEntity);
        this.WorkspaceFile.save(workspaceEntity);
    }

    public ClassEntity getClassById(int id) throws Exception {
        ArrayList<ClassEntity> classes = this.get().getClasses();
        Optional<ClassEntity> result = classes.stream().filter(p -> p.getId() == id).findFirst();
        return result.isPresent() ? result.get() : null;
    }

    public PlaceEntity getPlaceById(int id) throws Exception {
        ArrayList<PlaceEntity> places = this.get().getPlaces();
        Optional<PlaceEntity> result = places.stream().filter(p -> p.getId() == id).findFirst();
        return result.isPresent() ? result.get() : null;
    }

    public StateEntity getStateById(int id) throws Exception {
        ArrayList<StateEntity> states = this.get().getStates();
        Optional<StateEntity> result = states.stream().filter(p -> p.getId() == id).findFirst();
        return result.isPresent() ? result.get() : null;
    }

    public int getLastImageId() throws Exception {
        int lastImageId = ApplicationSettings.PREFIX_ID;
        WorkspaceEntity workspaceEntity = this.WorkspaceFile.read();
        ArrayList<ImageEntity> images = workspaceEntity.getImages();
        if (!images.isEmpty()) {
            ImageEntity imageIndexEntity = images.get(images.size() - 1);
            lastImageId = imageIndexEntity.getId();
        }
        return !images.isEmpty() ? lastImageId + 1 : lastImageId;
    }

    public Color getColorStateByIdImage(int idImage) {
        Color color = Color.WHITE;
        try {
            WorkspaceEntity workspaceEntity = this.WorkspaceFile.read();
            Optional<ImageEntity> result = workspaceEntity.getImages().stream().filter(p -> p.getId() == idImage).findFirst();
            if (result.isPresent()) {
                int idState = result.get().getIdState();
                Optional<StateEntity> resultState = workspaceEntity.getStates().stream().filter(p -> p.getId() == idState).findFirst();
                if (resultState.isPresent()) {
                    color = resultState.get().getDisplayColor();
                }
            }
        } catch (Exception exception) {
            System.err.println(exception.getLocalizedMessage());
        }
        return color;
    }

    public void updateImages(ArrayList<ImageEntity> images) throws Exception {
        WorkspaceEntity workspace = this.WorkspaceFile.read();
        workspace.updateImages(images);
        this.WorkspaceFile.save(workspace);
    }

    public void updateAuthor(WorkspaceEntity workspaceEntity) throws Exception {
        WorkspaceEntity workspace = this.WorkspaceFile.read();
        workspace.update(workspaceEntity.getAuthor());
        this.WorkspaceFile.save(workspace);
    }

    public void updateSettings(SettingsEntity settingsEntity) throws Exception {
        WorkspaceEntity workspaceEntity = this.WorkspaceFile.read();
        SettingsEntity settings = workspaceEntity.getSettings();
        settings.update(settingsEntity.getTrain(),
                settingsEntity.getTest(),
                settingsEntity.getValidation(),
                settingsEntity.isRandom());
        workspaceEntity.updateSettings(settings);
        this.WorkspaceFile.save(workspaceEntity);
    }

    public void updateState(ImageEntity imageEntity, int idImage) throws Exception {
        WorkspaceEntity workspace = this.WorkspaceFile.read();
        ArrayList<ImageEntity> images = workspace.getImages();
        images.forEach(p -> {
            if (p.getId() == idImage) {
                p.update(imageEntity.getIdState());
            }
        });
        workspace.updateImages(images);
        this.WorkspaceFile.save(workspace);
    }
}
