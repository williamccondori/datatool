package Helpers;

public class ApplicationSettings {

    public static int PREFIX_ID = 100000001;
    public static final String APP_FILE = "app";
    public static final String APP_FILE_TYPE = "json";
    public static final String LOOK_AND_FEEL = "Metal";
    
    public static final int TRAIN = 60;
    public static final int TEST = 30;
    public static final int VALIDATION = 10;
    
    public static float MAX_BRIGHTNESS = 300f;
    public static float DEF_BRIGHTNESS = 100f;
    public static float MIN_BRIGHTNESS = 0f;
    
    public static float MAX_CONTRAST = 300f;
    public static float DEF_CONTRAST = 100f;
    public static float MIN_CONTRAST = 0f;

    public static int MAX_ZOOM = 200;
    public static int DEF_ZOOM = 100;
    public static int MIN_ZOOM = 0;
}
