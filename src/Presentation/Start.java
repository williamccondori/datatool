package Presentation;

import Helpers.ApplicationSettings;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Start {

    public static void main(String[] args) {
        try {
            String lookAndFeel = ApplicationSettings.LOOK_AND_FEEL.equals("Metal")
                    ? "javax.swing.plaf.metal.MetalLookAndFeel"
                    : UIManager.getSystemLookAndFeelClassName();
            UIManager.setLookAndFeel(lookAndFeel);
            StartFrame startFrame = new StartFrame();
            startFrame.setVisible(true);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException exception) {
            System.err.println(exception.getMessage());
        }
    }

}
