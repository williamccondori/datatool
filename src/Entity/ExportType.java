package Entity;

public enum ExportType {
    TRAIN,
    TEST,
    VALIDATION
}