package Entity;

import Helpers.StringHelper;
import java.awt.Color;

public class ClassEntity {

    private final int Id;
    private String Name;
    private String Description;
    private String CustomName;
    private Color DisplayColor;
    private Color EdgeColor;
    private Color SegmentationColor;

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public String getCustomName() {
        return CustomName;
    }

    public Color getDisplayColor() {
        return DisplayColor;
    }

    public Color getEdgeColor() {
        return EdgeColor;
    }

    public Color getSegmentationColor() {
        return SegmentationColor;
    }

    public ClassEntity() {
        this.Id = 0;
    }

    public ClassEntity(int Id, String Name, String Description, String CustomName, Color DisplayColor, Color EdgeColor, Color SegmentationColor) {
        this.Id = Id;
        this.Name = Name.toUpperCase();
        this.Description = Description;
        this.CustomName = CustomName;
        this.DisplayColor = DisplayColor;
        this.EdgeColor = EdgeColor;
        this.SegmentationColor = SegmentationColor;
    }

    public void updateClass(String name, String description,
            String customName, Color displayColor, Color edgeColor, Color segmentationColor) {
        this.Name = name.toUpperCase();
        this.Description = description;
        this.CustomName = customName;
        this.DisplayColor = displayColor;
        this.EdgeColor = edgeColor;
        this.SegmentationColor = segmentationColor;
    }

    public void validate() throws Exception {
        if (StringHelper.isNullOrEmpty(this.Name)) {
            throw new Exception("Name is required!");
        }
        if (this.DisplayColor == null) {
            throw new Exception("Display color is required!");
        }
        if (this.EdgeColor == null) {
            throw new Exception("Edge color is required!");
        }
        if (this.SegmentationColor == null) {
            throw new Exception("Segmentation color is required!");
        }
    }
}
