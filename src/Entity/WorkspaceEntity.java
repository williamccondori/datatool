package Entity;

import Helpers.StringHelper;
import java.util.ArrayList;

public class WorkspaceEntity {

    private String Name;
    private String Author;
    private String Path;
    private String ImagePath;
    private String AnnotationPath;
    private SettingsEntity Settings;
    private ArrayList<ClassEntity> Classes;
    private ArrayList<PlaceEntity> Places;
    private ArrayList<StateEntity> States;
    private ArrayList<ImageEntity> Images;

    public String getName() {
        return Name;
    }

    public String getAuthor() {
        return Author;
    }

    public String getPath() {
        return Path;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public String getAnnotationPath() {
        return AnnotationPath;
    }

    public SettingsEntity getSettings() {
        return Settings;
    }

    public ArrayList<ClassEntity> getClasses() {
        return Classes;
    }

    public ArrayList<PlaceEntity> getPlaces() {
        return Places;
    }

    public ArrayList<StateEntity> getStates() {
        return States;
    }

    public ArrayList<ImageEntity> getImages() {
        return Images;
    }

    public static WorkspaceEntity create(String name, String author, String path) throws Exception {
        validate(name, author, path);
        WorkspaceEntity workspaceEntity = new WorkspaceEntity();
        workspaceEntity.Name = name.toUpperCase();
        workspaceEntity.Author = author.toUpperCase();
        workspaceEntity.Path = String.format("%s/%s", path, workspaceEntity.Name);
        workspaceEntity.ImagePath = String.format("%s/%s", workspaceEntity.Path, "Images");
        workspaceEntity.AnnotationPath = String.format("%s/%s", workspaceEntity.Path, "Annotations");
        workspaceEntity.Settings = SettingsEntity.getDefaultSettings();
        workspaceEntity.Classes = new ArrayList<>();
        workspaceEntity.Places = new ArrayList<>();
        workspaceEntity.States = new ArrayList<>();
        workspaceEntity.States.add(StateEntity.getDefaultState());
        workspaceEntity.Images = new ArrayList<>();
        return workspaceEntity;
    }

    public static WorkspaceEntity updateAuthor(String author) throws Exception {
        validate(author);
        WorkspaceEntity workspaceEntity = new WorkspaceEntity();
        workspaceEntity.Author = author.toUpperCase();
        return workspaceEntity;
    }

    private static void validate(String name, String author, String path) throws Exception {
        if (StringHelper.isNullOrEmpty(name)) {
            throw new Exception("Name is required!");
        }
        if (StringHelper.isNullOrEmpty(author)) {
            throw new Exception("Author is required!");
        }
        if (StringHelper.isNullOrEmpty(path)) {
            throw new Exception("Path is required!");
        }
    }

    private static void validate(String author) throws Exception {
        if (StringHelper.isNullOrEmpty(author)) {
            throw new Exception("Author is required!");
        }
    }

    public void open(String path) {
        this.Path = path;
        this.ImagePath = String.format("%s/%s", this.Path, "Images");
        this.AnnotationPath = String.format("%s/%s", this.Path, "Annotations");
    }

    public void createClass(ClassEntity classEntity) {
        this.Classes.add(classEntity);
    }

    public void createPlace(PlaceEntity placeEntity) {
        this.Places.add(placeEntity);
    }

    public void createState(StateEntity stateEntity) {
        this.States.add(stateEntity);
    }

    public void createImage(ImageEntity imageEntity) {
        this.Images.add(imageEntity);
    }

    public void update(String author) {
        this.Author = author;
    }

    public void updateClasses(ArrayList<ClassEntity> classes) {
        this.Classes = classes;
    }

    public void updatePlaces(ArrayList<PlaceEntity> places) {
        this.Places = places;
    }

    public void updateStates(ArrayList<StateEntity> states) {
        this.States = states;
    }

    public void updateImages(ArrayList<ImageEntity> images) {
        this.Images = images;
    }

    public void updateSettings(SettingsEntity setting) {
        this.Settings = setting;
    }
}
