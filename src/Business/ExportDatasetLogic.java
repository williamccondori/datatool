package Business;

import Entity.AnnotationEntity;
import Entity.ClassEntity;
import Entity.DatasetType;
import Entity.ExportType;
import Entity.SettingsEntity;
import Entity.VocEntity;
import Entity.VocObjectEntity;
import Entity.VocSizeEntity;
import Entity.VocSourceEntity;
import Entity.WorkspaceEntity;
import Entity.YoloEntity;
import Entity.YoloObjectEntity;
import Helpers.FileHelper;
import Helpers.StringHelper;
import Presentation.ApplicationFrame;
import Presentation.ExportDetectionsDatasetFrame;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Optional;

public class ExportDatasetLogic extends Thread {

    private DatasetType DatasetTipe;
    private WorkspaceEntity Workspace;
    private String ImageFolder;
    private String ImageValidationFolder;
    private String AnnotationFolder;
    private String SetFolder;
    private int Interator;

    public ExportDatasetLogic() {
    }

    public ExportDatasetLogic(String destinationPath, DatasetType datasetTipe) {
        try {
            WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
            this.Workspace = workspaceLogic.get();
            this.DatasetTipe = datasetTipe;
            this.Interator = 0;
            createDatasetStructure(destinationPath);
        } catch (Exception exception) {
            errorWindow(exception);
        }
    }

    private void createDatasetStructure(String destinationPath) {
        switch (this.DatasetTipe) {
            case VOC_PASCAL:
                createDatasetStructureVocPascal(destinationPath);
                break;
            case YOLO:
                createDataSsetStructureYolo(destinationPath);
                break;
            default:
                errorWindow(new Exception("Data set type requiered!"));
                break;
        }
    }

    private void createDatasetStructureVocPascal(String destinationPath) {
        String projectFolderPath = String.format("%s/VOC_%s", destinationPath, this.Workspace.getName());
        FileHelper.createFolder(projectFolderPath);

        String annotationsFolderPath = String.format("%s/Annotations", projectFolderPath);
        FileHelper.createFolder(annotationsFolderPath);

        String jpegImagesFolderPath = String.format("%s/JPEGImages", projectFolderPath);
        FileHelper.createFolder(jpegImagesFolderPath);
        
        String imageValidationPath = String.format("%s/ImagesValidation", projectFolderPath);
        FileHelper.createFolder(imageValidationPath);

        String imageSetsFolderPath = String.format("%s/ImageSets", projectFolderPath);
        FileHelper.createFolder(imageSetsFolderPath);

        this.AnnotationFolder = annotationsFolderPath;
        this.ImageFolder = jpegImagesFolderPath;
        this.ImageValidationFolder = imageValidationPath;
        this.SetFolder = imageSetsFolderPath;
    }

    private void createDataSsetStructureYolo(String destinationPath) {
        String projectFolderPath = String.format("%s/YOLO_%s", destinationPath, this.Workspace.getName());
        FileHelper.createFolder(projectFolderPath);

        String imagesFolderPath = String.format("%s/Images", projectFolderPath);
        FileHelper.createFolder(imagesFolderPath);
        
        String imageValidationPath = String.format("%s/ImagesValidation", projectFolderPath);
        FileHelper.createFolder(imageValidationPath);

        String imageSetsFolderPath = String.format("%s/ImageSets", projectFolderPath);
        FileHelper.createFolder(imageSetsFolderPath);

        this.AnnotationFolder = imagesFolderPath;
        this.ImageFolder = imagesFolderPath;
        this.ImageValidationFolder = imageValidationPath;
        this.SetFolder = imageSetsFolderPath;
    }

    @Override
    public void run() {
        try {
            startWindow();
            ArrayList<Integer> images = new ArrayList<>();
            this.Workspace.getImages().forEach((image) -> {
                images.add(image.getId());
            });
            ExportDetectionsDatasetFrame.proLog.setMaximum(images.size());
            SettingsEntity settingsEntity = this.Workspace.getSettings();
            ExportDistributionLogic exportDistributionLogic = new ExportDistributionLogic(images);
            exportDistributionLogic.distribute(settingsEntity.getTrain(),
                    settingsEntity.getTest(),
                    settingsEntity.getValidation(),
                    settingsEntity.isRandom());

            ArrayList<Integer> imagesTrain = exportDistributionLogic.getImagesTrain();
            ArrayList<Integer> imagesTest = exportDistributionLogic.getImagesTest();
            ArrayList<Integer> imagesValidation = exportDistributionLogic.getImagesValidation();

            exportData(imagesTrain, ExportType.TRAIN);
            exportData(imagesTest, ExportType.TEST);
            exportData(imagesValidation, ExportType.VALIDATION);

            finishWindow();
        } catch (Exception exception) {
            errorWindow(exception);
        }
    }

    private void exportData(ArrayList<Integer> imagesId, ExportType exportType) throws IOException {
        // GENERACION DE ARCHIVOS DE ANOTACION.
        if (exportType != ExportType.VALIDATION) {
            AnnotationLogic annotationLogic = new AnnotationLogic(this.Workspace.getAnnotationPath());
            try {
                for (Integer imageId : imagesId) {
                    AnnotationEntity annotationEntity = annotationLogic.get(imageId);
                    generateAnnotations(annotationEntity);
                    printLog(String.format("Exporting annotation: %s [%s]", imageId, exportType.name()));
                }
            } catch (Exception exception) {
                errorWindow(exception);
            }
        }

        // PROCESO DE COPIADO DE LAS IMAGENES.
        imagesId.stream().map((id) -> this.Workspace.getImages().stream().filter(p -> p.getId() == id).findFirst())
                .filter((imageEntity) -> (imageEntity.isPresent())).forEachOrdered((imageEntity) -> {
            String imageInputPath = String.format("%s/%s", this.Workspace.getImagePath(), imageEntity.get().getImageName());
            String imageOutputPath = exportType == ExportType.VALIDATION
                    ? String.format("%s/%s", this.ImageValidationFolder, imageEntity.get().getImageName())
                    : String.format("%s/%s", this.ImageFolder, imageEntity.get().getImageName());
            try {
                FileHelper.copyFile(new File(imageInputPath), new File(imageOutputPath));
                printLog(String.format("Exporting image: %s [%s]", imageEntity.get().getId(), exportType.name()));
                ExportDetectionsDatasetFrame.proLog.setValue(this.Interator + 1);
                this.Interator++;
            } catch (IOException exception) {
                errorWindow(exception);
            }
        });

        // GENERACION DE ARCHIVOS DE DEFINICION DE LA DISTRIBUCION.
        try (FileWriter fileWriter = new FileWriter(String.format("%s/%s.txt", this.SetFolder, exportType.name().toLowerCase()));
                PrintWriter printWriter = new PrintWriter(fileWriter)) {
            imagesId.forEach((id) -> {
                printWriter.println(id);
            });
        }
    }

    private void generateAnnotations(AnnotationEntity annotationEntity) throws Exception {
        switch (this.DatasetTipe) {
            case VOC_PASCAL:
                ArrayList<VocObjectEntity> vocObjects = new ArrayList<>();

                annotationEntity.getDetections().stream().map(p -> new VocObjectEntity(
                        getClassName(p.getIdClass()),
                        p.getPose(),
                        p.getTruncated() ? 1 : 0,
                        p.getDifficult() ? 1 : 0,
                        p.getOccluded() ? 1 : 0,
                        p.getXmin(),
                        p.getXmax(),
                        p.getYmin(),
                        p.getYmax())).forEachOrdered((vocObject) -> {
                    vocObjects.add(vocObject);
                });

                VocEntity vocEntity = new VocEntity("folder",
                        annotationEntity.getImageName(),
                        String.format("%s/%s", this.ImageFolder, annotationEntity.getImageName()),
                        new VocSourceEntity(this.Workspace.getName()),
                        new VocSizeEntity(String.valueOf(annotationEntity.getWidth()),
                                String.valueOf(annotationEntity.getHeight()),
                                String.valueOf(annotationEntity.getDepth())),
                        vocObjects.size() > 0 ? StringHelper.TRUE : StringHelper.FALSE,
                        vocObjects);

                ExportVocLogic exportVocLogic = new ExportVocLogic(this.AnnotationFolder);
                exportVocLogic.GenerateFile(vocEntity, String.valueOf(annotationEntity.getId()));
                break;
            case YOLO:
                ArrayList<YoloObjectEntity> yoloObjects = new ArrayList<>();

                annotationEntity.getDetections().stream().map(p -> YoloObjectEntity.toYolo(p.getIdClass(),
                        p.getXmin(), p.getYmin(), p.getXmax(), p.getYmax(),
                        annotationEntity.getWidth(),
                        annotationEntity.getHeight()
                )).forEachOrdered((yoloObject) -> {
                    yoloObjects.add(yoloObject);
                });

                YoloEntity yoloEntity = new YoloEntity(yoloObjects);

                ExportYoloLogic exportYoloLogic = new ExportYoloLogic(this.AnnotationFolder);
                exportYoloLogic.GenerateFile(yoloEntity, String.valueOf(annotationEntity.getId()));
                break;
            default:
                errorWindow(new Exception("Data set type requiered!"));
                break;
        }
    }

    private String getClassName(int idClass) {
        Optional<ClassEntity> optional = this.Workspace.getClasses()
                .stream().filter(p -> p.getId() == idClass)
                .findFirst();
        return optional.isPresent() ? optional.get().getName() : StringHelper.EMPTY;
    }

    private void startWindow() {
        ExportDetectionsDatasetFrame.lblLog.setText("Exporting workspace, this may take a few minutes...");
        ExportDetectionsDatasetFrame.txtLog.setText(StringHelper.EMPTY);
        ExportDetectionsDatasetFrame.txtLog.setEditable(Boolean.FALSE);
        ExportDetectionsDatasetFrame.proLog.setMaximum(0);
        ExportDetectionsDatasetFrame.proLog.setValue(0);
        ExportDetectionsDatasetFrame.btnAccept.setEnabled(Boolean.FALSE);
    }

    private void finishWindow() {
        ExportDetectionsDatasetFrame.lblLog.setText("Completed!");
        ExportDetectionsDatasetFrame.proLog.setValue(ExportDetectionsDatasetFrame.proLog.getMaximum());
        printLog("Completed!");
        ExportDetectionsDatasetFrame.btnAccept.setEnabled(Boolean.TRUE);
    }

    private void errorWindow(Exception exception) {
        ExportDetectionsDatasetFrame.lblLog.setText("Error!");
        ExportDetectionsDatasetFrame.proLog.setMaximum(0);
        ExportDetectionsDatasetFrame.proLog.setValue(0);
        printLog(String.format("An error has occurred: %s", exception.getLocalizedMessage()));
        ExportDetectionsDatasetFrame.btnAccept.setEnabled(Boolean.TRUE);
    }

    private void printLog(String message) {
        ExportDetectionsDatasetFrame.txtLog.append(message + "\n");
        ExportDetectionsDatasetFrame.txtLog.setCaretPosition(ExportDetectionsDatasetFrame.txtLog.getText().length());
    }
}
