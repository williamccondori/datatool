package Helpers;

import java.awt.Color;

public class ColorHelper {

    /**
     * OBTIENE EL EQUIVALENTE EN CADENA DE LA INSTANCIA DE UN COLOR.
     *
     * @param color COLOR.
     * @return VALOR EN CADENA DEL COLOR.
     */
    public static final String getColor(Color color) {
        return String.format("[%s,%s,%s]", color.getRed(), color.getGreen(), color.getBlue());
    }

    /**
     * OBTIENE UNA INSTANCIA DE COLOR A TRAVES DE SU EQUIVALENTE EN CADENA.
     *
     * @param colorString VALOR EN CADENA DEL COLOR.
     * @return COLOR.
     */
    public static Color setColor(String colorString) {
        colorString = colorString.substring(1, colorString.length() - 1);
        String[] colors = colorString.split(",");
        int red = Integer.parseInt(colors[0].trim());
        int green = Integer.parseInt(colors[1].trim());
        int blue = Integer.parseInt(colors[2].trim());
        return new Color(red, green, blue);
    }

    /**
     * LIMPIA LA CADENA QUE REPRESENTA UN COLOR.
     *
     * @param colorString CADENA DE ENTRADA.
     * @return CADENA DE SALIDA.
     */
    public static String clearColor(String colorString) {
        colorString = colorString.substring(1, colorString.length() - 1);
        return colorString;
    }
}
