package Helpers;

import java.text.Normalizer;

public class StringHelper {

    public static final String EMPTY = "";
    public static final String FALSE = "false";
    public static final String TRUE = "true";

    /**
     * VERIFICA QUE LA CADENA NO ESTE VACIA NI NULA.
     *
     * @param string VALOR DE LA CADENA.
     * @return INDICADOR.
     */
    public static final boolean isNullOrEmpty(String string) {
        boolean response = false;
        if (string.equals(StringHelper.EMPTY) || string.isEmpty()) {
            response = true;
        }
        return response;
    }

    /**
     * LIMPIA UNA CADENA DE CARACTERES EXTRANIOS.
     *
     * @param string VALOR DE LA CADENA.
     * @return CADENA LIMPIA.
     */
    public static String cleanString(String string) {
        string = Normalizer.normalize(string, Normalizer.Form.NFD);
        string = string.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return string;
    }
    
    public static int toInt32(String string){
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
