package Business;

import Entity.YoloEntity;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class ExportYoloLogic {

    private final String AnnotationFolder;

    public ExportYoloLogic(String AnnotationFolder) {
        this.AnnotationFolder = AnnotationFolder;
    }

    public void GenerateFile(YoloEntity yoloEntity, String id) throws IOException {
        ArrayList<String> objects = new ArrayList<>();

        yoloEntity.getObjects().stream().map((object)
                -> String.format("%s %s %s %s %s", object.getIdClass(), object.getX(), object.getY(), object.getW(), object.getH())
        ).forEachOrdered((objectString) -> {
            objects.add(objectString);
        });

        try (FileWriter fileWriter = new FileWriter(String.format("%s/%s.txt", this.AnnotationFolder, id)); 
                PrintWriter printWriter = new PrintWriter(fileWriter)) {
            objects.forEach(p -> {
                printWriter.println(p);
            });
            printWriter.close();
            fileWriter.close();
        }
    }
}
