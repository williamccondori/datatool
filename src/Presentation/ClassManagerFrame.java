package Presentation;

import Business.WorkspaceLogic;
import Entity.ClassEntity;
import Helpers.ApplicationSettings;
import Helpers.ColorHelper;
import Helpers.MessageHelper;
import Helpers.StringHelper;
import Helpers.TableHelper;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.JColorChooser;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

public class ClassManagerFrame extends javax.swing.JInternalFrame {

    private int LastRecord;

    public ClassManagerFrame() {
        initComponents();
        loadWindow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrClass = new javax.swing.JScrollPane();
        tblClass = new javax.swing.JTable();
        lblId = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        lblName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        lblDescription = new javax.swing.JLabel();
        txtDescription = new javax.swing.JTextField();
        lblCustomName = new javax.swing.JLabel();
        txtCustomName = new javax.swing.JTextField();
        lblDisplayColor = new javax.swing.JLabel();
        lblEdgeColor = new javax.swing.JLabel();
        lblSegmentationColor = new javax.swing.JLabel();
        txtDisplayColor = new javax.swing.JTextField();
        txtEdgeColor = new javax.swing.JTextField();
        txtSegmentationColor = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnChooseDisplayColor = new javax.swing.JButton();
        btnChooseEdgeColor = new javax.swing.JButton();
        btnChooseSegmentationColor = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-etiqueta-16.png"))); // NOI18N

        tblClass.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrClass.setViewportView(tblClass);

        lblId.setText("Id:");

        txtId.setText("jTextField1");

        lblName.setText("Name:");

        txtName.setText("jTextField2");

        lblDescription.setText("Description:");

        txtDescription.setText("jTextField3");

        lblCustomName.setText("Custom name:");

        txtCustomName.setText("jTextField4");

        lblDisplayColor.setText("<html>Display color: [Image classification]</html>");

        lblEdgeColor.setText("<html>Edge color: [Image detection]</html>");

        lblSegmentationColor.setText("<html>Segmentation color: [Image segmentation]</html>");

        txtDisplayColor.setText("jTextField5");

        txtEdgeColor.setText("jTextField6");

        txtSegmentationColor.setText("jTextField7");

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 102, 153));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-guardar-16.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSave.setFocusPainted(false);

        btnClear.setBackground(new java.awt.Color(255, 255, 255));
        btnClear.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnClear.setForeground(new java.awt.Color(0, 102, 153));
        btnClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-borrar-16.png"))); // NOI18N
        btnClear.setText("Clear");
        btnClear.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnClear.setFocusPainted(false);

        btnChooseDisplayColor.setBackground(new java.awt.Color(255, 255, 255));
        btnChooseDisplayColor.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnChooseDisplayColor.setForeground(new java.awt.Color(0, 102, 153));
        btnChooseDisplayColor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-paleta-de-pintura-16.png"))); // NOI18N
        btnChooseDisplayColor.setText("Choose");
        btnChooseDisplayColor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnChooseDisplayColor.setFocusPainted(false);

        btnChooseEdgeColor.setBackground(new java.awt.Color(255, 255, 255));
        btnChooseEdgeColor.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnChooseEdgeColor.setForeground(new java.awt.Color(0, 102, 153));
        btnChooseEdgeColor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-paleta-de-pintura-16.png"))); // NOI18N
        btnChooseEdgeColor.setText("Choose");
        btnChooseEdgeColor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnChooseEdgeColor.setFocusPainted(false);

        btnChooseSegmentationColor.setBackground(new java.awt.Color(255, 255, 255));
        btnChooseSegmentationColor.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnChooseSegmentationColor.setForeground(new java.awt.Color(0, 102, 153));
        btnChooseSegmentationColor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-paleta-de-pintura-16.png"))); // NOI18N
        btnChooseSegmentationColor.setText("Choose");
        btnChooseSegmentationColor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnChooseSegmentationColor.setFocusPainted(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDisplayColor)
                    .addComponent(scrClass, javax.swing.GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDescription, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDescription)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                    .addComponent(txtId, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(lblEdgeColor)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblSegmentationColor)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblCustomName, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCustomName, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtDisplayColor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnChooseDisplayColor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtEdgeColor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnChooseEdgeColor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSegmentationColor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnChooseSegmentationColor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrClass, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblId)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCustomName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCustomName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDescription))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDisplayColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnChooseDisplayColor, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(txtDisplayColor))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblEdgeColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnChooseEdgeColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtEdgeColor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSegmentationColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSegmentationColor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChooseSegmentationColor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChooseDisplayColor;
    private javax.swing.JButton btnChooseEdgeColor;
    private javax.swing.JButton btnChooseSegmentationColor;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel lblCustomName;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblDisplayColor;
    private javax.swing.JLabel lblEdgeColor;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblSegmentationColor;
    private javax.swing.JScrollPane scrClass;
    private javax.swing.JTable tblClass;
    private javax.swing.JTextField txtCustomName;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtDisplayColor;
    private javax.swing.JTextField txtEdgeColor;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtSegmentationColor;
    // End of variables declaration//GEN-END:variables

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
            loadData();
            loadForm();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA EL FUNCIONAMIENTO DE LOS CONTROLES DE LA VENTANA.
     */
    private void loadControl() {
        this.setSize(new Dimension(500, 500));
        this.setResizable(Boolean.FALSE);
        this.setTitle("Class manager");
        this.btnChooseDisplayColor.addActionListener(this::chooseDisplayColor);
        this.btnChooseEdgeColor.addActionListener(this::chooseEdgeColor);
        this.btnChooseSegmentationColor.addActionListener(this::chooseSegmentationColor);
        this.btnSave.addActionListener(this::save);
        this.btnClear.addActionListener(this::clear);
        this.txtId.setEditable(Boolean.FALSE);
        this.txtDisplayColor.setEditable(Boolean.FALSE);
        this.txtEdgeColor.setEditable(Boolean.FALSE);
        this.txtSegmentationColor.setEditable(Boolean.FALSE);
        ListSelectionModel classSelectionModel = this.tblClass.getSelectionModel();
        classSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        classSelectionModel.addListSelectionListener(this::selectClass);
    }

    /**
     * CARGA LOS DATOS UTILIZADOS EN LA VENTANA.
     *
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    public void loadData() throws Exception {
        loadClasses();
    }

    /**
     * CARGA LOS DATOS PREDETERMINADOS PARA EL FORMULARIO.
     */
    private void loadForm() {
        this.txtId.setText(String.valueOf(this.LastRecord));
        this.txtName.setText(StringHelper.EMPTY);
        this.txtDescription.setText(StringHelper.EMPTY);
        this.txtCustomName.setText(StringHelper.EMPTY);
        this.txtDisplayColor.setText(ColorHelper.getColor(Color.GREEN));
        this.txtEdgeColor.setText(ColorHelper.getColor(Color.GREEN));
        this.txtSegmentationColor.setText(ColorHelper.getColor(Color.GREEN));
    }

    /**
     * ASIGNA LOS VALORES A LA TABLA DE LAS CLASES.
     *
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    private void loadClasses() throws Exception {
        String columns[] = {"Id", "Class"};
        DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
        WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
        ArrayList<ClassEntity> classes = workspaceLogic.get().getClasses();
        this.LastRecord = classes.isEmpty() ? ApplicationSettings.PREFIX_ID : classes.size() + ApplicationSettings.PREFIX_ID;
        classes.forEach((p) -> {
            defaultTableModel.addRow(new Object[]{
                p.getId(),
                p.getName()
            });
        });
        this.tblClass.setModel(defaultTableModel);
    }

    /**
     * SELECCIONA UNA FILA DE LA TABLA DE LAS CLASES.
     *
     * @param e EVENTO.
     */
    private void selectClass(ListSelectionEvent e) {
        try {
            if (!e.getValueIsAdjusting()) {
                int idClass = TableHelper.getSelectedId(this.tblClass);
                if (idClass != 0) {
                    WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                    ClassEntity classEntity = workspaceLogic.getClassById(idClass);
                    this.txtId.setText(String.valueOf(classEntity.getId()));
                    this.txtName.setText(classEntity.getName());
                    this.txtDescription.setText(classEntity.getDescription());
                    this.txtCustomName.setText(classEntity.getCustomName());
                    this.txtDisplayColor.setText(ColorHelper.getColor(classEntity.getDisplayColor()));
                    this.txtEdgeColor.setText(ColorHelper.getColor(classEntity.getEdgeColor()));
                    this.txtSegmentationColor.setText(ColorHelper.getColor(classEntity.getSegmentationColor()));
                }
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UN CUADRO PARA SELECCIONAR UN COLOR.
     *
     * @param e EVENTO.
     */
    private void chooseDisplayColor(ActionEvent e) {
        try {
            Color color = JColorChooser.showDialog(null, "Choose a color", Color.BLACK);
            if (!(color == null)) {
                this.txtDisplayColor.setText(ColorHelper.getColor(color));
            }
        } catch (HeadlessException exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UN CUADRO PARA SELECCIONAR UN COLOR.
     *
     * @param e EVENTO.
     */
    private void chooseEdgeColor(ActionEvent e) {
        try {
            Color color = JColorChooser.showDialog(null, "Choose a color", Color.BLACK);
            if (!(color == null)) {
                this.txtEdgeColor.setText(ColorHelper.getColor(color));
            }
        } catch (HeadlessException exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UN CUADRO PARA SELECCIONAR UN COLOR.
     *
     * @param e EVENTO.
     */
    private void chooseSegmentationColor(ActionEvent e) {
        try {
            Color color = JColorChooser.showDialog(null, "Choose a color", Color.BLACK);
            if (!(color == null)) {
                this.txtSegmentationColor.setText(ColorHelper.getColor(color));
            }
        } catch (HeadlessException exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * GAURDA LOS CAMBIOS REALIZADOS EN EL FORMULARIO.
     *
     * @param e EVENTO.
     */
    private void save(ActionEvent e) {
        int result = MessageHelper.showWarningMessage("Do you want to save the changes?");
        if (MessageHelper.isWarningYes(result)) {
            try {
                ClassEntity classEntity = new ClassEntity(
                        Integer.parseInt(txtId.getText()),
                        txtName.getText(),
                        txtDescription.getText(),
                        txtCustomName.getText(),
                        ColorHelper.setColor(txtDisplayColor.getText()),
                        ColorHelper.setColor(txtEdgeColor.getText()),
                        ColorHelper.setColor(txtSegmentationColor.getText()));
                classEntity.validate();
                WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                workspaceLogic.saveClass(classEntity);
                loadClasses();
                loadForm();
                MessageHelper.showSuccessMessage("The changes were saved correctly!");
            } catch (Exception exception) {
                MessageHelper.showErrorMessage(exception);
            }
        }
    }

    /**
     * DESHACE LOS CAMBIOS REALIZADOS EN EL FORMULARIO.
     *
     * @param e EVENTO.
     */
    private void clear(ActionEvent e) {
        try {
            loadClasses();
            loadForm();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }
}
