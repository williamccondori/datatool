package Entity;

public class VocObjectEntity {

    private String Name;
    private String Pose;
    private int Truncated;
    private int Dificult;
    private int Occluded;
    private int Xmin;
    private int Xmax;
    private int Ymin;
    private int Ymax;

    public String getName() {
        return Name;
    }

    public String getPose() {
        return Pose;
    }

    public int getTruncated() {
        return Truncated;
    }

    public int getDificult() {
        return Dificult;
    }

    public int getOccluded() {
        return Occluded;
    }

    public int getXmin() {
        return Xmin;
    }

    public int getXmax() {
        return Xmax;
    }

    public int getYmin() {
        return Ymin;
    }

    public int getYmax() {
        return Ymax;
    }

    public VocObjectEntity() {
    }

    public VocObjectEntity(String Name, String Pose, int Truncated,
            int Dificult, int Occluded, int Xmin, int Xmax, int Ymin,
            int Ymax) {
        this.Name = Name;
        this.Pose = Pose;
        this.Truncated = Truncated;
        this.Dificult = Dificult;
        this.Occluded = Occluded;
        this.Xmin = Xmin;
        this.Xmax = Xmax;
        this.Ymin = Ymin;
        this.Ymax = Ymax;
    }
}
