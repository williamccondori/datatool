package Entity;

import Helpers.ApplicationSettings;

public class SettingsEntity {

    private String LookAndFeel;
    private int Test;
    private int Train;
    private int Validation;
    private boolean Random;

    public String getLookAndFeel() {
        return LookAndFeel;
    }

    public int getTest() {
        return Test;
    }

    public int getTrain() {
        return Train;
    }

    public int getValidation() {
        return Validation;
    }

    public boolean isRandom() {
        return Random;
    }

    public SettingsEntity() {
    }

    public static SettingsEntity getDefaultSettings() {
        SettingsEntity settingsEntity = new SettingsEntity();
        settingsEntity.LookAndFeel = ApplicationSettings.LOOK_AND_FEEL;
        settingsEntity.Test = ApplicationSettings.TEST;
        settingsEntity.Train = ApplicationSettings.TRAIN;
        settingsEntity.Validation = ApplicationSettings.VALIDATION;
        settingsEntity.Random = Boolean.FALSE;
        return settingsEntity;
    }

    public static SettingsEntity updateDistribution(int train, int test, int validation, boolean isRandom) throws Exception {
        validate(train, test, validation);
        SettingsEntity settingsEntity = new SettingsEntity();
        settingsEntity.Test = test;
        settingsEntity.Train = train;
        settingsEntity.Validation = validation;
        settingsEntity.Random = isRandom;
        return settingsEntity;
    }

    public void update(int train, int test, int validation, boolean isRandom) {
        this.Train = train;
        this.Test = test;
        this.Validation = validation;
        this.Random = isRandom;
    }

    private static void validate(int train, int test, int validation) throws Exception {
        int total = train + test + validation;
        if (total != 100) {
            throw new Exception("Invalid distribution!");
        }
    }
}
