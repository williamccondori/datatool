package Presentation;

import Business.AnnotationLogic;
import Business.WorkspaceLogic;
import Components.BoundingBoxComponent;
import Entity.AnnotationEntity;
import Entity.ClassEntity;
import Entity.ComboEntity;
import Entity.DetectionEntity;
import Entity.ImageEntity;
import Entity.StateEntity;
import Entity.WorkspaceEntity;
import Helpers.ApplicationSettings;
import Helpers.MessageHelper;
import Helpers.StringHelper;
import Helpers.TableHelper;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.util.ArrayList;
import java.util.Optional;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

public class ImageDetectionFrame extends javax.swing.JInternalFrame {

    private int Id;
    private int LastId;
    private int IdObject;

    private BufferedImage OriginalImage;
    private BufferedImage ResultImage;

    public ImageDetectionFrame() {
        this.Id = ApplicationSettings.PREFIX_ID;
        this.IdObject = 0;
        initComponents();
        loadWindow();
    }

    public ImageDetectionFrame(int Id) {
        this.Id = Id;
        this.IdObject = 0;
        initComponents();
        loadWindow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgDetection = new javax.swing.JDialog();
        lblClass = new javax.swing.JLabel();
        cmbClass = new javax.swing.JComboBox<>();
        lblPose = new javax.swing.JLabel();
        cmbPose = new javax.swing.JComboBox<>();
        chkTruncated = new javax.swing.JCheckBox();
        chkDifficult = new javax.swing.JCheckBox();
        chkOccluded = new javax.swing.JCheckBox();
        btnSaveBoundingBox = new javax.swing.JButton();
        btnRefreshClass = new javax.swing.JButton();
        tooPrincipal = new javax.swing.JToolBar();
        btnCursor = new javax.swing.JButton();
        btnHand = new javax.swing.JButton();
        btnResize = new javax.swing.JButton();
        pnlId = new javax.swing.JPanel();
        lblId = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        pnlImage = new javax.swing.JPanel();
        scrImage = new javax.swing.JScrollPane();
        cmpDetectionWindow = new Components.BoundingBoxComponent();
        jPanel2 = new javax.swing.JPanel();
        lblSearch = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        lblState = new javax.swing.JLabel();
        cmbState = new javax.swing.JComboBox<>();
        srcObjects = new javax.swing.JScrollPane();
        tblObjects = new javax.swing.JTable();
        lblBrightness = new javax.swing.JLabel();
        sliBrightness = new javax.swing.JSlider();
        lblContrast = new javax.swing.JLabel();
        sliContrast = new javax.swing.JSlider();
        txtBrightness = new javax.swing.JLabel();
        txtContrast = new javax.swing.JLabel();
        btnRefreshState = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnDeleteBoundingBox = new javax.swing.JButton();
        btnAddBoundingBox = new javax.swing.JButton();
        tooZoom = new javax.swing.JToolBar();
        btnZoomIn = new javax.swing.JButton();
        btnZoomOut = new javax.swing.JButton();
        btnZoomDefault = new javax.swing.JButton();
        sliZoom = new javax.swing.JSlider();
        txtZoom = new javax.swing.JLabel();

        lblClass.setText("Class:");

        lblPose.setText("Pose:");

        cmbPose.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        chkTruncated.setText("Truncated");

        chkDifficult.setText("Dificult");

        chkOccluded.setText("Occluded");

        btnSaveBoundingBox.setBackground(new java.awt.Color(0, 102, 153));
        btnSaveBoundingBox.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSaveBoundingBox.setForeground(new java.awt.Color(255, 255, 255));
        btnSaveBoundingBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-más-16.png"))); // NOI18N
        btnSaveBoundingBox.setText("Add");
        btnSaveBoundingBox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));

        btnRefreshClass.setBackground(new java.awt.Color(0, 102, 153));
        btnRefreshClass.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnRefreshClass.setForeground(new java.awt.Color(255, 255, 255));
        btnRefreshClass.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-reiniciar-16.png"))); // NOI18N
        btnRefreshClass.setToolTipText("Refresh");
        btnRefreshClass.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnRefreshClass.setFocusPainted(false);
        btnRefreshClass.setFocusable(false);
        btnRefreshClass.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout dlgDetectionLayout = new javax.swing.GroupLayout(dlgDetection.getContentPane());
        dlgDetection.getContentPane().setLayout(dlgDetectionLayout);
        dlgDetectionLayout.setHorizontalGroup(
            dlgDetectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgDetectionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgDetectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbPose, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPose, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblClass, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(dlgDetectionLayout.createSequentialGroup()
                        .addGroup(dlgDetectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(chkOccluded)
                            .addComponent(chkDifficult)
                            .addComponent(chkTruncated))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgDetectionLayout.createSequentialGroup()
                        .addGap(0, 280, Short.MAX_VALUE)
                        .addComponent(btnSaveBoundingBox, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgDetectionLayout.createSequentialGroup()
                        .addComponent(cmbClass, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefreshClass, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dlgDetectionLayout.setVerticalGroup(
            dlgDetectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgDetectionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblPose)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbPose, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblClass)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dlgDetectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnRefreshClass, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(cmbClass))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chkTruncated)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkDifficult)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkOccluded)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 85, Short.MAX_VALUE)
                .addComponent(btnSaveBoundingBox, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-agrupar-objetos-16.png"))); // NOI18N

        tooPrincipal.setRollover(true);

        btnCursor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-crear-nuevo-16.png"))); // NOI18N
        btnCursor.setToolTipText("Draw");
        btnCursor.setBorderPainted(false);
        btnCursor.setFocusPainted(false);
        btnCursor.setFocusable(false);
        btnCursor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCursor.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnCursor);

        btnHand.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cambiar-tamaño-cuatro-direcciones-16.png"))); // NOI18N
        btnHand.setToolTipText("Move");
        btnHand.setBorderPainted(false);
        btnHand.setFocusPainted(false);
        btnHand.setFocusable(false);
        btnHand.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnHand.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnHand);

        btnResize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cambiar-el-tamaño-16.png"))); // NOI18N
        btnResize.setToolTipText("Resize");
        btnResize.setBorderPainted(false);
        btnResize.setFocusPainted(false);
        btnResize.setFocusable(false);
        btnResize.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnResize.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnResize);

        lblId.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblId.setText("[ID]");
        lblId.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblId.setPreferredSize(new java.awt.Dimension(100, 26));

        javax.swing.GroupLayout pnlIdLayout = new javax.swing.GroupLayout(pnlId);
        pnlId.setLayout(pnlIdLayout);
        pnlIdLayout.setHorizontalGroup(
            pnlIdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblId, javax.swing.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE)
        );
        pnlIdLayout.setVerticalGroup(
            pnlIdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblId, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
        );

        tooPrincipal.add(pnlId);

        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-anterior-16.png"))); // NOI18N
        btnBack.setToolTipText("Back");
        btnBack.setBorderPainted(false);
        btnBack.setFocusPainted(false);
        btnBack.setFocusable(false);
        btnBack.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBack.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnBack);

        btnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-siguiente-16.png"))); // NOI18N
        btnNext.setToolTipText("Next");
        btnNext.setBorderPainted(false);
        btnNext.setFocusPainted(false);
        btnNext.setFocusable(false);
        btnNext.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNext.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooPrincipal.add(btnNext);

        pnlImage.setLayout(new javax.swing.OverlayLayout(pnlImage));

        javax.swing.GroupLayout cmpDetectionWindowLayout = new javax.swing.GroupLayout(cmpDetectionWindow);
        cmpDetectionWindow.setLayout(cmpDetectionWindowLayout);
        cmpDetectionWindowLayout.setHorizontalGroup(
            cmpDetectionWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 413, Short.MAX_VALUE)
        );
        cmpDetectionWindowLayout.setVerticalGroup(
            cmpDetectionWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 423, Short.MAX_VALUE)
        );

        scrImage.setViewportView(cmpDetectionWindow);

        pnlImage.add(scrImage);

        lblSearch.setText("Search:");

        txtSearch.setText("jTextField1");

        lblState.setText("State:");

        tblObjects.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        srcObjects.setViewportView(tblObjects);

        lblBrightness.setText("Brightness:");

        lblContrast.setText("Contrast:");

        txtBrightness.setText("[]");

        txtContrast.setText("[]");

        btnRefreshState.setBackground(new java.awt.Color(255, 255, 255));
        btnRefreshState.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnRefreshState.setForeground(new java.awt.Color(0, 102, 153));
        btnRefreshState.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-reiniciar-16.png"))); // NOI18N
        btnRefreshState.setToolTipText("Refresh");
        btnRefreshState.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnRefreshState.setFocusPainted(false);
        btnRefreshState.setFocusable(false);
        btnRefreshState.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        btnSearch.setBackground(new java.awt.Color(255, 255, 255));
        btnSearch.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(0, 102, 153));
        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-búsqueda-16.png"))); // NOI18N
        btnSearch.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSearch.setFocusPainted(false);

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 102, 153));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-guardar-16.png"))); // NOI18N
        btnSave.setToolTipText("Refresh");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSave.setFocusPainted(false);
        btnSave.setFocusable(false);
        btnSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        btnDeleteBoundingBox.setBackground(new java.awt.Color(255, 255, 255));
        btnDeleteBoundingBox.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnDeleteBoundingBox.setForeground(new java.awt.Color(0, 102, 153));
        btnDeleteBoundingBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-eliminar-16.png"))); // NOI18N
        btnDeleteBoundingBox.setToolTipText("Refresh");
        btnDeleteBoundingBox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnDeleteBoundingBox.setFocusPainted(false);
        btnDeleteBoundingBox.setFocusable(false);
        btnDeleteBoundingBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        btnAddBoundingBox.setBackground(new java.awt.Color(255, 255, 255));
        btnAddBoundingBox.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnAddBoundingBox.setForeground(new java.awt.Color(0, 102, 153));
        btnAddBoundingBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-añadir-etiqueta-16.png"))); // NOI18N
        btnAddBoundingBox.setText("Add");
        btnAddBoundingBox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnAddBoundingBox.setFocusPainted(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(srcObjects, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(lblSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblState, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cmbState, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefreshState, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(sliBrightness, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sliContrast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnDeleteBoundingBox, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblBrightness)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBrightness))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblContrast)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtContrast)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnAddBoundingBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(txtSearch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblState)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbState, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRefreshState, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBrightness)
                    .addComponent(txtBrightness))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliBrightness, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblContrast)
                    .addComponent(txtContrast))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sliContrast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAddBoundingBox, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(srcObjects, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDeleteBoundingBox, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        tooZoom.setRollover(true);

        btnZoomIn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-acercar-16.png"))); // NOI18N
        btnZoomIn.setToolTipText("Zoom in");
        btnZoomIn.setBorderPainted(false);
        btnZoomIn.setFocusPainted(false);
        btnZoomIn.setFocusable(false);
        btnZoomIn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnZoomIn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooZoom.add(btnZoomIn);

        btnZoomOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-alejar-16.png"))); // NOI18N
        btnZoomOut.setToolTipText("Zoom out");
        btnZoomOut.setBorderPainted(false);
        btnZoomOut.setFocusPainted(false);
        btnZoomOut.setFocusable(false);
        btnZoomOut.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnZoomOut.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooZoom.add(btnZoomOut);

        btnZoomDefault.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-visible-16.png"))); // NOI18N
        btnZoomDefault.setToolTipText("Zoom default");
        btnZoomDefault.setBorderPainted(false);
        btnZoomDefault.setFocusPainted(false);
        btnZoomDefault.setFocusable(false);
        btnZoomDefault.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnZoomDefault.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        tooZoom.add(btnZoomDefault);
        tooZoom.add(sliZoom);

        txtZoom.setText("100");
        tooZoom.add(txtZoom);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tooPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(tooZoom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tooPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addComponent(tooZoom, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddBoundingBox;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCursor;
    private javax.swing.JButton btnDeleteBoundingBox;
    private javax.swing.JButton btnHand;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnRefreshClass;
    private javax.swing.JButton btnRefreshState;
    private javax.swing.JButton btnResize;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSaveBoundingBox;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnZoomDefault;
    private javax.swing.JButton btnZoomIn;
    private javax.swing.JButton btnZoomOut;
    private javax.swing.JCheckBox chkDifficult;
    private javax.swing.JCheckBox chkOccluded;
    private javax.swing.JCheckBox chkTruncated;
    private javax.swing.JComboBox<ComboEntity> cmbClass;
    private javax.swing.JComboBox<String> cmbPose;
    private javax.swing.JComboBox<ComboEntity> cmbState;
    private Components.BoundingBoxComponent cmpDetectionWindow;
    private javax.swing.JDialog dlgDetection;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblBrightness;
    private javax.swing.JLabel lblClass;
    private javax.swing.JLabel lblContrast;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblPose;
    private javax.swing.JLabel lblSearch;
    private javax.swing.JLabel lblState;
    private javax.swing.JPanel pnlId;
    private javax.swing.JPanel pnlImage;
    private javax.swing.JScrollPane scrImage;
    private javax.swing.JSlider sliBrightness;
    private javax.swing.JSlider sliContrast;
    private javax.swing.JSlider sliZoom;
    private javax.swing.JScrollPane srcObjects;
    private javax.swing.JTable tblObjects;
    private javax.swing.JToolBar tooPrincipal;
    private javax.swing.JToolBar tooZoom;
    private javax.swing.JLabel txtBrightness;
    private javax.swing.JLabel txtContrast;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JLabel txtZoom;
    // End of variables declaration//GEN-END:variables

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
            loadData();
            loadForm();
            loadResource();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void loadControl() {
        this.setSize(new Dimension(600, 500));
        this.setTitle("Image detection");

        this.tooPrincipal.setFloatable(Boolean.FALSE);
        this.tooZoom.setFloatable(Boolean.FALSE);

        this.btnCursor.addActionListener(this::cursor);
        this.btnHand.addActionListener(this::hand);
        this.btnResize.addActionListener(this::resize);
        this.btnBack.addActionListener(this::back);
        this.btnNext.addActionListener(this::next);
        this.btnZoomIn.addActionListener(this::zoomIn);
        this.btnZoomOut.addActionListener(this::zoomOut);
        this.btnZoomDefault.addActionListener(this::zoomDefault);
        this.btnSearch.addActionListener(this::search);
        this.btnSave.addActionListener(this::saveState);
        this.btnRefreshState.addActionListener(this::refreshState);
        this.btnRefreshClass.addActionListener(this::refreshClass);
        this.btnAddBoundingBox.addActionListener(this::addBoundingBox);
        this.btnDeleteBoundingBox.addActionListener(this::deleteBoundingBox);
        this.btnSaveBoundingBox.addActionListener(this::saveBoundingBox);

        this.scrImage.getVerticalScrollBar().setUnitIncrement(20);
        this.scrImage.getHorizontalScrollBar().setUnitIncrement(20);
        this.scrImage.addMouseWheelListener(this::addZoomEvent);

        this.cmpDetectionWindow.loadScrollPanel(this.scrImage);

        // CONFIGRUACION DEL ZOOM, BRILLO Y CONTRASTE.
        this.sliZoom.addChangeListener(this::changeZoom);
        this.sliBrightness.addChangeListener(this::changeIntensity);
        this.sliContrast.addChangeListener(this::changeIntensity);
        this.sliZoom.setMaximum(ApplicationSettings.MAX_ZOOM);
        this.sliZoom.setMinimum(ApplicationSettings.MIN_ZOOM);
        this.sliBrightness.setMinimum((int) ApplicationSettings.MIN_BRIGHTNESS);
        this.sliBrightness.setMaximum((int) ApplicationSettings.MAX_BRIGHTNESS);
        this.sliContrast.setMinimum((int) ApplicationSettings.MIN_CONTRAST);
        this.sliContrast.setMaximum((int) ApplicationSettings.MAX_CONTRAST);

        ListSelectionModel classSelectionModel = this.tblObjects.getSelectionModel();
        classSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        classSelectionModel.addListSelectionListener(this::selectObject);

        String columns[] = {"Id", "Class", "Bounding box"};
        DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
        this.tblObjects.setModel(defaultTableModel);
    }

    public void loadData() throws Exception {
        loadStates();
        loadPoses();
        loadClasses();
    }

    private void loadForm() {
        setZoom(ApplicationSettings.DEF_ZOOM);
        setBrightness((int) ApplicationSettings.DEF_BRIGHTNESS);
        setContrast((int) ApplicationSettings.DEF_CONTRAST);
        this.txtSearch.setText(StringHelper.EMPTY);
    }

    private void loadResource() {
        try {
            this.cmpDetectionWindow.clear();
            WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
            WorkspaceEntity workspaceEntity = workspaceLogic.get();
            AnnotationLogic annotationLogic = new AnnotationLogic(workspaceEntity.getAnnotationPath());
            AnnotationEntity annotationEntity = getAnnotationEntity(annotationLogic);

            if (annotationEntity != null) {
                this.LastId = workspaceLogic.getLastImageId() - 1;

                //CARGA LA IMAGEN SELECCIONADA.
                String imagePath = String.format("%s/%s", workspaceEntity.getImagePath(), annotationEntity.getImageName());
                BufferedImage bufferedImage = ImageIO.read(new File(imagePath));
                this.cmpDetectionWindow.loadImage(bufferedImage);
                this.OriginalImage = bufferedImage;
                this.ResultImage = ImageIO.read(new File(imagePath));
                bufferedImage.flush();

                // CARGA LA ANOTACION SELECCIONADA.
                String columns[] = {"Id", "Class", "Bounding box"};
                DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
                ArrayList<DetectionEntity> objects = annotationEntity.getDetections();

                objects.forEach((object) -> {
                    String className = getClassName(workspaceEntity.getClasses(), object.getIdClass());
                    Color classColor = getClassColor(workspaceEntity.getClasses(), object.getIdClass());
                    defaultTableModel.addRow(new Object[]{
                        object.getId(),
                        className,
                        String.format("[%s, %s, %s, %s]", object.getXmin(), object.getYmin(), object.getXmax(), object.getYmax())
                    });
                    this.cmpDetectionWindow.setObjectStatic(object.getXmin(),
                            object.getYmin(), object.getXmax(), object.getYmax(), classColor, className);
                });

                this.tblObjects.setModel(defaultTableModel);

                // CARGAR INFORMACION DEL ESTADO.
                Optional<ImageEntity> resultImage = workspaceEntity.getImages()
                        .stream().filter(p -> p.getId() == this.Id).findFirst();
                if (resultImage.isPresent()) {
                    Optional<StateEntity> resultState = workspaceEntity.getStates()
                            .stream().filter(p -> p.getId() == resultImage.get().getIdState()).findFirst();
                    setSelectedState(resultState.get());
                    this.pnlId.setBackground(resultState.get().getDisplayColor());
                }

                this.txtSearch.setText(String.valueOf(annotationEntity.getId()));
                this.lblId.setText(String.valueOf(annotationEntity.getId()));
            } else {
                this.txtSearch.setText(StringHelper.EMPTY);
                this.lblId.setText(StringHelper.EMPTY);
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void loadStates() throws Exception {
        this.cmbState.removeAllItems();
        this.cmbState.addItem(ComboEntity.getDefault());
        WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
        WorkspaceEntity workspaceEntity = workspaceLogic.get();
        workspaceEntity.getStates().forEach(p -> {
            this.cmbState.addItem(new ComboEntity(p.getId(), p.getName()));
        });
    }

    private void loadPoses() {
        this.cmbPose.removeAllItems();
        this.cmbPose.addItem("Frontal");
    }

    private void loadClasses() throws Exception {
        this.cmbClass.removeAllItems();
        WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
        WorkspaceEntity workspaceEntity = workspaceLogic.get();
        workspaceEntity.getClasses().forEach(p -> {
            this.cmbClass.addItem(new ComboEntity(p.getId(), p.getName()));
        });
    }

    /**
     * SELECCIONA UNA FILA DE LA TABLA DE OBJETOS.
     *
     * @param e EVENTO.
     */
    private void selectObject(ListSelectionEvent e) {
        try {
            if (!e.getValueIsAdjusting()) {
                int selectedRow = this.tblObjects.getSelectedRow();
                this.cmpDetectionWindow.selectObject(selectedRow);
                this.IdObject = TableHelper.getSelectedId(this.tblObjects);
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private String getClassName(ArrayList<ClassEntity> classEntitys, int idClass) {
        Optional<ClassEntity> optional = classEntitys.stream().filter(p -> p.getId() == idClass)
                .findFirst();
        return optional.isPresent() ? optional.get().getName() : StringHelper.EMPTY;
    }

    private Color getClassColor(ArrayList<ClassEntity> classEntitys, int idClass) {
        Optional<ClassEntity> optional = classEntitys.stream().filter(p -> p.getId() == idClass)
                .findFirst();
        return optional.isPresent() ? optional.get().getEdgeColor() : Color.GREEN;
    }

    private void cursor(ActionEvent e) {
        try {
            this.cmpDetectionWindow.setObjectEditable(Boolean.FALSE);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void hand(ActionEvent e) {
        try {
            this.cmpDetectionWindow.setHandMode();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void resize(ActionEvent e) {
        try {
            this.cmpDetectionWindow.setObjectEditable(Boolean.TRUE);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA SIGUIENTE IMAGEN.
     *
     * @param e EVENTO.
     */
    private void next(ActionEvent e) {
        try {
            if (!(this.Id >= this.LastId)) {
                this.Id++;
                loadForm();
                loadResource();
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA ANTERIOR IMAGEN.
     *
     * @param e EVENTO.
     */
    private void back(ActionEvent e) {
        try {
            if (!(this.Id == ApplicationSettings.PREFIX_ID)) {
                this.Id--;
                loadForm();
                loadResource();
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * BUSCA UNA IMAGEN ESPECIFICA.
     *
     * @param e EVENTO.
     */
    private void search(ActionEvent e) {
        try {
            if (!(StringHelper.isNullOrEmpty(txtSearch.getText()))) {
                int idImage = StringHelper.toInt32(txtSearch.getText());
                if (idImage < ApplicationSettings.PREFIX_ID) {
                    idImage = idImage + (ApplicationSettings.PREFIX_ID - 1);
                }
                if (idImage > this.LastId) {
                    idImage = this.LastId;
                }
                this.Id = idImage;
                loadForm();
                loadResource();
            }
        } catch (NumberFormatException exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * REALIZA UN ACERCAMIENTO A LA IMAGEN.
     *
     * @param e EVENTO.
     */
    private void zoomIn(ActionEvent e) {
        try {
            setZoom(this.sliZoom.getValue() + 1);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * REALIZA UN ALEJAMIENTO A LA IMAGEN.
     *
     * @param e EVENTO.
     */
    private void zoomOut(ActionEvent e) {
        try {
            setZoom(this.sliZoom.getValue() - 1);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA IMAGEN SIN ACERCAMIENTO NI ALEJAMIENTO.
     *
     * @param e EVENTO.
     */
    private void zoomDefault(ActionEvent e) {
        try {
            setZoom(ApplicationSettings.DEF_ZOOM);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * ASIGNA UN VALOR PARA EL ZOOM.
     *
     * @param value VALOR.
     */
    private void setZoom(int value) {
        this.sliZoom.setValue(value);
        changeZoom();
    }

    /**
     * ASIGNA UN VALOR PARA EL BRILLO.
     *
     * @param value VALOR.
     */
    private void setBrightness(int value) {
        this.sliBrightness.setValue(value);
        changeIntensity();
    }

    /**
     * ASIGNA UN VALOR PARA EL CONTRASTE.
     *
     * @param value VALOR.
     */
    private void setContrast(int value) {
        this.sliContrast.setValue(value);
        changeIntensity();
    }

    /**
     * CONTROLA EL NIVEL DEL ZOOM.
     *
     * @param e EVENTO.
     */
    private void changeZoom(ChangeEvent e) {
        try {
            changeZoom();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CONTROLA EL NIVEL DE INTENSIDAD.
     *
     * @param e EVENTO.
     */
    private void changeIntensity(ChangeEvent e) {
        try {
            changeIntensity();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MODIFICA EL VALOR DEL ZOOM.
     */
    private void changeZoom() {

        // VALIDAR LOS LIMITES DEL ZOOM.
        if (this.sliZoom.getValue() == ApplicationSettings.MAX_ZOOM) {
            this.sliZoom.setValue(ApplicationSettings.MAX_ZOOM);
        }

        if (this.sliZoom.getValue() == ApplicationSettings.MIN_ZOOM) {
            this.sliZoom.setValue(ApplicationSettings.MIN_ZOOM);
        }

        // ACTUALIZA LOS VALORES DE LOS CONTROLES.
        this.txtZoom.setText(String.valueOf(this.sliZoom.getValue()));

        // ACTUALIZA EL VALOR DEL ZOOM DE LA IMAGEN.
        if (!(this.OriginalImage == null || this.ResultImage == null)) {
            double factor = (double) this.sliZoom.getValue() / ApplicationSettings.DEF_ZOOM;
            this.cmpDetectionWindow.setZoom(factor);
        }
    }

    /**
     * MODIFICA EL VALOR DE LA INTENSIDAD.
     */
    private void changeIntensity() {
        // ACTUALIZA LOS VALORES DE LOS CONTROLES.
        this.txtBrightness.setText(String.valueOf(this.sliBrightness.getValue()));
        this.txtContrast.setText(String.valueOf(this.sliContrast.getValue()));

        // ACTUALIZA LA INTENSIDAD DE LA IMAGEN.
        if (!(this.OriginalImage == null || this.ResultImage == null)) {
            float factorBrightness = (float) (this.sliBrightness.getValue() / ApplicationSettings.DEF_BRIGHTNESS);
            float factorContrast = (float) ((this.sliContrast.getValue() - ApplicationSettings.DEF_CONTRAST) / 2);
            RescaleOp rescale = new RescaleOp(factorBrightness, factorContrast, null);
            rescale.filter(this.OriginalImage, this.ResultImage);
            this.cmpDetectionWindow.loadImage(this.ResultImage);
        }
    }

    /**
     * VUELVE A CONSULTAR EL LISTADO DE LOS ESTADOS.
     *
     * @param e EVENTO.
     */
    private void refreshState(ActionEvent e) {
        try {
            int selectedIndex = this.cmbState.getSelectedIndex();
            loadStates();
            this.cmbState.setSelectedIndex(selectedIndex);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * VUELVE A CONSULTAR EL LISTADO DE LAS CLASES.
     *
     * @param e EVENTO.
     */
    private void refreshClass(ActionEvent e) {
        try {
            int selectedIndex = this.cmbClass.getSelectedIndex();
            loadClasses();
            this.cmbClass.setSelectedIndex(selectedIndex);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * SELECCIONA UN ELEMENTO EN EL COMBO DE ESTADO DE ACUERDO AL MODELO
     * SELECCIONADO.
     *
     * @param stateEntity MODELO DE ESTADO.
     */
    private void setSelectedState(StateEntity stateEntity) {
        if (stateEntity == null) {
            this.cmbState.setSelectedIndex(0);
        } else {
            for (int i = 0; i < this.cmbState.getItemCount(); i++) {
                ComboEntity comboModel = this.cmbState.getItemAt(i);
                if (comboModel.getValue() == stateEntity.getId()) {
                    this.cmbState.setSelectedIndex(i);
                }
            }
        }
    }

    /**
     * AGREGA UN OBJETO A LA IMAGEN.
     *
     * @param e EVENTO.
     */
    private void addBoundingBox(ActionEvent e) {
        try {
            showAddBoundingBoxWindow();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * ELIMINA UN OBJETO DE LA IMAGEN.
     *
     * @param e EVENTO.
     */
    private void deleteBoundingBox(ActionEvent e) {

        int result = MessageHelper.showWarningMessage("You are sure to delete the object?");
        if (MessageHelper.isWarningYes(result)) {
            try {
                if (this.IdObject > 0) {
                    WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                    WorkspaceEntity workspaceEntity = workspaceLogic.get();
                    AnnotationLogic annotationLogic = new AnnotationLogic(workspaceEntity.getAnnotationPath());
                    annotationLogic.deleteDetection(this.Id, this.IdObject);
                    loadResource();
                    changeIntensity();
                }
            } catch (Exception exception) {
                MessageHelper.showErrorMessage(exception);
            }
        }
    }

    /**
     * GUARDA LA INFORMACION DEL ESTADO.
     *
     * @param e EVENTO.
     */
    private void saveState(ActionEvent e) {
        int result = MessageHelper.showWarningMessage("Do you want to save the changes?");
        if (MessageHelper.isWarningYes(result)) {
            try {
                ComboEntity comboModel = (ComboEntity) this.cmbState.getSelectedItem();
                if (comboModel.getValue() != 0) {
                    WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                    WorkspaceEntity workspaceEntity = workspaceLogic.get();
                    ImageEntity imageEntity = ImageEntity.updateIdState(
                            comboModel.getValue()
                    );
                    workspaceLogic.updateState(imageEntity, this.Id);

                    Optional<StateEntity> resultState = workspaceEntity.getStates()
                            .stream().filter(p -> p.getId() == comboModel.getValue()).findFirst();
                    this.pnlId.setBackground(resultState.get().getDisplayColor());

                    MessageHelper.showSuccessMessage("The changes were saved correctly!");
                }
            } catch (Exception exception) {
                MessageHelper.showErrorMessage(exception);
            }
        }
    }

    /**
     * MUESTRA EL CUADRO DE DIALOGO PARA AGREGAR UN OBJETO.
     */
    private void showAddBoundingBoxWindow() {
        this.dlgDetection.setVisible(Boolean.TRUE);
        this.dlgDetection.setSize(new Dimension(400, 300));
        this.dlgDetection.setAlwaysOnTop(Boolean.TRUE);
        this.dlgDetection.setResizable(Boolean.FALSE);
        this.dlgDetection.setLocationRelativeTo(null);
        this.dlgDetection.setTitle("Add bounding box");
        try {
            ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Icons/icons8-tarea-del-sistema-100.png"));
            this.dlgDetection.setIconImage(imageIcon.getImage());
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
        this.btnSaveBoundingBox.requestFocus();
    }

    /**
     * DEVUELVE LOS DATOS DE UNA ANOTACION SIN LANZAR UNA EXCEPCION.
     *
     * @param annotationLogic LOGICA DE LA ANOTACION.
     * @return ENTIDAD DE LA ANOTACION.
     */
    private AnnotationEntity getAnnotationEntity(AnnotationLogic annotationLogic) {
        try {
            return annotationLogic.get(this.Id);
        } catch (Exception e) {
            return null;
        }
    }

    private void saveBoundingBox(ActionEvent e) {
        try {
            ComboEntity comboEntity = (ComboEntity) this.cmbClass.getSelectedItem();

            if (comboEntity == null) {
                throw new Exception("First, select a class!");
            }

            String pose = (String) this.cmbPose.getSelectedItem();
            if (StringHelper.isNullOrEmpty(pose)) {
                throw new Exception("First, select a pose!");
            }

            BoundingBoxComponent.BoundingBox object = this.cmpDetectionWindow.getObject();
            if (object == null) {
                throw new Exception("First, draw a valid object!");
            }

            WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
            WorkspaceEntity workspaceEntity = workspaceLogic.get();
            AnnotationLogic annotationLogic = new AnnotationLogic(workspaceEntity.getAnnotationPath());

            DetectionEntity detectionEntity = new DetectionEntity(
                    annotationLogic.getNextDetection(this.Id),
                    comboEntity.getValue(), 0, 0, pose,
                    this.chkTruncated.isSelected(),
                    this.chkDifficult.isSelected(),
                    this.chkOccluded.isSelected(),
                    Math.min(object.getXmin(), object.getXmax()),
                    Math.min(object.getYmin(), object.getYmax()),
                    Math.max(object.getXmin(), object.getXmax()),
                    Math.max(object.getYmin(), object.getYmax()));

            annotationLogic.addDetection(this.Id, detectionEntity);

            this.dlgDetection.dispose();
            loadResource();
            changeIntensity();
        } catch (Exception exception) {
            this.dlgDetection.dispose();
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * AGREGA EL EVENTO DEL ZOOM CUANDO SE PRESIONA CONTROL.
     *
     * @param e EVENTO.
     */
    private void addZoomEvent(MouseWheelEvent e) {
        try {
            if (e.isControlDown()) {
                if (e.getWheelRotation() < 0) {
                    setZoom(this.sliZoom.getValue() + 1);
                } else {
                    setZoom(this.sliZoom.getValue() - 1);
                }
                this.scrImage.setWheelScrollingEnabled(false);
            } else {
                this.scrImage.setWheelScrollingEnabled(true);
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }
}
