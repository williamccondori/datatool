package Business;

import Entity.ClassEntity;
import Entity.DetectionEntity;
import Entity.VocImportEntity;
import Helpers.ApplicationSettings;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class ImportVocLogic {

    private final String AnnotationFolder;

    public ImportVocLogic(String AnnotationFolder) {
        this.AnnotationFolder = AnnotationFolder;
    }

    public VocImportEntity getDetections(String fileName) throws Exception {
        ArrayList<DetectionEntity> detections = new ArrayList<>();
        ArrayList<ClassEntity> classes = new ArrayList<>();

        File file = new File(String.format("%s/%s", this.AnnotationFolder, fileName));

        SAXBuilder sAXBuilder = new SAXBuilder();
        Document document = (Document) sAXBuilder.build(file);
        Element root = document.getRootElement();
        List<Element> elements = root.getChildren("object");

        int objectId = ApplicationSettings.PREFIX_ID;
        int classId = ApplicationSettings.PREFIX_ID;

        for (Element element : elements) {

            // This section of code, register the class name to Datatool.
            
            Element name = element.getChild("name");
            String className = name.getValue();
            
            if (!classes.stream().anyMatch(x -> x.getName()
                    .equals(className.toUpperCase())))
            {
                ClassEntity classEntity = new ClassEntity(
                    classId, className, className, className, Color.GREEN,
                    Color.GREEN, Color.GREEN);
                classes.add(classEntity);
                classId++;
            }
            
            Element truncated = element.getChild("truncated");
            Element difficult = element.getChild("difficult");
            Element occluded = element.getChild("occluded");

            DetectionEntity detectionEntity = DetectionEntity.setObjectData(objectId, ApplicationSettings.PREFIX_ID, 0, 0,
                    element.getChild("pose").getText(),
                    "0".equals(truncated == null ? "0" : truncated.getText()) ? Boolean.FALSE : Boolean.TRUE,
                    "0".equals(difficult == null ? "0" : difficult.getText()) ? Boolean.FALSE : Boolean.TRUE,
                    "0".equals(occluded == null ? "0" : occluded.getText()) ? Boolean.FALSE : Boolean.TRUE);

            List<Element> points = element.getChildren("bndbox");

            points.forEach(point -> {
                Element _xMin = point.getChild("xmin");
                Element _xMax = point.getChild("xmax");
                Element _yMin = point.getChild("ymin");
                Element _yMax = point.getChild("ymax");

                double xMin = Double.parseDouble(_xMin == null ? "0" : _xMin.getValue());
                double xMax = Double.parseDouble(_xMax == null ? "0" : _xMax.getValue());
                double yMin = Double.parseDouble(_yMin == null ? "0" : _yMin.getValue());
                double yMax = Double.parseDouble(_yMax == null ? "0" : _yMax.getValue());

                detectionEntity.setObjectDetection((int) xMin, (int) yMin, (int) xMax, (int) yMax);
            });

            detections.add(detectionEntity);
            objectId++;
        }

        return VocImportEntity.create(detections, classes);
    }
}
