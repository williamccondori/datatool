package Business;

import java.util.ArrayList;
import java.util.Collections;

public class ExportDistributionLogic {

    private final ArrayList<Integer> ImagesId;

    private final ArrayList<Integer> ImagesIdTrain;
    private final ArrayList<Integer> ImagesIdTest;
    private final ArrayList<Integer> ImagesIdValidation;

    public ExportDistributionLogic(ArrayList<Integer> ImagesId) {
        this.ImagesId = ImagesId;
        this.ImagesIdTrain = new ArrayList();
        this.ImagesIdTest = new ArrayList();
        this.ImagesIdValidation = new ArrayList();
    }

    public void distribute(int train, int test, int validation, boolean random) throws Exception {
        if ((train + test + validation) == 100) {
            if (random) {
                Collections.shuffle(this.ImagesId);
            }

            int total = this.ImagesId.size();

            double porTrain = (train * 0.01) * total;
            double porTest = (test * 0.01) * total;

            int trainDistribution = (int) Math.floor(porTrain); 
            int testDistribution = (int) Math.floor(porTest);

            for (int i = 0; i < total; i++) {
                if (i < trainDistribution) {
                    this.ImagesIdTrain.add(ImagesId.get(i));
                } else if (i < trainDistribution + testDistribution) {
                    this.ImagesIdTest.add(ImagesId.get(i));
                } else {
                    this.ImagesIdValidation.add(ImagesId.get(i));
                }
            }
        } else {
            throw new Exception("Invalid distribution!");
        }
    }

    public ArrayList<Integer> getImagesTrain() {
        return this.ImagesIdTrain;
    }

    public ArrayList<Integer> getImagesTest() {
        return this.ImagesIdTest;
    }

    public ArrayList<Integer> getImagesValidation() {
        return this.ImagesIdValidation;
    }
}
