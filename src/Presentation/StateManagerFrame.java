package Presentation;

import Business.WorkspaceLogic;
import Entity.StateEntity;
import Helpers.ApplicationSettings;
import Helpers.ColorHelper;
import Helpers.MessageHelper;
import Helpers.StringHelper;
import Helpers.TableHelper;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.JColorChooser;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

public class StateManagerFrame extends javax.swing.JInternalFrame {

    private int LastRecord;

    public StateManagerFrame() {
        initComponents();
        loadWindow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrClass = new javax.swing.JScrollPane();
        tblState = new javax.swing.JTable();
        lblId = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        lblName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        lblDescription = new javax.swing.JLabel();
        txtDescription = new javax.swing.JTextField();
        lblDisplayColor = new javax.swing.JLabel();
        txtDisplayColor = new javax.swing.JTextField();
        btnClear = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnChooseDisplayColor = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-hoy-16.png"))); // NOI18N

        tblState.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrClass.setViewportView(tblState);

        lblId.setText("Id:");

        txtId.setText("jTextField1");

        lblName.setText("Name:");

        txtName.setText("jTextField2");

        lblDescription.setText("Description:");

        txtDescription.setText("jTextField3");

        lblDisplayColor.setText("Display color:");

        txtDisplayColor.setText("jTextField5");

        btnClear.setBackground(new java.awt.Color(255, 255, 255));
        btnClear.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnClear.setForeground(new java.awt.Color(0, 102, 153));
        btnClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-borrar-16.png"))); // NOI18N
        btnClear.setText("Clear");
        btnClear.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnClear.setFocusPainted(false);

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 102, 153));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-guardar-16.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSave.setFocusPainted(false);

        btnChooseDisplayColor.setBackground(new java.awt.Color(255, 255, 255));
        btnChooseDisplayColor.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnChooseDisplayColor.setForeground(new java.awt.Color(0, 102, 153));
        btnChooseDisplayColor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-paleta-de-pintura-16.png"))); // NOI18N
        btnChooseDisplayColor.setText("Choose");
        btnChooseDisplayColor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnChooseDisplayColor.setFocusPainted(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDisplayColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(scrClass, javax.swing.GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblDescription, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(lblName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDescription)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblId, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtDisplayColor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnChooseDisplayColor, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrClass, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblId)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDescription))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDisplayColor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDisplayColor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChooseDisplayColor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 196, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChooseDisplayColor;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblDisplayColor;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblName;
    private javax.swing.JScrollPane scrClass;
    private javax.swing.JTable tblState;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtDisplayColor;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
            loadData();
            loadForm();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA EL FUNCIONAMIENTO DE LOS CONTROLES DE LA VENTANA.
     */
    private void loadControl() {
        this.setSize(new Dimension(500, 500));
        this.setResizable(Boolean.FALSE);
        this.setTitle("State manager");
        this.btnChooseDisplayColor.addActionListener(this::chooseDisplayColor);
        this.btnSave.addActionListener(this::save);
        this.btnClear.addActionListener(this::clear);
        this.txtId.setEditable(Boolean.FALSE);
        this.txtDisplayColor.setEditable(Boolean.FALSE);
        ListSelectionModel stateSelectionModel = this.tblState.getSelectionModel();
        stateSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        stateSelectionModel.addListSelectionListener(this::selectState);
    }

    /**
     * CARGA LOS DATOS UTILIZADOS EN LA VENTANA.
     *
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    public void loadData() throws Exception {
        loadStates();
    }

    /**
     * CARGA LOS DATOS PREDETERMINADOS PARA EL FORMULARIO.
     */
    private void loadForm() {
        this.txtId.setText(String.valueOf(this.LastRecord));
        this.txtName.setText(StringHelper.EMPTY);
        this.txtDescription.setText(StringHelper.EMPTY);
        this.txtDisplayColor.setText(ColorHelper.getColor(Color.GREEN));
    }

    /**
     * ASIGNA LOS VALORES A LA TABLA DE LOS ESTADOS.
     *
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    private void loadStates() throws Exception {
        String columns[] = {"Id", "State"};
        DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
        WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
        ArrayList<StateEntity> states = workspaceLogic.get().getStates();
        this.LastRecord = states.isEmpty() ? ApplicationSettings.PREFIX_ID : states.size() + ApplicationSettings.PREFIX_ID;
        states.forEach((p) -> {
            defaultTableModel.addRow(new Object[]{
                p.getId(),
                p.getName()
            });
        });
        this.tblState.setModel(defaultTableModel);
    }

    /**
     * SELECCIONA UNA FILA DE LA TABLA DE LOS ESTADOS.
     *
     * @param e EVENTO.
     */
    private void selectState(ListSelectionEvent e) {
        try {
            if (!e.getValueIsAdjusting()) {
                int idState = TableHelper.getSelectedId(this.tblState);
                if (idState != 0) {
                    WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                    StateEntity stateEntity = workspaceLogic.getStateById(idState);
                    this.txtId.setText(String.valueOf(stateEntity.getId()));
                    this.txtName.setText(stateEntity.getName());
                    this.txtDescription.setText(stateEntity.getDescription());
                    this.txtDisplayColor.setText(ColorHelper.getColor(stateEntity.getDisplayColor()));
                }
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UN CUADRO PARA SELECCIONAR UN COLOR.
     *
     * @param e EVENTO.
     */
    private void chooseDisplayColor(ActionEvent e) {
        try {
            Color color = JColorChooser.showDialog(null, "Choose a color", Color.BLACK);
            if (!(color == null)) {
                this.txtDisplayColor.setText(ColorHelper.getColor(color));
            }
        } catch (HeadlessException exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * GAURDA LOS CAMBIOS REALIZADOS EN EL FORMULARIO.
     *
     * @param e EVENTO.
     */
    private void save(ActionEvent e) {
        int result = MessageHelper.showWarningMessage("Do you want to save the changes?");
        if (MessageHelper.isWarningYes(result)) {
            try {
                StateEntity stateEntity = new StateEntity(
                        Integer.parseInt(txtId.getText()),
                        txtName.getText(),
                        txtDescription.getText(),
                        ColorHelper.setColor(txtDisplayColor.getText()));
                stateEntity.validate();
                WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                workspaceLogic.saveState(stateEntity);
                loadStates();
                loadForm();
                MessageHelper.showSuccessMessage("The changes were saved correctly!");
            } catch (Exception exception) {
                MessageHelper.showErrorMessage(exception);
            }
        }
    }

    /**
     * DESHACE LOS CAMBIOS REALIZADOS EN EL FORMULARIO.
     *
     * @param e EVENTO.
     */
    private void clear(ActionEvent e) {
        try {
            loadStates();
            loadForm();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }
}
