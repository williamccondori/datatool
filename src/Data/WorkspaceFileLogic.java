package Data;

import Entity.WorkspaceEntity;
import Helpers.ApplicationSettings;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

public class WorkspaceFileLogic {

    private final String WorkspaceFolderPath;

    public WorkspaceFileLogic(String WorkspaceFolderPath) {
        this.WorkspaceFolderPath = WorkspaceFolderPath;
    }

    /**
     * LEE EL ARCHIVO DEL ESPACIO DE TRABAJO.
     *
     * @return ENTIDAD CON LOS DATOS DEL ESPACIO DE TRABAJO.
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    public WorkspaceEntity read() throws Exception {
        String json;
        Gson gson = new Gson();
        try (FileReader fileReader = new FileReader(String.format("%s/%s.%s", this.WorkspaceFolderPath, ApplicationSettings.APP_FILE, ApplicationSettings.APP_FILE_TYPE));
                BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            json = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        }
        return gson.fromJson(json, WorkspaceEntity.class);
    }

    /**
     * ALMACENA EN EL ARCHIVO DEL ESPACIO DE TRABAJO.
     *
     * @param workspaceEntity ENTIDAD CON LOS DATOS DEL ESPACIO DE TRABAJO.
     * @throws Exception EXCEPCION NO CONTROLADA.
     */
    public void save(WorkspaceEntity workspaceEntity) throws Exception {
        Gson gson = new Gson();
        try (FileWriter fileWriter = new FileWriter(String.format("%s/%s.%s", this.WorkspaceFolderPath, ApplicationSettings.APP_FILE, ApplicationSettings.APP_FILE_TYPE));
                PrintWriter printWriter = new PrintWriter(fileWriter)) {
            String json = gson.toJson(workspaceEntity);
            printWriter.println(json);
            printWriter.close();
            fileWriter.close();
        }
    }
}
