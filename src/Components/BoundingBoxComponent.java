package Components;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

public class BoundingBoxComponent extends JPanel {

    private static int RECTANGLE_SIZE = 6;

    private OperationType Operation;

    // IMAGEN.
    private BufferedImage Image;
    private double ImageScale;

    // OBJETO.
    private BoundingBox Object;
    private BoundingBox ObjectSelected;
    private ArrayList<BoundingBox> ObjectsStatic;

    // RECTANGULO EDITABLE.
    private Rectangle2D RectangleEditable = new Rectangle2D.Double();
    private Rectangle2D[] RectangleEditablePoints = new Rectangle2D[8];

    // HERRAMIENTA MANO.
    private JScrollPane ScrollPanel;

    /**
     * CONSTRUCTOR.
     */
    public BoundingBoxComponent() {
        loadComponent();
    }

    /**
     * CARGA E INICIALIZA LAS VARIABLES DEL COMPONENTE.
     */
    private void loadComponent() {
        this.setBackground(Color.BLACK);
        this.Image = null;
        this.ImageScale = 1.0;
        this.Object = null;
        this.ObjectSelected = null;
        this.ObjectsStatic = new ArrayList<>();
        this.Operation = OperationType.DEFAULT;
        this.ScrollPanel = null;
        loadListeners();
    }

    /**
     * CARGA LA CLASE CONTENEDORA DE LOS EVENTOS.
     */
    private void loadListeners() {
        DrawObjectListener drawObjectListener = new DrawObjectListener();
        this.addMouseListener(drawObjectListener);
        this.addMouseMotionListener(drawObjectListener);
    }

    /**
     * CARGA UNA IMAGEN PARA SER VISUALIZADA EN EL PANEL.
     *
     * @param image IMAGEN.
     */
    public void loadImage(BufferedImage image) {
        this.Image = image;
        repaint();
    }

    /**
     * CARGA EL COMPONENTE SCROLL.
     *
     * @param scrollPanel COMPONENTE DE SCROLL.
     */
    public void loadScrollPanel(JScrollPane scrollPanel) {
        this.ScrollPanel = scrollPanel;
    }

    /**
     * DIBUJA LOS RECTANGULOS CORRESPONDIENTES EN EL PANEL.
     *
     * @param g LIBRERIA DE GRAFICOS.
     * @param label ETIQUETA PARA UN RECTANGULO ESTATICO.
     * @param xMin PUNTO X MINIMO.
     * @param yMin PUNTO Y MINIMO.
     * @param xMax PUNTO X MAXIMO.
     * @param yMax PUNTO Y MAXIMO.
     * @param isStatic DETERMINA SI EL RECTANGULO ES ESTATICO.
     */
    private void drawBoundingBox(Graphics g, String label, int Xmin, int Ymin, int Xmax, int Ymax, boolean isStatic) {
        if (!(Xmin == Xmax && Ymin == Ymax)) {
            if (isStatic) {
                int px = Math.min(Xmin, Xmax);
                int py = Math.min(Ymin, Ymax);
                int pw = Math.abs(Xmin - Xmax);
                int ph = Math.abs(Ymin - Ymax);
                g.setFont(new Font("Arial", Font.BOLD, 14));
                int labelpositionx = px + 0;
                int labelPositiony = py - 5;
                if (py < 18) {
                    labelpositionx = px + 2;
                    labelPositiony = py + 12;
                }
                g.drawString(label, labelpositionx, labelPositiony);
                g.drawRect(px, py, pw, ph);
            } else {
                int px = Math.min(Xmin, Xmax);
                int py = Math.min(Ymin, Ymax);
                int pw = (int) (Math.abs(Xmin - Xmax));
                int ph = (int) (Math.abs(Ymin - Ymax));
                g.drawRect(px, py, pw, ph);
            }
        }
    }

    /**
     * OBTIENE EL TAMANIO DEL PANEL PARA TAREAS DE MOVIMIENTO.
     *
     * @return TAMANIO DEL PANEL.
     */
    @Override
    public Dimension getPreferredSize() {
        if (this.Image == null) {
            return new Dimension(100, 100);
        } else {
            int width = (int) (this.ImageScale * this.Image.getWidth());
            int height = (int) (this.ImageScale * this.Image.getHeight());
            return new Dimension(width, height);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (this.Image != null) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.scale(this.ImageScale, this.ImageScale);
            g2.drawImage(this.Image, 0, 0, null);

            // RECTANGULOS ESTATICOS.
            this.ObjectsStatic.forEach(p -> {
                g2.scale(1.0, 1.0);
                g2.setColor(p.getColor());
                g2.setStroke(new BasicStroke(2f));
                drawBoundingBox(g2, p.getLabel(), p.getXmin(), p.getYmin(), p.getXmax(), p.getYmax(), Boolean.TRUE);
            });

            // RECTANGULOS ESTATICOS SELECCIONADOS.
            if (this.ObjectSelected != null) {
                Graphics2D gr = (Graphics2D) g2;
                gr.setColor(this.ObjectSelected.getColor());
                drawBoundingBox(gr, this.ObjectSelected.getLabel(), this.ObjectSelected.getXmin(), this.ObjectSelected.getYmin(), this.ObjectSelected.getXmax(), this.ObjectSelected.getYmax(), Boolean.TRUE);
            }

            if (this.Object != null) {
                g2.scale(1.0, 1.0);
                g2.setColor(this.Object.getColor());

                if (this.Operation == OperationType.RESIZE) {
                    int rectangleSize = RECTANGLE_SIZE / 2;

                    this.RectangleEditablePoints[0] = new Rectangle2D.Double(Object.getXmin() - 3, Object.getYmin() - 3, RECTANGLE_SIZE, RECTANGLE_SIZE);
                    this.RectangleEditablePoints[1] = new Rectangle2D.Double(Object.getXmax() - 3, Object.getYmax() - 3, RECTANGLE_SIZE, RECTANGLE_SIZE);

                    // CALCULA EL PUNTO MEDIO Y EL PUNTO MAXIMO EN EL EJE X.
                    int xMax = (int) (Object.getXmax());
                    int xCenter = (int) ((Object.getXmin() + Object.getXmax()) / 2);

                    // CALCULA EL PUNTO MEDIO Y EL PUNTO MAXIMO EN EL EJE Y.
                    int yMax = (int) (Object.getYmax());
                    int yCenter = (int) ((Object.getYmin() + Object.getYmax()) / 2);

                    this.RectangleEditable.setRect(this.RectangleEditablePoints[0].getCenterX(), this.RectangleEditablePoints[0].getCenterY(),
                            Math.abs(this.RectangleEditablePoints[1].getCenterX() - this.RectangleEditablePoints[0].getCenterX()),
                            Math.abs(this.RectangleEditablePoints[1].getCenterY() - this.RectangleEditablePoints[0].getCenterY()));

                    yMax = (int) (this.Object.getYmin() > this.Object.getYmax() ? yMax : this.RectangleEditable.getMaxY());
                    xMax = (int) (this.Object.getXmin() > this.Object.getXmax() ? xMax : this.RectangleEditable.getMaxX());

                    // SE ASIGNAN LOS VALORES AL ARREGLO DE RECTANGULOS.
                    this.RectangleEditablePoints[2] = new Rectangle2D.Double(xCenter - rectangleSize, yMax - rectangleSize, RECTANGLE_SIZE, RECTANGLE_SIZE);
                    this.RectangleEditablePoints[3] = new Rectangle2D.Double(xMax - rectangleSize, yCenter - rectangleSize, RECTANGLE_SIZE, RECTANGLE_SIZE);
                    this.RectangleEditablePoints[4] = new Rectangle2D.Double(this.RectangleEditable.getMinX() - rectangleSize, yMax - rectangleSize, RECTANGLE_SIZE, RECTANGLE_SIZE);
                    this.RectangleEditablePoints[5] = new Rectangle2D.Double(xMax - rectangleSize, this.RectangleEditable.getMinY() - rectangleSize, RECTANGLE_SIZE, RECTANGLE_SIZE);
                    this.RectangleEditablePoints[6] = new Rectangle2D.Double(xCenter - rectangleSize, this.RectangleEditable.getMinY() - rectangleSize, RECTANGLE_SIZE, RECTANGLE_SIZE);
                    this.RectangleEditablePoints[7] = new Rectangle2D.Double(this.RectangleEditable.getMinX() - rectangleSize, yCenter - rectangleSize, RECTANGLE_SIZE, RECTANGLE_SIZE);

                    for (Rectangle2D pointRectangle : this.RectangleEditablePoints) {
                        g2.fill(pointRectangle);
                    }
                }
                drawBoundingBox(g2, null, this.Object.getXmin(), this.Object.getYmin(), this.Object.getXmax(), this.Object.getYmax(), Boolean.FALSE);
            }
        }
    }

    /**
     * MODIFICA LA ESCALA DE VISUALIZACION DE LA IMAGEN.
     *
     * @param imageScale ESCALA DE LA IMAGEN.
     */
    public void setZoom(double imageScale) {
        this.ImageScale = imageScale;
        revalidate();
        repaint();
    }

    /**
     * ESTABLECE SI LOS OBJETOS SON EDITABLES O NO.
     *
     * @param isEditable INDICADOR.
     */
    public void setObjectEditable(boolean isEditable) {
        this.Operation = isEditable ? OperationType.RESIZE : OperationType.DEFAULT;
        repaint();
    }

    public void setHandMode() {
        this.Operation = OperationType.HAND;
    }

    public void setObjectStatic(int xmin, int ymin, int xmax, int ymax, Color color, String className) {
        this.ObjectsStatic.add(new BoundingBox(xmin, ymin, xmax, ymax, color, className));
    }

    /**
     * OBTIENE EL ULTIMO OBJETO CREADO.
     *
     * @return COORDENADAS DEL NUEVO OBJETO.
     */
    public BoundingBox getObject() {
        BoundingBox boundingBox = this.Object;

        if (boundingBox == null) {
            return null;
        }

        int w = (int) (this.ImageScale * this.Image.getWidth());
        int h = (int) (this.ImageScale * this.Image.getHeight());

        int xMin = (int) (this.ImageScale * boundingBox.getXmin());
        int xMax = (int) (this.ImageScale * boundingBox.getXmax());
        int yMin = (int) (this.ImageScale * boundingBox.getYmin());
        int yMax = (int) (this.ImageScale * boundingBox.getYmax());

        if (!(xMin <= w && xMax <= w & (Math.abs(xMax - xMin) <= w) && yMin <= h && yMax <= h && (Math.abs(yMin - yMax) <= h))) {
            return null;
        }

        if (xMin < 0 || xMax < 0 || yMin < 0 || yMax < 0) {
            return null;
        }

        if (Math.abs(xMax - xMin) * Math.abs(yMax - yMin) < 25) {
            return null;
        }

        return this.Object;
    }

    /**
     * RESALTA UN OBJETO SEGUN SU INDICE.
     *
     * @param index INDICE.
     */
    public void selectObject(int index) {
        if (index > -1) {
            BoundingBox object = this.ObjectsStatic.get(index);
            this.ObjectSelected = new BoundingBox(
                    object.getXmin(),
                    object.getYmin(),
                    object.getXmax(),
                    object.getYmax(),
                    Color.WHITE, object.getLabel());
            repaint();
        }
    }

    /**
     * LIMPIA EL COMPONENTE DE TODOS LOS OBJETOS.
     */
    public void clear() {
        this.Object = null;
        this.ObjectSelected = null;
        this.ObjectsStatic = new ArrayList<>();
        repaint();
    }

    public class BoundingBox {

        private int Xmin;
        private int Ymin;
        private int Xmax;
        private int Ymax;
        private Color Color;
        private String Label;

        public BoundingBox() {
            this.Xmin = this.Ymin = this.Xmax = this.Ymax = 0;
            this.Color = Color.GREEN;
        }

        public BoundingBox(int Xmin, int Ymin, int Xmax, int Ymax, Color Color, String Label) {
            this.Xmin = Xmin;
            this.Ymin = Ymin;
            this.Xmax = Xmax;
            this.Ymax = Ymax;
            this.Color = Color;
            this.Label = Label;
        }

        public int getXmin() {
            return Xmin;
        }

        public int getYmin() {
            return Ymin;
        }

        public int getXmax() {
            return Xmax;
        }

        public int getYmax() {
            return Ymax;
        }

        public Color getColor() {
            return Color;
        }

        public String getLabel() {
            return Label;
        }

        public void setStartPoint(int Xmin, int Ymin) {
            this.Xmin = Xmin;
            this.Ymin = Ymin;
        }

        public void setEndPoint(int Xmax, int Ymax) {
            this.Xmax = Xmax;
            this.Ymax = Ymax;
        }
    }

    private class DrawObjectListener extends MouseAdapter {

        private int Position;
        private int X;
        private int Y;

        public DrawObjectListener() {
            this.X = 0;
            this.Y = 0;
            this.Position = -1;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            AffineTransform affineTransform = AffineTransform.getScaleInstance(1.0 / ImageScale, 1.0 / ImageScale);
            Point2D point2D = affineTransform.transform(e.getPoint(), null);
            switch (Operation) {
                case RESIZE:
                    for (int i = 0; i < RectangleEditablePoints.length; i++) {
                        if (RectangleEditablePoints[i] != null) {
                            if (RectangleEditablePoints[i].contains(point2D)) {
                                this.Position = i;
                                return;
                            }
                        }
                    }
                    break;
                case HAND:
                    X = e.getX();
                    Y = e.getY();
                    break;
                default:
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        Object = new BoundingBox();
                        if (Object != null) {
                            Object.setStartPoint((int) point2D.getX(), (int) point2D.getY());
                        }
                    }
                    break;
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (Operation == OperationType.DEFAULT) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    AffineTransform affineTransform = AffineTransform.getScaleInstance(1.0 / ImageScale, 1.0 / ImageScale);
                    Point2D point2D = affineTransform.transform(e.getPoint(), null);
                    if (Object != null) {
                        Object.setEndPoint((int) point2D.getX(), (int) point2D.getY());
                    }
                    repaint();
                }
            } else {
                this.Position = -1;
            }
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            AffineTransform affineTransform = AffineTransform.getScaleInstance(1.0 / ImageScale, 1.0 / ImageScale);
            Point2D point2D = affineTransform.transform(e.getPoint(), null);
            switch (Operation) {
                case RESIZE:
                    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    if (this.Position == -1) {
                        return;
                    }
                    RectangleEditablePoints[this.Position].setRect(point2D.getX(), point2D.getY(), RectangleEditablePoints[this.Position].getWidth(), RectangleEditablePoints[this.Position].getHeight());
                    switch (this.Position) {
                        case 0:
                            setCursor(new Cursor(Cursor.NW_RESIZE_CURSOR));
                            Object.setStartPoint((int) point2D.getX() + 3, (int) point2D.getY() + 3);
                            break;
                        case 1:
                            setCursor(new Cursor(Cursor.SE_RESIZE_CURSOR));
                            Object.setEndPoint((int) point2D.getX() + 3, (int) point2D.getY() + 3);
                            break;
                        case 2:
                            setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
                            Object.setEndPoint((int) Object.getXmax(), (int) point2D.getY() + 3);
                            break;
                        case 3:
                            setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
                            Object.setEndPoint((int) point2D.getX() + 3, (int) Object.getYmax());
                            break;
                        case 4:
                            setCursor(new Cursor(Cursor.SW_RESIZE_CURSOR));
                            Object.setStartPoint((int) point2D.getX() + 3, (int) Object.getYmin());
                            Object.setEndPoint((int) Object.getXmax(), (int) point2D.getY() + 3);
                            break;
                        case 5:
                            setCursor(new Cursor(Cursor.NE_RESIZE_CURSOR));
                            Object.setStartPoint((int) Object.getXmin(), (int) point2D.getY() + 3);
                            Object.setEndPoint((int) point2D.getX() + 3, (int) Object.getYmax());
                            break;
                        case 6:
                            setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
                            Object.setStartPoint((int) Object.getXmin(), (int) point2D.getY() + 3);
                            break;
                        case 7:
                            setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
                            Object.setStartPoint((int) point2D.getX() + 3, (int) Object.getYmin());
                            break;
                    }
                    repaint();
                    break;
                case HAND:
                    setCursor(new Cursor(Cursor.MOVE_CURSOR));
                    JViewport viewPort = ScrollPanel.getViewport();
                    Point point = viewPort.getViewPosition();
                    point.translate(-1 * (e.getX() - X), (e.getY() - Y) * (-1));
                    scrollRectToVisible(new Rectangle(point, viewPort.getSize()));
                    break;
                default:
                    setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
                    if (Object != null) {
                        Object.setEndPoint((int) point2D.getX(), (int) point2D.getY());
                    }
                    repaint();
                    break;
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            switch (Operation) {
                case RESIZE:
                    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    break;
                case HAND:
                    setCursor(new Cursor(Cursor.MOVE_CURSOR));
                    break;
                default:
                    setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
                    break;
            }
        }
    }

    private enum OperationType {
        DEFAULT,
        RESIZE,
        HAND
    }
}
