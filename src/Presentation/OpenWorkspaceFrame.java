package Presentation;

import Business.WorkspaceLogic;
import Entity.WorkspaceEntity;
import Helpers.MessageHelper;
import Helpers.StringHelper;
import Helpers.SystemHelper;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

public class OpenWorkspaceFrame extends javax.swing.JFrame {

    public OpenWorkspaceFrame() {
        initComponents();
        loadWindow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblName = new javax.swing.JLabel();
        lblAuthor = new javax.swing.JLabel();
        lblPath = new javax.swing.JLabel();
        txtPath = new javax.swing.JTextField();
        lblTitle = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtAuthor = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnOpen = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Open Workspace");

        lblName.setText("Name:");

        lblAuthor.setText("Author:");

        lblPath.setText("Path:");

        txtPath.setText("jTextField1");

        lblTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTitle.setForeground(new java.awt.Color(0, 102, 153));
        lblTitle.setText("OPEN WORKSPACE");

        txtName.setText("jTextField1");

        txtAuthor.setText("jTextField2");

        btnSearch.setBackground(new java.awt.Color(255, 255, 255));
        btnSearch.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(0, 102, 153));
        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-búsqueda-16.png"))); // NOI18N
        btnSearch.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSearch.setFocusPainted(false);

        btnOpen.setBackground(new java.awt.Color(255, 255, 255));
        btnOpen.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnOpen.setForeground(new java.awt.Color(0, 102, 153));
        btnOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-de-acuerdo-16.png"))); // NOI18N
        btnOpen.setText("Open");
        btnOpen.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnOpen.setFocusPainted(false);

        btnCancel.setBackground(new java.awt.Color(255, 255, 255));
        btnCancel.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(0, 102, 153));
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cancelar-16.png"))); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnCancel.setFocusPainted(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPath, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtName)
                    .addComponent(txtAuthor)
                    .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblAuthor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 174, Short.MAX_VALUE)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnOpen, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(txtPath)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPath)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(txtPath))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblAuthor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAuthor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 280, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOpen, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel lblAuthor;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPath;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JTextField txtAuthor;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPath;
    // End of variables declaration//GEN-END:variables

    public static void main(String args[]) {
        new StartFrame().setVisible(Boolean.TRUE);
    }

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA EL FUNCIONAMIENTO DE LOS CONTROLES DE LA VENTANA.
     */
    private void loadControl() {
        this.setSize(new Dimension(400, 500));
        this.setLocationRelativeTo(null);
        this.btnSearch.addActionListener(this::search);
        this.btnOpen.addActionListener(this::open);
        this.btnCancel.addActionListener(this::cancel);
        this.txtName.setEditable(Boolean.FALSE);
        this.txtAuthor.setEditable(Boolean.FALSE);
        this.txtPath.setEditable(Boolean.FALSE);
        this.txtName.setText(StringHelper.EMPTY);
        this.txtAuthor.setText(StringHelper.EMPTY);
        this.txtPath.setText(StringHelper.EMPTY);
        try {
            ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Icons/logo_datatool.png"));
            this.setIconImage(imageIcon.getImage());
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UN CUADRO DE DIALOGO PARA OBTENER UNA RUTA.
     *
     * @param e EVENTO.
     */
    private void search(ActionEvent e) {
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File(SystemHelper.getUserHome()));
            fileChooser.setDialogTitle("Choose a direcory");
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                String wokspaceFolderPath = fileChooser.getSelectedFile().getPath();
                WorkspaceLogic workspaceLogic = new WorkspaceLogic(wokspaceFolderPath);
                WorkspaceEntity workspaceEntity = workspaceLogic.get();
                this.txtName.setText(workspaceEntity.getName());
                this.txtAuthor.setText(workspaceEntity.getAuthor());
                this.txtPath.setText(wokspaceFolderPath);
            } else {
                this.txtPath.setText(StringHelper.EMPTY);
            }
        } catch (Exception exception) {
            System.err.println(exception.getLocalizedMessage());
            MessageHelper.showErrorMessage(new Exception("The app.json file is required!"));
        }
    }

    /**
     * ABRE UN ESPACIO DE TRABAJO EXISTENTE.
     *
     * @param e EVENTO.
     */
    private void open(ActionEvent e) {
        try {
            String wokspaceFolderPath = this.txtPath.getText();
            WorkspaceLogic workspaceLogic = new WorkspaceLogic(wokspaceFolderPath);
            WorkspaceEntity workspaceEntity = workspaceLogic.get();
            workspaceEntity.open(wokspaceFolderPath);
            workspaceLogic.open(workspaceEntity);
            ApplicationFrame applicationFrame = new ApplicationFrame();
            applicationFrame.setWorkspacePath(wokspaceFolderPath);
            applicationFrame.loadData(wokspaceFolderPath);
            applicationFrame.setVisible(true);
            this.dispose();
        } catch (Exception exception) {
            System.err.println(exception.getLocalizedMessage());
            MessageHelper.showErrorMessage(new Exception("The app.json file is required!"));
        }
    }

    /**
     * REGRESA A LA VENTANA PRINCIPAL.
     *
     * @param e EVENTO.
     */
    private void cancel(ActionEvent e) {
        StartFrame startFrame = new StartFrame();
        startFrame.setVisible(true);
        this.dispose();
    }
}
