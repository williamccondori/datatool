package Entity;

import Helpers.StringHelper;

public class MetadataEntity {

    private String CompressionType;
    private String DataPrecision;
    private int Height;
    private int Width;
    private int NumberComponents;
    private String Version;
    private String ResolutionUnits;
    private String XResolution;
    private String YResolution;
    private String ThumbnailWidth;
    private String ThumbnailHeight;
    private String NumberTables;
    private String FileTypeName;
    private String FileTypeLongName;
    private String FileTypeMime;
    private String FileExtension;
    private String FileName;
    private String FileSize;
    private String FileDate;
    private String UserComment;

    public String getCompressionType() {
        return CompressionType;
    }

    public String getDataPrecision() {
        return DataPrecision;
    }

    public int getHeight() {
        return Height;
    }

    public int getWidth() {
        return Width;
    }

    public int getNumberComponents() {
        return NumberComponents;
    }

    public String getVersion() {
        return Version;
    }

    public String getResolutionUnits() {
        return ResolutionUnits;
    }

    public String getXResolution() {
        return XResolution;
    }

    public String getYResolution() {
        return YResolution;
    }

    public String getThumbnailWidth() {
        return ThumbnailWidth;
    }

    public String getThumbnailHeight() {
        return ThumbnailHeight;
    }

    public String getNumberTables() {
        return NumberTables;
    }

    public String getFileTypeName() {
        return FileTypeName;
    }

    public String getFileTypeLongName() {
        return FileTypeLongName;
    }

    public String getFileTypeMime() {
        return FileTypeMime;
    }

    public String getFileExtension() {
        return FileExtension;
    }

    public String getFileName() {
        return FileName;
    }

    public String getFileSize() {
        return FileSize;
    }

    public String getFileDate() {
        return FileDate;
    }

    public String getUserComment() {
        return UserComment;
    }

    public MetadataEntity() {
    }

    public void setProperties(String compressionType, String dataPrecision,
            String height, String width, String numberComponents, String version, String resolutionUnits,
            String xResolution, String yResolution, String thumbnailWidth, String thumbnailHeight,
            String numberTables, String fileTypeName, String fileTypeLongName, String fileTypeMime,
            String fileExtension, String fileName, String fileSize, String fileDate, String userComment) {
        this.CompressionType = compressionType;
        this.DataPrecision = dataPrecision;
        this.Height = Integer.parseInt(height.split(" ")[0]);
        this.Width = Integer.parseInt(width.split(" ")[0]);
        this.NumberComponents = Integer.parseInt(StringHelper.isNullOrEmpty(numberComponents) ? "3" : numberComponents);
        this.Version = version;
        this.ResolutionUnits = resolutionUnits;
        this.XResolution = xResolution;
        this.YResolution = yResolution;
        this.ThumbnailWidth = thumbnailWidth;
        this.ThumbnailHeight = thumbnailHeight;
        this.NumberTables = numberTables;
        this.FileTypeName = fileTypeName;
        this.FileTypeLongName = fileTypeLongName;
        this.FileTypeMime = fileTypeMime;
        this.FileExtension = fileExtension;
        this.FileName = fileName;
        this.FileSize = fileSize;
        this.FileDate = fileDate;
        this.UserComment = userComment;
    }
}
