package Entity;

import java.util.Comparator;

public class DetectionEntity {

    private int Id;
    private int IdClass;
    private int IdSubClass;
    private int IdSize;
    private String Name;
    private String Pose;
    private Boolean Truncated;
    private Boolean Difficult;
    private Boolean Occluded;
    private int Xmin;
    private int Ymin;
    private int Xmax;
    private int Ymax;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
    
    public int getId() {
        return Id;
    }

    public int getIdClass() {
        return IdClass;
    }

    public int getIdSubClass() {
        return IdSubClass;
    }

    public int getIdSize() {
        return IdSize;
    }

    public String getPose() {
        return Pose;
    }

    public Boolean getTruncated() {
        return Truncated;
    }

    public Boolean getDifficult() {
        return Difficult;
    }

    public Boolean getOccluded() {
        return Occluded;
    }

    public int getXmin() {
        return this.Xmin < 0 ? 0 : this.Xmin;
    }

    public int getYmin() {
        return this.Ymin < 0 ? 0 : this.Ymin;
    }

    public int getXmax() {
        return this.Xmax < 0 ? 0 : this.Xmax;
    }

    public int getYmax() {
        return this.Ymax < 0 ? 0 : this.Ymax;
    }

    public DetectionEntity() {
    }

    public DetectionEntity(int Id, int IdClass, int IdSubClass, int IdSize, String Pose, Boolean Truncated, Boolean Difficult, Boolean Occluded, int Xmin, int Ymin, int Xmax, int Ymax) {
        this.Id = Id;
        this.IdClass = IdClass;
        this.IdSubClass = IdSubClass;
        this.IdSize = IdSize;
        this.Pose = Pose;
        this.Truncated = Truncated;
        this.Difficult = Difficult;
        this.Occluded = Occluded;
        this.Xmin = Xmin;
        this.Ymin = Ymin;
        this.Xmax = Xmax;
        this.Ymax = Ymax;
    }

    public static DetectionEntity setObjectData(int Id, int IdClass, int IdSubClass, int IdSize, String Pose, Boolean Truncated, Boolean Difficult, Boolean Occluded) {
        DetectionEntity detectionEntity = new DetectionEntity();
        detectionEntity.Id = Id;
        detectionEntity.IdClass = IdClass;
        detectionEntity.IdSubClass = IdSubClass;
        detectionEntity.IdSize = IdSize;
        detectionEntity.Pose = Pose;
        detectionEntity.Truncated = Truncated;
        detectionEntity.Difficult = Difficult;
        detectionEntity.Occluded = Occluded;
        return detectionEntity;
    }

    public void setObjectDetection(int Xmin, int Ymin, int Xmax, int Ymax) {
        this.Xmin = Xmin < 0 ? 0 : Xmin;
        this.Ymin = Ymin < 0 ? 0 : Ymin;
        this.Xmax = Xmax < 0 ? 0 : Xmax;
        this.Ymax = Ymax < 0 ? 0 : Ymax;
    }

    public void updateId(int id) {
        this.Id = id;
    }

    public static Comparator<DetectionEntity> getComparatorById() {
        return (Comparator<DetectionEntity>) (DetectionEntity input, DetectionEntity output)
                -> String.valueOf(input.getId()).compareTo(String.valueOf(output.getId()));
    }

    public static DetectionEntity createSubDetection(int Id, int Xmin, int Ymin, int Xmax, int Ymax) {
        DetectionEntity subDetectionEntity = new DetectionEntity();
        subDetectionEntity.Id = Id;
        subDetectionEntity.Xmin = Xmin < 0 ? 0 : Xmin;
        subDetectionEntity.Ymin = Ymin < 0 ? 0 : Ymin;
        subDetectionEntity.Xmax = Xmax < 0 ? 0 : Xmax;
        subDetectionEntity.Ymax = Ymax < 0 ? 0 : Ymax;
        return subDetectionEntity;
    }
}
