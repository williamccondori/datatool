package Presentation;

import Business.ImportLogic;
import Entity.WorkspaceEntity;
import Helpers.MessageHelper;
import Helpers.StringHelper;
import Helpers.SystemHelper;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

public class ImportWorkspaceFrame extends javax.swing.JFrame {

    public ImportWorkspaceFrame() {
        initComponents();
        loadWindow();
    }

    public static void main(String args[]) {
        new StartFrame().setVisible(Boolean.TRUE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgImport = new javax.swing.JDialog();
        lblLog = new javax.swing.JLabel();
        scrLog = new javax.swing.JScrollPane();
        txtLog = new javax.swing.JTextArea();
        proLog = new javax.swing.JProgressBar();
        btnAccept = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        lblPath = new javax.swing.JLabel();
        txtPath = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        lblAuthor = new javax.swing.JLabel();
        txtAuthor = new javax.swing.JTextField();
        lblPath1 = new javax.swing.JLabel();
        txtDestinationPath = new javax.swing.JTextField();
        btnCancel = new javax.swing.JButton();
        btnImport = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnSearchDestination = new javax.swing.JButton();

        lblLog.setText("jLabel1");

        txtLog.setBackground(new java.awt.Color(51, 51, 51));
        txtLog.setColumns(20);
        txtLog.setFont(new java.awt.Font("Consolas", 0, 13)); // NOI18N
        txtLog.setForeground(new java.awt.Color(255, 255, 255));
        txtLog.setRows(5);
        txtLog.setText("s");
        scrLog.setViewportView(txtLog);

        btnAccept.setBackground(new java.awt.Color(255, 255, 255));
        btnAccept.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnAccept.setForeground(new java.awt.Color(0, 102, 153));
        btnAccept.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-de-acuerdo-16.png"))); // NOI18N
        btnAccept.setText("Accept");
        btnAccept.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnAccept.setFocusPainted(false);

        javax.swing.GroupLayout dlgImportLayout = new javax.swing.GroupLayout(dlgImport.getContentPane());
        dlgImport.getContentPane().setLayout(dlgImportLayout);
        dlgImportLayout.setHorizontalGroup(
            dlgImportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgImportLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgImportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrLog, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(lblLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(proLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgImportLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dlgImportLayout.setVerticalGroup(
            dlgImportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgImportLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblLog)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(proLog, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrLog, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Import Workspace");

        lblTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTitle.setForeground(new java.awt.Color(0, 102, 153));
        lblTitle.setText("IMPORT WORKSPACE");

        lblPath.setText("Path:");

        txtPath.setText("jTextField1");

        jLabel1.setText("<html>      <b>Workspaces accepted:</b>      <br>      <h5>VOC Dataset</h5>     <ul><li><span>JPEGImages</span></li><li><span>Annotations</span></li></ul>     <h5>YOLO Dataset</h5>     <ul><li><span>Images</span></li></ul> </html>");

        lblName.setText("Name:");

        txtName.setText("jTextField1");

        lblAuthor.setText("Author:");

        txtAuthor.setText("jTextField2");

        lblPath1.setText("Destination path:");

        txtDestinationPath.setText("jTextField1");

        btnCancel.setBackground(new java.awt.Color(255, 255, 255));
        btnCancel.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(0, 102, 153));
        btnCancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-cancelar-16.png"))); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnCancel.setFocusPainted(false);

        btnImport.setBackground(new java.awt.Color(255, 255, 255));
        btnImport.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnImport.setForeground(new java.awt.Color(0, 102, 153));
        btnImport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-de-acuerdo-16.png"))); // NOI18N
        btnImport.setText("Import");
        btnImport.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnImport.setFocusPainted(false);

        btnSearch.setBackground(new java.awt.Color(255, 255, 255));
        btnSearch.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(0, 102, 153));
        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-búsqueda-16.png"))); // NOI18N
        btnSearch.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSearch.setFocusPainted(false);

        btnSearchDestination.setBackground(new java.awt.Color(255, 255, 255));
        btnSearchDestination.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSearchDestination.setForeground(new java.awt.Color(0, 102, 153));
        btnSearchDestination.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-búsqueda-16.png"))); // NOI18N
        btnSearchDestination.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSearchDestination.setFocusPainted(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPath, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(jLabel1)
                    .addComponent(txtName)
                    .addComponent(txtAuthor)
                    .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblAuthor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPath1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(txtPath)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnImport, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(txtDestinationPath)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearchDestination, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPath)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(txtPath))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblAuthor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAuthor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPath1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSearchDestination, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(txtDestinationPath))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnImport, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnAccept;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnImport;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnSearchDestination;
    private javax.swing.JDialog dlgImport;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblAuthor;
    public static javax.swing.JLabel lblLog;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPath;
    private javax.swing.JLabel lblPath1;
    private javax.swing.JLabel lblTitle;
    public static javax.swing.JProgressBar proLog;
    private javax.swing.JScrollPane scrLog;
    private javax.swing.JTextField txtAuthor;
    private javax.swing.JTextField txtDestinationPath;
    public static javax.swing.JTextArea txtLog;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPath;
    // End of variables declaration//GEN-END:variables

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA EL FUNCIONAMIENTO DE LOS CONTROLES DE LA VENTANA.
     */
    private void loadControl() {
        this.setSize(new Dimension(400, 500));
        this.setLocationRelativeTo(null);
        this.btnSearch.addActionListener(this::search);
        this.btnSearchDestination.addActionListener(this::searchDestination);
        this.btnImport.addActionListener(this::inport);
        this.btnCancel.addActionListener(this::cancel);
        ImportWorkspaceFrame.btnAccept.addActionListener(this::accept);
        this.txtAuthor.setEditable(Boolean.FALSE);
        this.txtPath.setEditable(Boolean.FALSE);
        this.txtDestinationPath.setEditable(Boolean.FALSE);
        this.txtName.setText(StringHelper.EMPTY);
        this.txtAuthor.setText(SystemHelper.getUserName().toUpperCase());
        this.txtPath.setText(StringHelper.EMPTY);
        this.txtDestinationPath.setText(StringHelper.EMPTY);
        try {
            ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Icons/logo_datatool.png"));
            this.setIconImage(imageIcon.getImage());
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UN CUADRO DE DIALOGO PARA OBTENER UNA RUTA.
     *
     * @param e EVENTO.
     */
    private void search(ActionEvent e) {
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File(SystemHelper.getUserHome()));
            fileChooser.setDialogTitle("Choose a direcory");
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                String wokspaceFolderPath = fileChooser.getSelectedFile().getPath();
                this.txtPath.setText(wokspaceFolderPath);
            } else {
                this.txtPath.setText(StringHelper.EMPTY);
            }
        } catch (HeadlessException exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UN CUADRO DE DIALOGO PARA OBTENER UNA RUTA.
     *
     * @param e EVENTO.
     */
    private void searchDestination(ActionEvent e) {
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File(SystemHelper.getUserHome()));
            fileChooser.setDialogTitle("Choose a direcory");
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                String wokspaceFolderPath = fileChooser.getSelectedFile().getPath();
                this.txtDestinationPath.setText(wokspaceFolderPath);
            } else {
                this.txtDestinationPath.setText(StringHelper.EMPTY);
            }
        } catch (HeadlessException exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * INICIA CON EL PROCESO DE IMPORTACION DEL ESPACIO DE TRABAJO.
     *
     * @param e EVENTO.
     */
    private void inport(ActionEvent e) {
        try {
            String datasetPath = txtPath.getText();
            if (!StringHelper.isNullOrEmpty(datasetPath)) {
                showImportWindow();
                WorkspaceEntity workspaceEntity = WorkspaceEntity.create(this.txtName.getText(),
                        this.txtAuthor.getText(), this.txtDestinationPath.getText());
                this.txtDestinationPath.setText(workspaceEntity.getPath());
                ImportLogic importLogic = new ImportLogic(datasetPath, workspaceEntity);
                importLogic.start();
            }
        } catch (Exception exception) {
            this.dlgImport.dispose();
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * REGRESA A LA VENTANA PRINCIPAL.
     *
     * @param e EVENTO.
     */
    private void cancel(ActionEvent e) {
        StartFrame startFrame = new StartFrame();
        startFrame.setVisible(true);
        this.dispose();
    }

    /**
     * CIERRA EL CUADRO DE DIALOGO DE LA IMPORTACION.
     *
     * @param e EVENTO.
     */
    private void accept(ActionEvent e) {
        try {
            ApplicationFrame applicationFrame = new ApplicationFrame();
            String workspaceFolderPath = this.txtDestinationPath.getText();
            applicationFrame.setWorkspacePath(workspaceFolderPath);
            applicationFrame.loadData(workspaceFolderPath);
            applicationFrame.setVisible(true);
            this.dlgImport.dispose();
            this.dispose();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA EL CUADRO DE DIALOGO DE LA IMPORTACION.
     */
    private void showImportWindow() {
        this.dlgImport.setVisible(Boolean.TRUE);
        this.dlgImport.setSize(new Dimension(400, 300));
        this.dlgImport.setAlwaysOnTop(Boolean.TRUE);
        this.dlgImport.setResizable(Boolean.FALSE);
        this.dlgImport.setLocationRelativeTo(null);
        this.dlgImport.setTitle("Import workspace");
        try {
            ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Icons/icons8-tarea-del-sistema-100.png"));
            this.dlgImport.setIconImage(imageIcon.getImage());
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }
}
