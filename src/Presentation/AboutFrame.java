package Presentation;

import Helpers.SystemHelper;
import java.awt.Dimension;

public class AboutFrame extends javax.swing.JInternalFrame {

    public AboutFrame() {
        initComponents();
        loadWindow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlContainer = new javax.swing.JTabbedPane();
        pnlAbout = new javax.swing.JPanel();
        lblSubtitle = new javax.swing.JLabel();
        lblIcon = new javax.swing.JLabel();
        lblVersion = new javax.swing.JLabel();
        lblCompany = new javax.swing.JLabel();
        sepSeparator = new javax.swing.JSeparator();
        lblProductVersion = new javax.swing.JLabel();
        lblJavaRuntime = new javax.swing.JLabel();
        lblJavaHome = new javax.swing.JLabel();
        lblJavaVirtualMachine = new javax.swing.JLabel();
        lblOperatingSystem = new javax.swing.JLabel();
        lblOperatingSystemVersion = new javax.swing.JLabel();
        pblAuthors = new javax.swing.JPanel();
        lblSubtitle1 = new javax.swing.JLabel();
        lblWilliam = new javax.swing.JLabel();
        lblWilder = new javax.swing.JLabel();
        lblEveling = new javax.swing.JLabel();
        lblHenry = new javax.swing.JLabel();
        lblBragean = new javax.swing.JLabel();
        lblSubtitle2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        setClosable(true);
        setIconifiable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-acerca-de-16.png"))); // NOI18N

        pnlContainer.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);
        pnlContainer.setFocusable(false);

        pnlAbout.setBackground(new java.awt.Color(255, 255, 255));

        lblSubtitle.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblSubtitle.setForeground(new java.awt.Color(0, 102, 153));
        lblSubtitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSubtitle.setText("DATA SET ANNOTATION TOOL");

        lblIcon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/logo_large.png"))); // NOI18N

        lblVersion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblVersion.setForeground(new java.awt.Color(255, 204, 0));
        lblVersion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblVersion.setText("VERSION 1.0");

        lblCompany.setText("[]");

        lblProductVersion.setText("[]");

        lblJavaRuntime.setText("[]");

        lblJavaHome.setText("[]");

        lblJavaVirtualMachine.setText("[]");

        lblOperatingSystem.setText("[]");

        lblOperatingSystemVersion.setText("[]");

        javax.swing.GroupLayout pnlAboutLayout = new javax.swing.GroupLayout(pnlAbout);
        pnlAbout.setLayout(pnlAboutLayout);
        pnlAboutLayout.setHorizontalGroup(
            pnlAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAboutLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblIcon, javax.swing.GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
                    .addComponent(lblSubtitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblVersion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sepSeparator)
                    .addComponent(lblCompany, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblProductVersion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblJavaRuntime, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblJavaHome, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblJavaVirtualMachine, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblOperatingSystem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblOperatingSystemVersion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlAboutLayout.setVerticalGroup(
            pnlAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAboutLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(lblIcon)
                .addGap(18, 18, 18)
                .addComponent(lblSubtitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblVersion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sepSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCompany)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblProductVersion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblJavaRuntime)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblJavaHome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblJavaVirtualMachine)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblOperatingSystem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblOperatingSystemVersion)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        pnlContainer.addTab("About", pnlAbout);

        pblAuthors.setBackground(new java.awt.Color(255, 255, 255));

        lblSubtitle1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblSubtitle1.setForeground(new java.awt.Color(0, 102, 153));
        lblSubtitle1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSubtitle1.setText("AUTHORS");

        lblWilliam.setText("[]");

        lblWilder.setText("[]");

        lblEveling.setText("[]");

        lblHenry.setText("[]");

        lblBragean.setText("[]");

        lblSubtitle2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblSubtitle2.setForeground(new java.awt.Color(0, 102, 153));
        lblSubtitle2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSubtitle2.setText("ACKNOWLEDGMENT");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("<html>This research was supported from Universidad Nacional de San Agustín de Arequipa <br>Contract: IBA-0032-2017-UNSA, like part of the project \"Detection of industrial fishing vessels within 5 miles of the Arequipa Region using high performance computing and satellite images''. <br>Thanks to the CiTeSoft Contract: EC-0003-2017-UNSA for the equipment and  the resources bring to the project.</html>");

        javax.swing.GroupLayout pblAuthorsLayout = new javax.swing.GroupLayout(pblAuthors);
        pblAuthors.setLayout(pblAuthorsLayout);
        pblAuthorsLayout.setHorizontalGroup(
            pblAuthorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pblAuthorsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pblAuthorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(lblSubtitle1, javax.swing.GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
                    .addComponent(lblWilliam, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblWilder, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblEveling, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblHenry, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblBragean, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblSubtitle2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        pblAuthorsLayout.setVerticalGroup(
            pblAuthorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pblAuthorsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSubtitle1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblWilliam)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblWilder)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblEveling)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblHenry)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblBragean)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSubtitle2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(190, Short.MAX_VALUE))
        );

        pnlContainer.addTab("Authors", pblAuthors);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlContainer)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlContainer)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblBragean;
    private javax.swing.JLabel lblCompany;
    private javax.swing.JLabel lblEveling;
    private javax.swing.JLabel lblHenry;
    private javax.swing.JLabel lblIcon;
    private javax.swing.JLabel lblJavaHome;
    private javax.swing.JLabel lblJavaRuntime;
    private javax.swing.JLabel lblJavaVirtualMachine;
    private javax.swing.JLabel lblOperatingSystem;
    private javax.swing.JLabel lblOperatingSystemVersion;
    private javax.swing.JLabel lblProductVersion;
    private javax.swing.JLabel lblSubtitle;
    private javax.swing.JLabel lblSubtitle1;
    private javax.swing.JLabel lblSubtitle2;
    private javax.swing.JLabel lblVersion;
    private javax.swing.JLabel lblWilder;
    private javax.swing.JLabel lblWilliam;
    private javax.swing.JPanel pblAuthors;
    private javax.swing.JPanel pnlAbout;
    private javax.swing.JTabbedPane pnlContainer;
    private javax.swing.JSeparator sepSeparator;
    // End of variables declaration//GEN-END:variables

    private void loadWindow() {
        this.setSize(new Dimension(500, 500));
        this.setResizable(Boolean.FALSE);
        this.setTitle("About");
        
        this.lblCompany.setText(String.format("Research Center: %s", "CiTeSoft"));
        this.lblProductVersion.setText(String.format("Product Version: %s", "1.0"));
        this.lblJavaRuntime.setText(String.format("Java: %s", SystemHelper.getJavaRuntime()));
        this.lblJavaHome.setText(String.format("Java Home: %s", SystemHelper.getJavaHome()));
        this.lblJavaVirtualMachine.setText(String.format("Java Virtual Machine: %s", SystemHelper.getJavaVirtualMachine()));
        this.lblOperatingSystem.setText(String.format("Operating System: %s", SystemHelper.getOperatingSystem()));
        this.lblOperatingSystemVersion.setText(String.format("Operating System Version: %s", SystemHelper.getOperatingSystemVersion()));

        this.lblWilliam.setText("William Condori Quispe");
        this.lblWilder.setText("Wilder Nina Choquehuayta");
        this.lblEveling.setText("Eveling Gloria Castro Gutierrez");
        this.lblHenry.setText("Henry Aurelio Aymara Apaza");
        this.lblBragean.setText("Bragean Luis Vargas Marquez");
    }
}
