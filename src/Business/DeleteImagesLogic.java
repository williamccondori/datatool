package Business;

import Entity.ImageEntity;
import Entity.WorkspaceEntity;
import Helpers.StringHelper;
import Presentation.ApplicationFrame;
import Presentation.WorkspaceSettingsFrame;
import java.io.File;
import java.util.ArrayList;

public class DeleteImagesLogic extends Thread {

    @Override
    public void run() {
        try {
            startWindow();
            WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
            WorkspaceEntity workspaceEntity = workspaceLogic.get();
            ArrayList<ImageEntity> images = workspaceEntity.getImages();
            WorkspaceSettingsFrame.proLog.setMaximum(images.size());

            for (int i = 0; i < images.size(); i++) {
                File image = new File(String.format("%s/%s", workspaceEntity.getImagePath(), images.get(i).getImageName()));
                if (image.delete()) {
                    WorkspaceSettingsFrame.txtLog.append(String.format("Deleting image: %s\n", images.get(i).getImageName()));
                } else {
                    throw new Exception(String.format("Error deleting image: %s", images.get(i).getImageName()));
                }

                File annotation = new File(String.format("%s/%s", workspaceEntity.getAnnotationPath(), images.get(i).getAnnotationName()));
                if (annotation.delete()) {
                    WorkspaceSettingsFrame.txtLog.append(String.format("Deleting annotation: %s\n", images.get(i).getAnnotationName()));
                } else {
                    throw new Exception(String.format("Error deleting annotation: %s", images.get(i).getAnnotationName()));
                }

                WorkspaceSettingsFrame.proLog.setValue(i + 1);
                WorkspaceSettingsFrame.txtLog.setCaretPosition(WorkspaceSettingsFrame.txtLog.getText().length());
            }

            workspaceLogic.updateImages(new ArrayList<>());
            finishWindow();
        } catch (Exception exception) {
            errorWindow(exception);
        }
    }

    private void startWindow() {
        WorkspaceSettingsFrame.lblLog.setText("Deleting images, this may take a few minutes...");
        WorkspaceSettingsFrame.txtLog.setText(StringHelper.EMPTY);
        WorkspaceSettingsFrame.txtLog.setEditable(Boolean.FALSE);
        WorkspaceSettingsFrame.btnAccept.setEnabled(Boolean.FALSE);
    }

    private void finishWindow() {
        WorkspaceSettingsFrame.lblLog.setText("Completed!");
        WorkspaceSettingsFrame.txtLog.append("Completed!");
        WorkspaceSettingsFrame.btnAccept.setEnabled(Boolean.TRUE);
    }

    private void errorWindow(Exception exception) {
        WorkspaceSettingsFrame.lblLog.setText("Error!");
        WorkspaceSettingsFrame.txtLog.append(String.format("An error has occurred: %s\n", exception.getLocalizedMessage()));
        WorkspaceSettingsFrame.btnAccept.setEnabled(Boolean.TRUE);
    }
}
