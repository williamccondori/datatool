package Entity;

import java.util.ArrayList;
import Helpers.ApplicationSettings;
import Helpers.StringHelper;

public class AnnotationEntity {

    private int Id;
    private int IdPlace;
    private String ImageName;
    private String AnnotationName;
    private String FileNameInput;
    private String FileNameOutput;
    private String FileType;
    private String FileTypeLarge;
    private String FileTypeMime;
    private String Extension;
    private String FileSize;
    private int Width;
    private int Height;
    private int Depth;
    private String Description;
    private ArrayList<ClassificationEntity> Classifications;
    private ArrayList<DetectionEntity> Detections;
    private ArrayList<DetectionEntity> SubDetections;

    public int getId() {
        return Id;
    }

    public int getIdPlace() {
        return IdPlace;
    }

    public String getImageName() {
        return ImageName;
    }

    public String getAnnotationName() {
        return AnnotationName;
    }

    public String getFileNameInput() {
        return FileNameInput;
    }

    public String getFileNameOutput() {
        return FileNameOutput;
    }

    public String getFileType() {
        return FileType;
    }

    public String getFileTypeLarge() {
        return FileTypeLarge;
    }

    public String getFileTypeMime() {
        return FileTypeMime;
    }

    public String getExtension() {
        return Extension;
    }

    public String getFileSize() {
        return FileSize;
    }

    public int getWidth() {
        return Width;
    }

    public int getHeight() {
        return Height;
    }

    public int getDepth() {
        return Depth;
    }

    public String getDescription() {
        return Description;
    }

    public ArrayList<ClassificationEntity> getClassifications() {
        return Classifications;
    }

    public ArrayList<DetectionEntity> getDetections() {
        return Detections == null ? new ArrayList<>() : Detections;
    }

    public ArrayList<DetectionEntity> getSubDetections() {
        return SubDetections == null ? new ArrayList<>() : SubDetections;
    }

    public static AnnotationEntity generate(int id, String imageName, String fileNameInput,
            String fileNameOutput) {
        AnnotationEntity annotationEntity = new AnnotationEntity();
        annotationEntity.Id = id;
        annotationEntity.IdPlace = 0;
        annotationEntity.ImageName = imageName;
        annotationEntity.AnnotationName = String.format("%s.%s", id, ApplicationSettings.APP_FILE_TYPE);
        annotationEntity.FileNameInput = fileNameInput;
        annotationEntity.FileNameOutput = fileNameOutput;
        annotationEntity.Description = StringHelper.EMPTY;
        annotationEntity.Classifications = new ArrayList<>();
        annotationEntity.Detections = new ArrayList<>();
        annotationEntity.SubDetections = new ArrayList<>();
        return annotationEntity;
    }

    public void setProperties(String fileType, String fileTypeLarge, String fileTypeMime,
            String extension, String fileSize, int width, int height, int depth) {
        this.FileType = fileType;
        this.FileTypeLarge = fileTypeLarge;
        this.FileTypeMime = fileTypeMime;
        this.Extension = extension;
        this.FileSize = fileSize;
        this.Width = width;
        this.Height = height;
        this.Depth = depth;
    }

    public static AnnotationEntity setCustomProperties(int idPlace, String description) {
        AnnotationEntity annotationEntity = new AnnotationEntity();
        annotationEntity.IdPlace = idPlace;
        annotationEntity.Description = description;
        return annotationEntity;
    }

    public void update(int idPlace, String description) {
        this.IdPlace = idPlace;
        this.Description = description;
    }

    public void updateDetections(ArrayList<DetectionEntity> detections) {
        this.Detections = detections;
    }

    public void updateSubDetections(ArrayList<DetectionEntity> subDetections) {
        this.SubDetections = subDetections;
    }
}
