package Entity;

import Helpers.ApplicationSettings;

public class YoloObjectEntity {

    private int IdClass;
    private float X;
    private float Y;
    private float W;
    private float H;

    public int getIdClass() {
        return IdClass;
    }

    public float getX() {
        return X;
    }

    public float getY() {
        return Y;
    }

    public float getW() {
        return W;
    }

    public float getH() {
        return H;
    }

    public YoloObjectEntity() {
    }

    public YoloObjectEntity(int IdClass, float X, float Y, float W, float H) {
        this.IdClass = IdClass;
        this.X = X;
        this.Y = Y;
        this.W = W;
        this.H = H;
    }
    
    public static YoloObjectEntity toYolo(int idClass, int xMin, int yMin, int xMax, int yMax, int widthImage, int heightImage) {
        float x = ((float) (xMax + xMin) / 2f) / (float) widthImage;
        float y = ((float) (yMax + yMin) / 2f) / (float) heightImage;
        float w = ((float) (xMax - xMin)) / (float) widthImage;
        float h = ((float) (yMax - yMin)) / (float) heightImage;
        int classs = idClass - ApplicationSettings.PREFIX_ID;
        return new YoloObjectEntity(classs, x, y, w, h);
    }
}
