package Entity;

import Helpers.ApplicationSettings;

public class ImageEntity {

    private int Id;
    private String ImageName;
    private String AnnotationName;
    private int IdState;

    public int getId() {
        return Id;
    }

    public String getImageName() {
        return ImageName;
    }

    public String getAnnotationName() {
        return AnnotationName;
    }

    public int getIdState() {
        return IdState;
    }

    public static ImageEntity register(int idImage, String imageFileExtension) {
        ImageEntity imageEntity = new ImageEntity();
        imageEntity.Id = idImage;
        imageEntity.ImageName = String.format("%s.%s", idImage, imageFileExtension);
        imageEntity.AnnotationName = String.format("%s.%s", idImage, ApplicationSettings.APP_FILE_TYPE);
        imageEntity.IdState = ApplicationSettings.PREFIX_ID;
        return imageEntity;
    }

    public static ImageEntity updateIdState(int idState) {
        ImageEntity imageEntity = new ImageEntity();
        imageEntity.IdState = idState;
        return imageEntity;
    }

    public void update(int idState) {
        this.IdState = idState;
    }
}
