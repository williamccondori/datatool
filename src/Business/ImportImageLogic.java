package Business;

import Entity.ImageEntity;
import Entity.ImportImageEntity;
import Entity.WorkspaceEntity;
import Helpers.FileHelper;
import Helpers.StringHelper;
import Presentation.ApplicationFrame;
import Presentation.ImageManagerFrame;
import java.io.File;
import java.util.ArrayList;

public class ImportImageLogic extends Thread {

    private final String ImageInputPath;
    private final Boolean IsDirectory;

    public ImportImageLogic(String ImageInputPath, Boolean IsDirectory) {
        this.ImageInputPath = ImageInputPath;
        this.IsDirectory = IsDirectory;
    }

    @Override
    public void run() {
        try {
            startWindow();
            ArrayList<ImportImageEntity> importImages = ImportImageEntity.getImportImages(this.IsDirectory, this.ImageInputPath);
            ImageManagerFrame.proLog.setMaximum(importImages.size());

            WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
            WorkspaceEntity workspaceEntity = workspaceLogic.get();
            int lastImageId = workspaceLogic.getLastImageId();
            AnnotationLogic annotationLogic = new AnnotationLogic(workspaceEntity.getAnnotationPath());
            
            printLog(String.format("Number of images: %s", importImages.size()));
            
            for (int i = 0; i < importImages.size(); i++) {
                int idImage = lastImageId + i;

                // PROCESO DE COPIADO DE LAS IMAGENES.
                ImportImageEntity importImageEntity = importImages.get(i);
                File imageInput = new File(importImageEntity.getPath());
                File imageOutput = new File(String.format("%s/%s.%s", workspaceEntity.getImagePath(), idImage, importImageEntity.getExtension()));
                FileHelper.copyFile(imageInput, imageOutput);
                printLog(String.format("Image input: %s", imageInput.getName()));
                printLog(String.format("Image output: %s", imageOutput.getName()));

                // PROCESO DE GENERACION DE LAS ANOTACIONES.
                annotationLogic.generate(idImage, imageOutput, imageInput.getName());

                printLog(String.format("Generating annotation: %s", idImage));

                // REGISTRA LA IMAGEN IMPORTADA EN EL ARCHIVO PRINCIPAL.
                ImageEntity imageEntity = ImageEntity.register(idImage, importImageEntity.getExtension());
                workspaceLogic.registerImage(imageEntity);

                // ACTUALIZA EL VALOR DE LA BARRA DE PROGRESO.
                ImageManagerFrame.proLog.setValue(i + 1);
            }
            finishWindow();
        } catch (Exception exception) {
            errorWindow(exception);
        }
    }

    private void startWindow() {
        ImageManagerFrame.lblLog.setText("Importing images, this may take a few minutes...");
        ImageManagerFrame.txtLog.setText(StringHelper.EMPTY);
        ImageManagerFrame.txtLog.setEditable(Boolean.FALSE);
        ImageManagerFrame.proLog.setMaximum(0);
        ImageManagerFrame.proLog.setValue(0);
        ImageManagerFrame.btnAccept.setEnabled(Boolean.FALSE);
    }

    private void finishWindow() {
        ImageManagerFrame.lblLog.setText("Completed!");
        ImageManagerFrame.proLog.setValue(ImageManagerFrame.proLog.getMaximum());
        printLog("Completed!");
        ImageManagerFrame.btnAccept.setEnabled(Boolean.TRUE);
    }

    private void errorWindow(Exception exception) {
        ImageManagerFrame.lblLog.setText("Error!");
        ImageManagerFrame.proLog.setMaximum(0);
        ImageManagerFrame.proLog.setValue(0);
        printLog(String.format("An error has occurred: %s", exception.getLocalizedMessage()));
        ImageManagerFrame.btnAccept.setEnabled(Boolean.TRUE);
    }

    private void printLog(String message) {
        ImageManagerFrame.txtLog.append(message + "\n");
        ImageManagerFrame.txtLog.setCaretPosition(ImageManagerFrame.txtLog.getText().length());
    }
}
