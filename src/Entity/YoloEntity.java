package Entity;

import java.util.ArrayList;

public class YoloEntity {

    public static final String IMAGES = "Images";
    
    private ArrayList<YoloObjectEntity> Objects;

    public ArrayList<YoloObjectEntity> getObjects() {
        return Objects;
    }

    public YoloEntity() {
    }

    public YoloEntity(ArrayList<YoloObjectEntity> Objects) {
        this.Objects = Objects;
    }
}
