package Business;

import Entity.VocEntity;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class ExportVocLogic {

    private final String ANNOTATION = "annotation";
    private final String VERIFIED = "verified";
    private final String YES = "yes";

    private final String AnnotationFolder;

    public ExportVocLogic(String AnnotationFolder) {
        this.AnnotationFolder = AnnotationFolder;
    }

    public void GenerateFile(VocEntity vocEntity, String id) throws IOException {
        Document document = new Document();
        Element root = new Element(this.ANNOTATION);
        root.setAttribute(this.VERIFIED, this.YES);

        Element folder = new Element("folder");
        folder.addContent(vocEntity.getFolder());

        Element filename = new Element("filename");
        filename.addContent(vocEntity.getFileName());

        Element path = new Element("path");
        path.addContent(vocEntity.getPath());

        Element source = new Element("source");

        Element database = new Element("database");
        database.addContent(vocEntity.getSource().getDatabase());

        source.addContent(database);

        Element size = new Element("size");

        Element width = new Element("width");
        width.addContent(vocEntity.getSize().getWidth());
        Element height = new Element("height");
        height.addContent(vocEntity.getSize().getHeight());
        Element depth = new Element("depth");
        depth.addContent(vocEntity.getSize().getDepth());

        size.addContent(width);
        size.addContent(height);
        size.addContent(depth);

        Element segmented = new Element("segmented");
        segmented.addContent(vocEntity.getSegmented());

        root.addContent(folder);
        root.addContent(filename);
        root.addContent(path);
        root.addContent(source);
        root.addContent(size);
        root.addContent(segmented);

        vocEntity.getObjects().forEach(p -> {

            Element object = new Element("object");

            Element name = new Element("name");
            name.addContent(p.getName());

            Element pose = new Element("pose");
            pose.addContent(p.getPose());

            Element truncated = new Element("truncated");
            truncated.addContent(String.valueOf(p.getTruncated()));

            Element difficult = new Element("difficult");
            difficult.addContent(String.valueOf(p.getDificult()));

            Element occluded = new Element("occluded");
            occluded.addContent(String.valueOf(p.getOccluded()));

            Element boundingBox = new Element("bndbox");

            Element xmin = new Element("xmin");
            xmin.addContent(String.valueOf(p.getXmin()));

            Element ymin = new Element("ymin");
            ymin.addContent(String.valueOf(p.getYmin()));

            Element xmax = new Element("xmax");
            xmax.addContent(String.valueOf(p.getXmax()));

            Element ymax = new Element("ymax");
            ymax.addContent(String.valueOf(p.getYmax()));

            boundingBox.addContent(xmin);
            boundingBox.addContent(xmax);
            boundingBox.addContent(ymin);
            boundingBox.addContent(ymax);

            object.addContent(name);
            object.addContent(pose);
            object.addContent(truncated);
            object.addContent(difficult);
            object.addContent(occluded);
            object.addContent(boundingBox);

            root.addContent(object);
        });

        String filePath = String.format("%s/%s.xml", this.AnnotationFolder, id);

        document.setRootElement(root);
        XMLOutputter xMLOutputter = new XMLOutputter();
        xMLOutputter.setFormat(Format.getPrettyFormat());
        try (FileWriter fileWriter = new FileWriter(new File(filePath))) {
            xMLOutputter.output(document, fileWriter);
            fileWriter.close();
        }
    }
}
