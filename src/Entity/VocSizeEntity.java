package Entity;

public class VocSizeEntity {

    private String Width;
    private String Height;
    private String Depth;

    public String getWidth() {
        return Width;
    }

    public String getHeight() {
        return Height;
    }

    public String getDepth() {
        return Depth;
    }

    public VocSizeEntity() {
    }

    public VocSizeEntity(String Width, String Height, String Depth) {
        this.Width = Width;
        this.Height = Height;
        this.Depth = Depth;
    }
}
