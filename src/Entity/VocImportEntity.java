/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.ArrayList;

/**
 *
 * @author William
 */
public class VocImportEntity {
    private ArrayList<DetectionEntity> Detections; 
    private ArrayList<ClassEntity> Classes;

    public ArrayList<DetectionEntity> getDetections() {
        return Detections;
    }

    public void setDetections(ArrayList<DetectionEntity> Detections) {
        this.Detections = Detections;
    }

    public ArrayList<ClassEntity> getClasses() {
        return Classes;
    }

    public void setClasses(ArrayList<ClassEntity> Classes) {
        this.Classes = Classes;
    }

    public static VocImportEntity create(ArrayList<DetectionEntity> detections, ArrayList<ClassEntity> classes) {
        VocImportEntity vocImportEntity =  new VocImportEntity();
        vocImportEntity.setDetections(detections);
        vocImportEntity.setClasses(classes);
        return vocImportEntity;
    }
}
