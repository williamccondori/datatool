package Presentation;

import Helpers.MessageHelper;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class StartFrame extends javax.swing.JFrame {

    public StartFrame() {
        initComponents();
        loadWindow();
    }

    public static void main(String args[]) {
        new StartFrame().setVisible(Boolean.TRUE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlContainer = new javax.swing.JPanel();
        lblSubtitle = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnCreateWorkspace = new javax.swing.JButton();
        btnOpenWorkspace = new javax.swing.JButton();
        btnImportWorkspace = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Data Set Annotation Tool");

        pnlContainer.setBackground(new java.awt.Color(255, 255, 255));

        lblSubtitle.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblSubtitle.setForeground(new java.awt.Color(0, 102, 153));
        lblSubtitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSubtitle.setText("DATA SET ANNOTATION TOOL");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/logo.png"))); // NOI18N

        btnCreateWorkspace.setBackground(new java.awt.Color(255, 255, 255));
        btnCreateWorkspace.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnCreateWorkspace.setForeground(new java.awt.Color(0, 102, 153));
        btnCreateWorkspace.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-agregar-base-de-datos-16.png"))); // NOI18N
        btnCreateWorkspace.setText("Create Workspace");
        btnCreateWorkspace.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnCreateWorkspace.setFocusPainted(false);

        btnOpenWorkspace.setBackground(new java.awt.Color(255, 255, 255));
        btnOpenWorkspace.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnOpenWorkspace.setForeground(new java.awt.Color(0, 102, 153));
        btnOpenWorkspace.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-abrir-carpeta-16.png"))); // NOI18N
        btnOpenWorkspace.setText("Open Workspace");
        btnOpenWorkspace.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnOpenWorkspace.setFocusPainted(false);

        btnImportWorkspace.setBackground(new java.awt.Color(255, 255, 255));
        btnImportWorkspace.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnImportWorkspace.setForeground(new java.awt.Color(0, 102, 153));
        btnImportWorkspace.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-reestablecer-las-bases-de-datos-16.png"))); // NOI18N
        btnImportWorkspace.setText("Import Workspace");
        btnImportWorkspace.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnImportWorkspace.setFocusPainted(false);

        javax.swing.GroupLayout pnlContainerLayout = new javax.swing.GroupLayout(pnlContainer);
        pnlContainer.setLayout(pnlContainerLayout);
        pnlContainerLayout.setHorizontalGroup(
            pnlContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlContainerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblSubtitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlContainerLayout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addGroup(pnlContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnCreateWorkspace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnOpenWorkspace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnImportWorkspace, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 115, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlContainerLayout.setVerticalGroup(
            pnlContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlContainerLayout.createSequentialGroup()
                .addGap(99, 99, 99)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSubtitle)
                .addGap(85, 85, 85)
                .addComponent(btnCreateWorkspace, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnImportWorkspace, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnOpenWorkspace, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(94, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreateWorkspace;
    private javax.swing.JButton btnImportWorkspace;
    private javax.swing.JButton btnOpenWorkspace;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblSubtitle;
    private javax.swing.JPanel pnlContainer;
    // End of variables declaration//GEN-END:variables

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA EL FUNCIONAMIENTO DE LOS CONTROLES DE LA VENTANA.
     */
    private void loadControl() {
        this.setSize(new Dimension(400, 500));
        this.setResizable(Boolean.FALSE);
        this.setLocationRelativeTo(null);
        this.btnCreateWorkspace.addActionListener(this::createWorkspace);
        this.btnOpenWorkspace.addActionListener(this::openWorkspace);
        this.btnImportWorkspace.addActionListener(this::importWorkspace);
        try {
            ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Icons/logo_datatool.png"));
            this.setIconImage(imageIcon.getImage());
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA LA VENTANA PARA CREAR UN NUEVO ESTACIO DE TRABAJO.
     *
     * @param e EVENTO.
     */
    private void createWorkspace(ActionEvent e) {
        CreateWorkspaceFrame createWorkspaceFrame = new CreateWorkspaceFrame();
        createWorkspaceFrame.setVisible(true);
        this.dispose();
    }

    /**
     * MUESTRA LA VENTANA PARA ABRIR UN ESPACIO DE TRABAJO.
     *
     * @param e EVENTO.
     */
    private void openWorkspace(ActionEvent e) {
        OpenWorkspaceFrame openWorkspaceFrame = new OpenWorkspaceFrame();
        openWorkspaceFrame.setVisible(true);
        this.dispose();
    }

    /**
     * MUESTRA LA VENTANA PARA IMPORTAR UN ESPACIO DE TRABAJO.
     *
     * @param e EVENTO.
     */
    private void importWorkspace(ActionEvent e) {
        ImportWorkspaceFrame importWorkspaceFrame = new ImportWorkspaceFrame();
        importWorkspaceFrame.setVisible(true);
        this.dispose();
    }
}
