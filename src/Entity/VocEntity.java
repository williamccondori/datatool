package Entity;

import java.util.ArrayList;

public class VocEntity {

    public static final String JPEG_IMAGES = "JPEGImages";
    public static final String ANNOTATIONS = "Annotations";

    private String Folder;
    private String FileName;
    private String Path;
    private VocSourceEntity Source;
    private VocSizeEntity Size;
    private String Segmented;
    private ArrayList<VocObjectEntity> Objects;

    public String getFolder() {
        return Folder;
    }

    public String getFileName() {
        return FileName;
    }

    public String getPath() {
        return Path;
    }

    public VocSourceEntity getSource() {
        return Source;
    }

    public VocSizeEntity getSize() {
        return Size;
    }

    public String getSegmented() {
        return Segmented;
    }

    public ArrayList<VocObjectEntity> getObjects() {
        return Objects;
    }

    public VocEntity() {
    }

    public VocEntity(String Folder, String FileName, String Path, VocSourceEntity Source, VocSizeEntity Size, String Segmented, ArrayList<VocObjectEntity> Objects) {
        this.Folder = Folder;
        this.FileName = FileName;
        this.Path = Path;
        this.Source = Source;
        this.Size = Size;
        this.Segmented = Segmented;
        this.Objects = Objects;
    }

}
