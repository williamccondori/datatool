package Presentation;

import Business.DeleteImagesLogic;
import Business.WorkspaceLogic;
import Entity.SettingsEntity;
import Entity.WorkspaceEntity;
import Helpers.ApplicationSettings;
import Helpers.ChartHelper;
import Helpers.FileTreeModelHelper;
import Helpers.MessageHelper;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.TreeModel;
import org.jfree.data.general.DefaultPieDataset;

public class WorkspaceSettingsFrame extends javax.swing.JInternalFrame {

    public WorkspaceSettingsFrame() {
        initComponents();
        loadWindow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgDelete = new javax.swing.JDialog();
        lblLog = new javax.swing.JLabel();
        scrLog = new javax.swing.JScrollPane();
        txtLog = new javax.swing.JTextArea();
        proLog = new javax.swing.JProgressBar();
        btnAccept = new javax.swing.JButton();
        pnlContainer = new javax.swing.JTabbedPane();
        pnlGeneral = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        lblAuthor = new javax.swing.JLabel();
        txtAuthor = new javax.swing.JTextField();
        lblPath = new javax.swing.JLabel();
        txtPath = new javax.swing.JTextField();
        lblImagePath = new javax.swing.JLabel();
        txtImagePath = new javax.swing.JTextField();
        lblAnnotationPath = new javax.swing.JLabel();
        txtAnnotationPath = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnDefault = new javax.swing.JButton();
        scrWorkspace = new javax.swing.JScrollPane();
        tblWorkspace = new javax.swing.JTable();
        lblResource = new javax.swing.JLabel();
        sepSeparator = new javax.swing.JSeparator();
        lblResource1 = new javax.swing.JLabel();
        pnlDataset = new javax.swing.JPanel();
        lblDistribution = new javax.swing.JLabel();
        lblTest = new javax.swing.JLabel();
        txtTest = new javax.swing.JTextField();
        lblTrain = new javax.swing.JLabel();
        txtTrain = new javax.swing.JTextField();
        lblValidation = new javax.swing.JLabel();
        txtValidation = new javax.swing.JTextField();
        lblPercentage01 = new javax.swing.JLabel();
        lblPercentage02 = new javax.swing.JLabel();
        lblPercentage03 = new javax.swing.JLabel();
        chkRandom = new javax.swing.JCheckBox();
        jSeparator1 = new javax.swing.JSeparator();
        btnDefaultDistribution = new javax.swing.JButton();
        btnSaveDistribution = new javax.swing.JButton();
        lblDistribution1 = new javax.swing.JLabel();
        btnDelete = new javax.swing.JButton();
        pnlChart = new javax.swing.JPanel();
        lblDeleteDataset = new javax.swing.JLabel();
        pnlStructure = new javax.swing.JPanel();
        lblDistribution2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        treWorkspaceStructure = new javax.swing.JTree();

        lblLog.setText("jLabel1");

        txtLog.setBackground(new java.awt.Color(51, 51, 51));
        txtLog.setColumns(20);
        txtLog.setFont(new java.awt.Font("Consolas", 0, 13)); // NOI18N
        txtLog.setForeground(new java.awt.Color(255, 255, 255));
        txtLog.setRows(5);
        txtLog.setText("s");
        scrLog.setViewportView(txtLog);

        btnAccept.setBackground(new java.awt.Color(0, 102, 153));
        btnAccept.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnAccept.setForeground(new java.awt.Color(255, 255, 255));
        btnAccept.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-de-acuerdo-16.png"))); // NOI18N
        btnAccept.setText("Accept");
        btnAccept.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnAccept.setFocusPainted(false);

        javax.swing.GroupLayout dlgDeleteLayout = new javax.swing.GroupLayout(dlgDelete.getContentPane());
        dlgDelete.getContentPane().setLayout(dlgDeleteLayout);
        dlgDeleteLayout.setHorizontalGroup(
            dlgDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgDeleteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrLog, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(lblLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(proLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgDeleteLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dlgDeleteLayout.setVerticalGroup(
            dlgDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgDeleteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblLog)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(proLog, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrLog, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        setClosable(true);
        setIconifiable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-configuración-de-datos-16.png"))); // NOI18N

        pnlContainer.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        pnlContainer.setFocusable(false);

        lblName.setText("Name:");

        txtName.setText("jTextField1");

        lblAuthor.setText("Author:");

        txtAuthor.setText("jTextField2");

        lblPath.setText("Path:");

        txtPath.setText("jTextField3");

        lblImagePath.setText("Image path:");

        txtImagePath.setText("jTextField4");

        lblAnnotationPath.setText("Annotation path:");

        txtAnnotationPath.setText("jTextField5");

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSave.setForeground(new java.awt.Color(0, 102, 153));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-guardar-16.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSave.setFocusPainted(false);

        btnDefault.setBackground(new java.awt.Color(255, 255, 255));
        btnDefault.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnDefault.setForeground(new java.awt.Color(0, 102, 153));
        btnDefault.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-datos-pendientes-16.png"))); // NOI18N
        btnDefault.setText("Default");
        btnDefault.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnDefault.setFocusPainted(false);

        tblWorkspace.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrWorkspace.setViewportView(tblWorkspace);

        lblResource.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblResource.setForeground(new java.awt.Color(0, 102, 153));
        lblResource.setText("RESOURCES");

        lblResource1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblResource1.setForeground(new java.awt.Color(0, 102, 153));
        lblResource1.setText("WORKSPACE");

        javax.swing.GroupLayout pnlGeneralLayout = new javax.swing.GroupLayout(pnlGeneral);
        pnlGeneral.setLayout(pnlGeneralLayout);
        pnlGeneralLayout.setHorizontalGroup(
            pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblResource1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPath)
                    .addGroup(pnlGeneralLayout.createSequentialGroup()
                        .addGroup(pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                            .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(7, 7, 7)
                        .addGroup(pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtAuthor)
                            .addComponent(lblAuthor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(txtImagePath)
                    .addComponent(lblImagePath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblAnnotationPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtAnnotationPath)
                    .addComponent(sepSeparator)
                    .addComponent(lblResource, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(scrWorkspace, javax.swing.GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlGeneralLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnDefault, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnlGeneralLayout.setVerticalGroup(
            pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblResource1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlGeneralLayout.createSequentialGroup()
                        .addComponent(lblName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlGeneralLayout.createSequentialGroup()
                        .addComponent(lblAuthor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAuthor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPath, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblImagePath)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtImagePath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblAnnotationPath)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAnnotationPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sepSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblResource)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrWorkspace, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 87, Short.MAX_VALUE)
                .addGroup(pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDefault, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pnlContainer.addTab("General", pnlGeneral);

        lblDistribution.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblDistribution.setForeground(new java.awt.Color(0, 102, 153));
        lblDistribution.setText("DISTRIBUTION");

        lblTest.setText("Test:");

        txtTest.setText("jTextField1");

        lblTrain.setText("Train:");

        txtTrain.setText("jTextField2");

        lblValidation.setText("Validation:");

        txtValidation.setText("jTextField3");

        lblPercentage01.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblPercentage01.setForeground(new java.awt.Color(0, 102, 153));
        lblPercentage01.setText("%");

        lblPercentage02.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblPercentage02.setForeground(new java.awt.Color(0, 102, 153));
        lblPercentage02.setText("%");

        lblPercentage03.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblPercentage03.setForeground(new java.awt.Color(0, 102, 153));
        lblPercentage03.setText("%");

        chkRandom.setBackground(new java.awt.Color(255, 255, 255));
        chkRandom.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        chkRandom.setForeground(new java.awt.Color(0, 102, 153));
        chkRandom.setText("Random");
        chkRandom.setFocusPainted(false);

        btnDefaultDistribution.setBackground(new java.awt.Color(255, 255, 255));
        btnDefaultDistribution.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnDefaultDistribution.setForeground(new java.awt.Color(0, 102, 153));
        btnDefaultDistribution.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-datos-pendientes-16.png"))); // NOI18N
        btnDefaultDistribution.setText("Default");
        btnDefaultDistribution.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnDefaultDistribution.setFocusPainted(false);

        btnSaveDistribution.setBackground(new java.awt.Color(255, 255, 255));
        btnSaveDistribution.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSaveDistribution.setForeground(new java.awt.Color(0, 102, 153));
        btnSaveDistribution.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-guardar-16.png"))); // NOI18N
        btnSaveDistribution.setText("Save");
        btnSaveDistribution.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSaveDistribution.setFocusPainted(false);

        lblDistribution1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblDistribution1.setForeground(new java.awt.Color(0, 102, 153));
        lblDistribution1.setText("DELETE DATA SET");

        btnDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnDelete.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnDelete.setForeground(new java.awt.Color(0, 102, 153));
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-eliminar-16.png"))); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnDelete.setFocusPainted(false);

        pnlChart.setBackground(new java.awt.Color(255, 255, 255));
        pnlChart.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(0, 102, 153))); // NOI18N

        javax.swing.GroupLayout pnlChartLayout = new javax.swing.GroupLayout(pnlChart);
        pnlChart.setLayout(pnlChartLayout);
        pnlChartLayout.setHorizontalGroup(
            pnlChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 256, Short.MAX_VALUE)
        );
        pnlChartLayout.setVerticalGroup(
            pnlChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        lblDeleteDataset.setText("This option will delete all images and annotations in the data set.");

        javax.swing.GroupLayout pnlDatasetLayout = new javax.swing.GroupLayout(pnlDataset);
        pnlDataset.setLayout(pnlDatasetLayout);
        pnlDatasetLayout.setHorizontalGroup(
            pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatasetLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDatasetLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnDefaultDistribution, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSaveDistribution, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlDatasetLayout.createSequentialGroup()
                        .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDistribution1)
                            .addComponent(lblDeleteDataset)
                            .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlDatasetLayout.createSequentialGroup()
                        .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(chkRandom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnlDatasetLayout.createSequentialGroup()
                                .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(lblTrain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtTrain, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                        .addComponent(lblValidation)
                                        .addComponent(txtValidation)
                                        .addComponent(lblDistribution))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(lblTest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtTest, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblPercentage01)
                                    .addComponent(lblPercentage02)
                                    .addComponent(lblPercentage03))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                        .addComponent(pnlChart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnlDatasetLayout.setVerticalGroup(
            pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatasetLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblDistribution)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlDatasetLayout.createSequentialGroup()
                        .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDatasetLayout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(lblPercentage01))
                            .addGroup(pnlDatasetLayout.createSequentialGroup()
                                .addComponent(lblTrain)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTrain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDatasetLayout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addComponent(lblPercentage02))
                            .addGroup(pnlDatasetLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTest)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblValidation)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtValidation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPercentage03))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkRandom))
                    .addComponent(pnlChart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(22, 22, 22)
                .addGroup(pnlDatasetLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSaveDistribution, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDefaultDistribution, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDistribution1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDeleteDataset)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(149, Short.MAX_VALUE))
        );

        pnlContainer.addTab("Data set", pnlDataset);

        lblDistribution2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblDistribution2.setForeground(new java.awt.Color(0, 102, 153));
        lblDistribution2.setText("STRUCTURE");

        jScrollPane1.setViewportView(treWorkspaceStructure);

        javax.swing.GroupLayout pnlStructureLayout = new javax.swing.GroupLayout(pnlStructure);
        pnlStructure.setLayout(pnlStructureLayout);
        pnlStructureLayout.setHorizontalGroup(
            pnlStructureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlStructureLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlStructureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDistribution2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlStructureLayout.setVerticalGroup(
            pnlStructureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlStructureLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblDistribution2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlContainer.addTab("Structure", pnlStructure);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlContainer)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlContainer)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnAccept;
    private javax.swing.JButton btnDefault;
    private javax.swing.JButton btnDefaultDistribution;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSaveDistribution;
    private javax.swing.JCheckBox chkRandom;
    private javax.swing.JDialog dlgDelete;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblAnnotationPath;
    private javax.swing.JLabel lblAuthor;
    private javax.swing.JLabel lblDeleteDataset;
    private javax.swing.JLabel lblDistribution;
    private javax.swing.JLabel lblDistribution1;
    private javax.swing.JLabel lblDistribution2;
    private javax.swing.JLabel lblImagePath;
    public static javax.swing.JLabel lblLog;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPath;
    private javax.swing.JLabel lblPercentage01;
    private javax.swing.JLabel lblPercentage02;
    private javax.swing.JLabel lblPercentage03;
    private javax.swing.JLabel lblResource;
    private javax.swing.JLabel lblResource1;
    private javax.swing.JLabel lblTest;
    private javax.swing.JLabel lblTrain;
    private javax.swing.JLabel lblValidation;
    private javax.swing.JPanel pnlChart;
    private javax.swing.JTabbedPane pnlContainer;
    private javax.swing.JPanel pnlDataset;
    private javax.swing.JPanel pnlGeneral;
    private javax.swing.JPanel pnlStructure;
    public static javax.swing.JProgressBar proLog;
    private javax.swing.JScrollPane scrLog;
    private javax.swing.JScrollPane scrWorkspace;
    private javax.swing.JSeparator sepSeparator;
    private javax.swing.JTable tblWorkspace;
    private javax.swing.JTree treWorkspaceStructure;
    private javax.swing.JTextField txtAnnotationPath;
    private javax.swing.JTextField txtAuthor;
    private javax.swing.JTextField txtImagePath;
    public static javax.swing.JTextArea txtLog;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPath;
    private javax.swing.JTextField txtTest;
    private javax.swing.JTextField txtTrain;
    private javax.swing.JTextField txtValidation;
    // End of variables declaration//GEN-END:variables

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
            loadData();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA EL FUNCIONAMIENTO DE LOS CONTROLES DE LA VENTANA.
     */
    private void loadControl() {
        this.setSize(new Dimension(500, 500));
        this.setResizable(Boolean.FALSE);
        this.setTitle("Workspace settings");
        this.btnDefault.addActionListener(this::defaultSettings);
        this.btnSave.addActionListener(this::saveSettings);
        this.btnDefaultDistribution.addActionListener(this::defaultDistribution);
        this.btnSaveDistribution.addActionListener(this::saveDistribution);
        this.btnDelete.addActionListener(this::delete);
        this.txtName.setEditable(Boolean.FALSE);
        this.txtPath.setEditable(Boolean.FALSE);
        this.txtImagePath.setEditable(Boolean.FALSE);
        this.txtAnnotationPath.setEditable(Boolean.FALSE);
        WorkspaceSettingsFrame.btnAccept.addActionListener(this::accept);
    }

    private void loadData() throws Exception {
        loadWorkspace();
    }

    private void loadWorkspace() throws Exception {
        WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
        WorkspaceEntity workspaceEntity = workspaceLogic.get();
        this.txtName.setText(workspaceEntity.getName());
        this.txtAuthor.setText(workspaceEntity.getAuthor());
        this.txtPath.setText(workspaceEntity.getPath());
        this.txtImagePath.setText(workspaceEntity.getImagePath());
        this.txtAnnotationPath.setText(workspaceEntity.getAnnotationPath());
        String columns[] = {"Resource", "Size"};
        DefaultTableModel defaultTableModel = new DefaultTableModel(columns, 0);
        defaultTableModel.addRow(new Object[]{"Classes", workspaceEntity.getClasses().size()});
        defaultTableModel.addRow(new Object[]{"Places", workspaceEntity.getPlaces().size()});
        defaultTableModel.addRow(new Object[]{"States", workspaceEntity.getStates().size()});
        defaultTableModel.addRow(new Object[]{"Images", workspaceEntity.getImages().size()});
        this.tblWorkspace.setModel(defaultTableModel);

        SettingsEntity settingsEntity = workspaceEntity.getSettings();

        this.txtTrain.setText(String.valueOf(settingsEntity.getTrain()));
        this.txtTest.setText(String.valueOf(settingsEntity.getTest()));
        this.txtValidation.setText(String.valueOf(settingsEntity.getValidation()));
        this.chkRandom.setSelected(settingsEntity.isRandom());

        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("Train", settingsEntity.getTrain());
        dataset.setValue("Test", settingsEntity.getTest());
        dataset.setValue("Validation", settingsEntity.getValidation());
        ChartHelper.showPieChart(dataset, this.pnlChart);
        
        TreeModel treeModel = new FileTreeModelHelper(workspaceEntity.getPath());
        this.treWorkspaceStructure.setModel(treeModel);
    }

    private void defaultSettings(ActionEvent e) {
        try {
            loadWorkspace();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void saveSettings(ActionEvent e) {
        try {
            int result = MessageHelper.showWarningMessage("Do you want to save the changes?");
            if (MessageHelper.isWarningYes(result)) {
                WorkspaceEntity workspaceEntity = WorkspaceEntity.updateAuthor(txtAuthor.getText());
                WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                workspaceLogic.updateAuthor(workspaceEntity);
                loadWorkspace();
                MessageHelper.showSuccessMessage("The changes were saved correctly!");
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void defaultDistribution(ActionEvent e) {
        try {
            this.txtTrain.setText(String.valueOf(ApplicationSettings.TRAIN));
            this.txtTest.setText(String.valueOf(ApplicationSettings.TEST));
            this.txtValidation.setText(String.valueOf(ApplicationSettings.VALIDATION));
            this.chkRandom.setSelected(Boolean.FALSE);
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void saveDistribution(ActionEvent e) {
        try {
            int result = MessageHelper.showWarningMessage("Do you want to save the changes?");
            if (MessageHelper.isWarningYes(result)) {
                SettingsEntity settingsEntity = SettingsEntity.updateDistribution(
                        Integer.parseInt(this.txtTrain.getText()),
                        Integer.parseInt(this.txtTest.getText()),
                        Integer.parseInt(this.txtValidation.getText()),
                        this.chkRandom.isSelected()
                );
                WorkspaceLogic workspaceLogic = new WorkspaceLogic(ApplicationFrame.getWorkspacePath());
                workspaceLogic.updateSettings(settingsEntity);
                loadWorkspace();
                MessageHelper.showSuccessMessage("The changes were saved correctly!");
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    private void delete(ActionEvent e) {
        try {
            int result = MessageHelper.showWarningMessage("Are you sure to delete all the images?");
            if (MessageHelper.isWarningYes(result)) {
                showDeleteWindow();
                DeleteImagesLogic deleteImagesLogic = new DeleteImagesLogic();
                deleteImagesLogic.start();
            }
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }
    
        /**
     * CIERRA EL CUADRO DE DIALOGO DE LA IMPORTACION.
     *
     * @param e EVENTO.
     */
    private void accept(ActionEvent e) {
        try {
            this.dlgDelete.dispose();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA EL CUADRO DE DIALOGO DE LA ELIMINACION.
     */
    private void showDeleteWindow() {
        this.dlgDelete.setVisible(Boolean.TRUE);
        this.dlgDelete.setSize(new Dimension(400, 300));
        this.dlgDelete.setAlwaysOnTop(Boolean.TRUE);
        this.dlgDelete.setResizable(Boolean.FALSE);
        this.dlgDelete.setLocationRelativeTo(null);
        this.dlgDelete.setTitle("Deleting images");
        try {
            ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Icons/icons8-tarea-del-sistema-100.png"));
            this.dlgDelete.setIconImage(imageIcon.getImage());
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }
}
