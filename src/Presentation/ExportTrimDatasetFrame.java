package Presentation;

import Business.ExportDatasetLogic;
import Business.ExportTrimLogic;
import Business.WorkspaceLogic;
import Entity.DatasetType;
import Entity.SettingsEntity;
import Entity.WorkspaceEntity;
import Helpers.ChartHelper;
import Helpers.MessageHelper;
import Helpers.StringHelper;
import Helpers.SystemHelper;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import org.jfree.data.general.DefaultPieDataset;

public class ExportTrimDatasetFrame extends javax.swing.JInternalFrame {

    public ExportTrimDatasetFrame() {
        initComponents();
        loadWindow();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgExport = new javax.swing.JDialog();
        lblLog = new javax.swing.JLabel();
        scrLog = new javax.swing.JScrollPane();
        txtLog = new javax.swing.JTextArea();
        proLog = new javax.swing.JProgressBar();
        btnAccept = new javax.swing.JButton();
        lblDistribution1 = new javax.swing.JLabel();
        lblOutputPath = new javax.swing.JLabel();
        txtOutputPath = new javax.swing.JTextField();
        lblDatasetType = new javax.swing.JLabel();
        cmbDatasetType = new javax.swing.JComboBox<>();
        btnSearch = new javax.swing.JButton();
        btnExport = new javax.swing.JButton();

        lblLog.setText("jLabel1");

        txtLog.setBackground(new java.awt.Color(51, 51, 51));
        txtLog.setColumns(20);
        txtLog.setFont(new java.awt.Font("Consolas", 0, 13)); // NOI18N
        txtLog.setForeground(new java.awt.Color(255, 255, 255));
        txtLog.setRows(5);
        txtLog.setText("s");
        scrLog.setViewportView(txtLog);

        btnAccept.setBackground(new java.awt.Color(255, 255, 255));
        btnAccept.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnAccept.setForeground(new java.awt.Color(0, 102, 153));
        btnAccept.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-de-acuerdo-16.png"))); // NOI18N
        btnAccept.setText("Accept");
        btnAccept.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnAccept.setFocusPainted(false);

        javax.swing.GroupLayout dlgExportLayout = new javax.swing.GroupLayout(dlgExport.getContentPane());
        dlgExport.getContentPane().setLayout(dlgExportLayout);
        dlgExportLayout.setHorizontalGroup(
            dlgExportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgExportLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgExportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrLog, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(lblLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(proLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgExportLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dlgExportLayout.setVerticalGroup(
            dlgExportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgExportLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblLog)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(proLog, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrLog, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAccept, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        setClosable(true);
        setIconifiable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-enviar-archivo-16.png"))); // NOI18N

        lblDistribution1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblDistribution1.setForeground(new java.awt.Color(0, 102, 153));
        lblDistribution1.setText("CONFIGURATION");

        lblOutputPath.setText("Output path:");

        txtOutputPath.setText("jTextField1");

        lblDatasetType.setText("Data set type:");

        cmbDatasetType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnSearch.setBackground(new java.awt.Color(255, 255, 255));
        btnSearch.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(0, 102, 153));
        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-búsqueda-16.png"))); // NOI18N
        btnSearch.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnSearch.setFocusPainted(false);

        btnExport.setBackground(new java.awt.Color(255, 255, 255));
        btnExport.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnExport.setForeground(new java.awt.Color(0, 102, 153));
        btnExport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8-enviar-archivo-16.png"))); // NOI18N
        btnExport.setText("Export");
        btnExport.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 153), 2));
        btnExport.setFocusPainted(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDistribution1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbDatasetType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtOutputPath)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 376, Short.MAX_VALUE)
                        .addComponent(btnExport, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblDatasetType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblOutputPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblDistribution1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDatasetType)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbDatasetType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblOutputPath)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtOutputPath))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 316, Short.MAX_VALUE)
                .addComponent(btnExport, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnAccept;
    private javax.swing.JButton btnExport;
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox<String> cmbDatasetType;
    private javax.swing.JDialog dlgExport;
    private javax.swing.JLabel lblDatasetType;
    private javax.swing.JLabel lblDistribution1;
    public static javax.swing.JLabel lblLog;
    private javax.swing.JLabel lblOutputPath;
    public static javax.swing.JProgressBar proLog;
    private javax.swing.JScrollPane scrLog;
    public static javax.swing.JTextArea txtLog;
    private javax.swing.JTextField txtOutputPath;
    // End of variables declaration//GEN-END:variables

    /**
     * CARGA LOS COMPONENTES DE LA VENTANA (CONTROLES Y DATOS).
     */
    private void loadWindow() {
        try {
            loadControl();
            loadData();
            loadForm();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CARGA EL FUNCIONAMIENTO DE LOS CONTROLES DE LA VENTANA.
     */
    private void loadControl() {
        this.setSize(new Dimension(500, 500));
        this.setResizable(Boolean.FALSE);
        this.setTitle("Export images to data set");
        this.btnSearch.addActionListener(this::search);
        this.btnExport.addActionListener(this::export);
        ExportTrimDatasetFrame.btnAccept.addActionListener(this::accept);
        this.txtOutputPath.setEditable(Boolean.FALSE);
    }

    private void loadData() throws Exception {
        loadDatasetTypes();
    }

    private void loadForm() {
        this.txtOutputPath.setText(StringHelper.EMPTY);
    }

    private void loadDatasetTypes() throws Exception {
        this.cmbDatasetType.removeAllItems();
        for (DatasetType dataset : DatasetType.values()) {
            this.cmbDatasetType.addItem(dataset.name());
        }
    }

    /**
     * MUESTRA UN CUADRO DE DIALOGO PARA OBTENER UNA RUTA.
     *
     * @param e EVENTO.
     */
    private void search(ActionEvent e) {
        try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File(SystemHelper.getUserHome()));
            fileChooser.setDialogTitle("Choose a direcory");
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                this.txtOutputPath.setText(fileChooser.getSelectedFile().getPath());
            } else {
                this.txtOutputPath.setText(StringHelper.EMPTY);
            }
        } catch (HeadlessException exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA UN CUADRO DE DIALOGO QUE PERMITE EXPORTAR LAS ANOTACIONES
     * DETECTADAS.
     *
     * @param e EVENTO.
     */
    private void export(ActionEvent e) {
        try {
            String destinationPath = this.txtOutputPath.getText();
            if (StringHelper.isNullOrEmpty(destinationPath)) {
                throw new Exception("Output path is required!");
            }
            showExportWindow();
            DatasetType datasetType = DatasetType.valueOf((String) this.cmbDatasetType.getSelectedItem());
            ExportTrimLogic exportTrimLogic = new ExportTrimLogic(destinationPath, datasetType);
            exportTrimLogic.start();
        } catch (Exception exception) {
            this.dlgExport.dispose();
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * CIERRA EL CUADRO DE DIALOGO DE LA EXPORTACION.
     *
     * @param e EVENTO.
     */
    private void accept(ActionEvent e) {
        try {
            this.dlgExport.dispose();
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }

    /**
     * MUESTRA EL CUADRO DE DIALOGO DE LA IMPORTACION.
     */
    private void showExportWindow() {
        this.dlgExport.setVisible(Boolean.TRUE);
        this.dlgExport.setSize(new Dimension(400, 300));
        this.dlgExport.setAlwaysOnTop(Boolean.TRUE);
        this.dlgExport.setResizable(Boolean.FALSE);
        this.dlgExport.setLocationRelativeTo(null);
        this.dlgExport.setTitle("Exporting images to data set");
        try {
            ImageIcon imageIcon = new ImageIcon(getClass().getResource("/Icons/icons8-tarea-del-sistema-100.png"));
            this.dlgExport.setIconImage(imageIcon.getImage());
        } catch (Exception exception) {
            MessageHelper.showErrorMessage(exception);
        }
    }
}
